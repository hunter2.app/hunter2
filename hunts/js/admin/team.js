import { createApp } from 'vue'
import TeamDetail from './team-detail.vue'
import * as Sentry from '@sentry/vue'

const el = document.getElementById('team_puzzles_admin_widget')

const team = createApp(TeamDetail, {
  href: el.attributes.href.value,
})
team.mixin(Sentry.createTracingMixins({ trackComponents: true }))
Sentry.attachErrorHandler(team, { logErrors: true })
team.mount(el)