# Generated by Django 4.2.7 on 2023-11-20 22:52

from django.db import migrations, models
import django.db.models.deletion
import django_jsonform.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0015_event_clues_in_flavour'),
        ('hunter2', '0009_configuration_privacy_policy_version'),
    ]

    operations = [
        migrations.AddField(
            model_name='apitoken',
            name='event',
            field=models.ForeignKey(blank=True, help_text='If provided restrict API usage to resources in this event.', null=True, on_delete=django.db.models.deletion.CASCADE, to='events.event'),
        ),
        migrations.AddField(
            model_name='apitoken',
            name='scopes',
            # In releases before this all scopes could access all APIs. Translate this into the new authZ system.
            field=django_jsonform.models.fields.JSONField(default=[
                {'fields': {}, 'operation': 'list', 'resource': 'teams'},
                {'fields': {}, 'operation': 'get', 'resource': 'team'},
            ], help_text='Which RPCs and/or Websocket update streams can be used by this token.'),
            preserve_default=False,
        ),
    ]
