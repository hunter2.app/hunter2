# Copyright (C) 2018-2022 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
import builtins
import json
import os
import tempfile
import unittest
import uuid
from datetime import timedelta, datetime
from datetime import timezone as dt_timezone
from io import StringIO
from unittest import expectedFailure
from unittest.mock import patch

import django.contrib.admin.sites
import freezegun
import pytest
from allauth.account.models import EmailAddress
from allauth.socialaccount.models import SocialAccount
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.management import CommandError, call_command
from django.http import HttpRequest
from django.test import TestCase, override_settings, RequestFactory
from django.urls import reverse
from django.utils import timezone
from django_tenants.test.client import TenantClient
from gdpr_assist.upgrading import check_migrate_gdpr_anonymised

from accounts.factories import UserFactory
from events.factories import EventFactory
from events.test import EventTestCase
from hunter2.management.commands import setupsite, anonymise
from hunts.factories import EpisodeFactory
from teams.factories import TeamMemberFactory
from .admin import ConfigurationAdmin
from .factories import FileFactory, IconFactory, APITokenFactory
from .mixins import APITokenRequiredMixin
from .models import Configuration
from .test import mock_inputs, MockTTY
from .utils import generate_secret_key, load_or_create_secret_key


@pytest.mark.usefixtures('db')
class TestFactories:
    def test_api_token_factory_default_construction(self):
        APITokenFactory.create()

    def test_file_factory_default_construction(self):
        FileFactory.create()

    def test_icon_factory_default_construction(self):
        IconFactory.create()


class TestAPITokenAuthZ:
    def test_no_scopes(self, event):
        token = APITokenFactory(scopes=[])
        assert token.authorise(event, 'stream', 'resources', {}) is False

    def test_wrong_event(self, event):
        wrong_event = EventFactory.build()  # Avoid writing the event in transaction
        token = APITokenFactory(event=event, scopes=[
            {'operation': 'read', 'resource': 'thing', 'fields': {}}
        ])
        assert token.authorise(wrong_event, 'read', 'thing', {'thing_id': str(uuid.uuid4())}) is False

    def test_right_event(self, event):
        token = APITokenFactory(event=event, scopes=[
            {'operation': 'read', 'resource': 'thing', 'fields': {}}
        ])
        assert token.authorise(event, 'read', 'thing', {'thing_id': str(uuid.uuid4())}) is True

    def test_wrong_operation(self, event):
        token = APITokenFactory(scopes=[{'operation': 'read', 'resource': 'things', 'fields': {}}])
        assert token.authorise(event, 'stream', 'things', {}) is False

    def test_wrong_resource(self, event):
        token = APITokenFactory(scopes=[{'operation': 'stream', 'resource': 'items', 'fields': {}}])
        assert token.authorise(event, 'stream', 'things', {}) is False

    def test_scope_no_fields(self, event):
        token = APITokenFactory(scopes=[
            {'operation': 'read', 'resource': 'thing', 'fields': {}}
        ])
        assert token.authorise(event, 'read', 'thing', {'thing_id': str(uuid.uuid4())}) is True

    def test_scope_wrong_fields(self, event):
        token = APITokenFactory(scopes=[
            {'operation': 'read', 'resource': 'thing', 'fields': {'thing_id': str(uuid.uuid4())}}
        ])
        assert token.authorise(event, 'read', 'thing', {'thing_id': str(uuid.uuid4())}) is False

    def test_scope_match_field(self, event):
        thing_id = str(uuid.uuid4())
        token = APITokenFactory(scopes=[
            {'operation': 'read', 'resource': 'thing', 'fields': {'thing_id': thing_id}}
        ])
        assert token.authorise(event, 'read', 'thing', {'thing_id': thing_id}) is True


@pytest.mark.usefixtures('db')
class TestAPITokenRequiredMixin:
    @pytest.fixture
    def mixin(self):
        mixin = APITokenRequiredMixin()
        mixin.operation = 'test'
        mixin.resource = 'test-target'
        return mixin

    @pytest.fixture
    def valid_token(self):
        return APITokenFactory(scopes={
            'operation': 'test',
            'resource': 'test-target',
            'fields': {},
        })

    def test_no_header(self, mixin):
        req = RequestFactory().get('/')
        rsp = mixin.dispatch(req)
        assert rsp.status_code == 401
        payload = json.loads(rsp.content)
        assert payload['result'] == 'Unauthorized'
        assert payload['message'] == 'No Authorization header'

    def test_short_authorization(self, mixin):
        req = RequestFactory().get('/', headers={'Authorization': 'Bearer'})
        rsp = mixin.dispatch(req)
        assert rsp.status_code == 401
        payload = json.loads(rsp.content)
        assert payload['result'] == 'Unauthorized'
        assert payload['message'] == 'Malformed Authorization header'

    def test_long_authorization(self, mixin):
        req = RequestFactory().get('/', headers={'Authorization': 'Bearer of bears'})
        rsp = mixin.dispatch(req)
        assert rsp.status_code == 401
        payload = json.loads(rsp.content)
        assert payload['result'] == 'Unauthorized'
        assert payload['message'] == 'Malformed Authorization header'

    def test_invalid_bearer_keyword(self, mixin, valid_token):
        req = RequestFactory().get('/', headers={'Authorization': f'Bear {valid_token.token}'})
        rsp = mixin.dispatch(req)
        assert rsp.status_code == 401
        payload = json.loads(rsp.content)
        assert payload['result'] == 'Unauthorized'
        assert payload['message'] == 'Malformed Authorization header'

    def test_invalid_bearer_token(self, mixin):
        req = RequestFactory().get('/', headers={'Authorization': f'Bearer {uuid.uuid4()}'})
        rsp = mixin.dispatch(req)
        assert rsp.status_code == 401
        payload = json.loads(rsp.content)
        assert payload['result'] == 'Unauthorized'
        assert payload['message'] == 'Invalid Bearer token'

    def test_non_json_body(self, mixin, valid_token):
        req = RequestFactory().post(
            '/',
            content_type='text/plain',
            data={'k': 'v'},
            headers={'Authorization': f'Bearer {valid_token.token}'},
        )
        rsp = mixin.dispatch(req)
        assert rsp.status_code == 406
        payload = json.loads(rsp.content)
        assert payload['result'] == 'Unacceptable'
        assert payload['message'] == 'APIs only accept JSON payloads'

    def test_bad_json_body(self, mixin, valid_token):
        req = RequestFactory().post(
            '/',
            content_type='application/json',
            data='the key k is to take the value v',
            headers={'Authorization': f'Bearer {valid_token.token}'},
        )
        rsp = mixin.dispatch(req)
        assert rsp.status_code == 400
        payload = json.loads(rsp.content)
        assert payload['result'] == 'Bad Request'
        assert payload['message'] == 'Malformed request body'

    def test_no_authorised_scope(self, mixin):
        token = APITokenFactory(scopes=[])
        req = RequestFactory().get('/', headers={'Authorization': f'Bearer {token.token}'})
        req.tenant = EventFactory.build()
        rsp = mixin.dispatch(req)
        assert rsp.status_code == 403
        payload = json.loads(rsp.content)
        assert payload['result'] == 'Forbidden'
        assert payload['message'] == 'Token not authorised for this operation'


class MigrationsTests(TestCase):
    # Adapted for Python3 from:
    # http://tech.octopus.energy/news/2016/01/21/testing-for-missing-migrations-in-django.html
    @override_settings(MIGRATION_MODULES={})
    def test_for_missing_migrations(self):
        output = StringIO()
        try:
            call_command(
                'makemigrations',
                interactive=False,
                dry_run=True,
                check_changes=True,
                stdout=output
            )
        except SystemExit:
            self.fail("There are missing migrations:\n %s" % output.getvalue())

    # We silence the check that gdpr_assist runs on startup, but still want the things that check asserts to be true
    # for the app
    @override_settings(SILENCED_SYSTEM_CHECKS=())
    def test_gdpr_assist_check(self):
        self.assertEqual(check_migrate_gdpr_anonymised(None), [])


class SecretKeyGenerationTests(TestCase):
    def test_secret_key_length(self):
        secret_key = generate_secret_key()
        self.assertGreaterEqual(len(secret_key), 50)

    def test_subsequent_secret_keys_are_different(self):
        secret_key1 = generate_secret_key()
        secret_key2 = generate_secret_key()
        self.assertNotEqual(secret_key1, secret_key2)

    def test_write_generated_key(self):
        with tempfile.TemporaryDirectory() as temp_dir:
            secrets_file = os.path.join(temp_dir, "secrets.ini")
            self.assertFalse(os.path.exists(secrets_file))
            load_or_create_secret_key(secrets_file)
            self.assertTrue(os.path.exists(secrets_file))

    def test_preserve_existing_key(self):
        with tempfile.TemporaryDirectory() as temp_dir:
            secrets_file = os.path.join(temp_dir, "secrets.ini")
            self.assertFalse(os.path.exists(secrets_file))
            secret_key1 = load_or_create_secret_key(secrets_file)
            self.assertTrue(os.path.exists(secrets_file))
            secret_key2 = load_or_create_secret_key(secrets_file)
            self.assertEqual(secret_key1, secret_key2)


class SetupSiteManagementCommandTests(TestCase):
    TEST_SITE_NAME   = "Test Site"
    TEST_SITE_DOMAIN = "test-domain.local"

    def test_no_site_name_argument(self):
        output = StringIO()
        with self.assertRaisesMessage(CommandError, "You must use --name and --domain with --noinput."):
            call_command('setupsite', interactive=False, site_domain="custom-domain.local", stdout=output)

    def test_no_site_domain_argument(self):
        output = StringIO()
        with self.assertRaisesMessage(CommandError, "You must use --name and --domain with --noinput."):
            call_command('setupsite', interactive=False, site_name="Custom Site", stdout=output)

    def test_non_interactive_usage(self):
        output = StringIO()
        call_command(
            'setupsite',
            interactive=False,
            site_name=self.TEST_SITE_NAME,
            site_domain=self.TEST_SITE_DOMAIN,
            stdout=output
        )
        command_output = output.getvalue().strip()
        self.assertEqual(command_output, "Set site name as \"{}\" with domain \"{}\"".format(
            self.TEST_SITE_NAME,
            self.TEST_SITE_DOMAIN
        ))

        site = Site.objects.get()
        self.assertEqual(site.name,   self.TEST_SITE_NAME)
        self.assertEqual(site.domain, self.TEST_SITE_DOMAIN)

    @mock_inputs({
        'site name':   TEST_SITE_NAME,
        'site domain': TEST_SITE_DOMAIN
    })
    def test_interactive_usage(self):
        output = StringIO()
        call_command(
            'setupsite',
            interactive=True,
            stdout=output,
            stdin=MockTTY(),
        )
        command_output = output.getvalue().strip()
        self.assertEqual(command_output, "Set site name as \"{}\" with domain \"{}\"".format(
            self.TEST_SITE_NAME,
            self.TEST_SITE_DOMAIN
        ))
        site = Site.objects.get()
        self.assertEqual(site.name,   self.TEST_SITE_NAME)
        self.assertEqual(site.domain, self.TEST_SITE_DOMAIN)

    @mock_inputs({
        'site name':   "",
        'site domain': "",
    })
    def test_interactive_defaults_usage(self):
        output = StringIO()
        call_command(
            'setupsite',
            interactive=True,
            stdout=output,
            stdin=MockTTY(),
        )
        command_output = output.getvalue().strip()
        self.assertEqual(command_output, "Set site name as \"{}\" with domain \"{}\"".format(
            setupsite.Command.DEFAULT_SITE_NAME,
            setupsite.Command.DEFAULT_SITE_DOMAIN
        ))

        site = Site.objects.get()
        self.assertEqual(site.name,   setupsite.Command.DEFAULT_SITE_NAME)
        self.assertEqual(site.domain, setupsite.Command.DEFAULT_SITE_DOMAIN)

    def test_domain_validation(self):
        output = StringIO()
        test_domain = "+.,|!"
        with self.assertRaisesMessage(CommandError, "Domain name \"{}\" is not a valid domain name.".format(test_domain)):
            call_command(
                'setupsite',
                interactive=False,
                site_name=self.TEST_SITE_NAME,
                site_domain=test_domain,
                stdout=output
            )


class AnonymisationCommandTests(EventTestCase):
    def setUp(self):
        # When a Daylight Savings Time boundary occurs during the window in the relative time specification
        # there's an ambiguity whether "a day ago" means the same local time on the previous calendar day
        # or 24 hours earlier.
        # We're choosing to interpret it as the same local time on the previous calendar day and accordingly
        # need to craft the cutoff carefully.
        now = timezone.localtime()
        self.cutoff = (now.replace(tzinfo=None) - timedelta(days=1)).astimezone(now.tzinfo)
        self.user_before = TeamMemberFactory(
            username='person1',
            email='someone@somewhere.com',
            last_login=self.cutoff - timedelta(minutes=1),
            picture='https://imagesite.com/an_image.jpg',
            contact=True,
        )
        SocialAccount(user=self.user_before, uid=1).save()
        EmailAddress(user=self.user_before, email=self.user_before.email).save()
        self.before_pk = self.user_before.pk
        self.user_after = TeamMemberFactory(last_login=self.cutoff + timedelta(minutes=1))
        self.after_username = self.user_after.username
        self.after_email = self.user_after.email
        self.after_picture = self.user_after.picture
        self.after_contact = self.user_after.contact
        SocialAccount(user=self.user_after, uid=2).save()
        EmailAddress(user=self.user_after, email=self.user_after.email).save()

    def test_no_site_name_argument(self):
        output = StringIO()
        with self.assertRaisesMessage(CommandError, "Error: the following arguments are required"):
            call_command('anonymise', interactive=False, stdout=output)

    def test_no_cancels(self):
        output = StringIO()
        with patch('builtins.input') as mock_input:
            mock_input.return_value = "no"
            call_command('anonymise', self.cutoff.isoformat(), stdout=output)
            mock_input.assert_called_once_with(
                f'Anonymise 1 users who last logged in before {self.cutoff.isoformat(" ", "seconds")}? (yes/no) '
            )
        output = output.getvalue().strip()

        self.assertIn('Aborting', output)

        self.user_before.refresh_from_db()
        self.user_after.refresh_from_db()

        self.assertEqual(self.user_before.username, 'person1')
        self.assertEqual(self.user_before.email, 'someone@somewhere.com')
        self.assertEqual(self.user_before.picture, 'https://imagesite.com/an_image.jpg')
        self.assertEqual(self.user_before.contact, True)

        self.assertEqual(self.user_after.username, self.after_username)
        self.assertEqual(self.user_after.email, self.after_email)
        self.assertEqual(self.user_after.picture, self.after_picture)
        self.assertEqual(self.user_after.contact, self.after_contact)

    def test_usage(self):
        output = StringIO()
        call_command('anonymise', self.cutoff.isoformat(), yes=True, stdout=output)
        output = output.getvalue().strip()

        self.assertIn('1 users who last logged in before', output)
        self.assertIn('Done', output)

        self.usage_assertions()

    def test_usage_interactive(self):
        output = StringIO()
        with patch('builtins.input') as mock_input:
            mock_input.return_value = "yes"
            call_command('anonymise', self.cutoff.isoformat(), stdout=output)
            mock_input.assert_called_once_with(
                f'Anonymise 1 users who last logged in before {self.cutoff.isoformat(" ", "seconds")}? (yes/no) '
            )
        output = output.getvalue().strip()

        self.assertIn('Done', output)

        self.usage_assertions()

    def test_usage_relative(self):
        output = StringIO()
        call_command('anonymise', "1 day ago", yes=True, stdout=output)
        output = output.getvalue().strip()

        self.assertIn('1 users who last logged in before', output)
        self.assertIn('Done', output)

        self.usage_assertions()

    def usage_assertions(self):
        self.user_before.refresh_from_db()
        self.user_after.refresh_from_db()

        self.assertEqual(self.user_before.username, f'{self.before_pk}')
        self.assertEqual(self.user_before.email, f'{self.before_pk}@anon.example.com')
        self.assertEqual(self.user_before.picture, '')
        self.assertEqual(self.user_before.contact, False)
        self.assertEqual(SocialAccount.objects.filter(user=self.user_before).count(), 0)
        self.assertEqual(EmailAddress.objects.filter(user=self.user_before).count(), 0)
        self.assertEqual(self.user_after.username, self.after_username)
        self.assertEqual(self.user_after.email, self.after_email)
        self.assertEqual(self.user_after.picture, self.after_picture)
        self.assertEqual(self.user_after.contact, self.after_contact)
        self.assertEqual(SocialAccount.objects.filter(user=self.user_after).count(), 1)
        self.assertEqual(EmailAddress.objects.filter(user=self.user_after).count(), 1)


class AnonymisationMethodTests(EventTestCase):
    def test_parse_date_parses_isodate(self):
        self.assertEqual(
            anonymise.Command.parse_date('2020-01-02T18:03:04+01:00'),
            datetime(2020, 1, 2, 18, 3, 4, tzinfo=dt_timezone(timedelta(hours=1)))
        )

    def test_parse_date_parses_relative_date(self):
        with freezegun.freeze_time(datetime(2020, 1, 2, tzinfo=dt_timezone(timedelta(hours=-1)))):
            self.assertEqual(
                anonymise.Command.parse_date('one year ago'),
                datetime(2019, 1, 2, tzinfo=dt_timezone(timedelta(hours=-1)))
            )

    def test_parse_date_parses_ambiguous_date(self):
        self.assertEqual(
            anonymise.Command.parse_date('01/02/03'),
            datetime(2003, 2, 1, tzinfo=dt_timezone(timedelta(hours=0)))
        )

    # TODO see https://github.com/scrapinghub/dateparser/issues/412
    @expectedFailure
    def test_parse_date_parses_ambiguous_date_year_first(self):
        self.assertEqual(
            anonymise.Command.parse_date('2001/02/03'),
            datetime(2001, 2, 3, tzinfo=dt_timezone(timedelta(hours=0)))
        )

    def test_get_confirmation(self):
        cmd = anonymise.Command()
        with unittest.mock.patch.object(builtins, 'input') as mock_input:
            mock_input.return_value = 'yes'
            self.assertTrue(cmd.get_confirmation(False, range(5), datetime.fromisoformat('2020-01-02 00:00')))
            mock_input.assert_called_once_with(
                'Anonymise 5 users who last logged in before 2020-01-02 00:00:00? (yes/no) '
            )

        with unittest.mock.patch.object(builtins, 'input') as mock_input:
            mock_input.return_value = 'no'
            self.assertFalse(cmd.get_confirmation(False, range(3), datetime.fromisoformat('2020-01-04 00:00')))
            mock_input.assert_called_once_with(
                'Anonymise 3 users who last logged in before 2020-01-04 00:00:00? (yes/no) '
            )


@pytest.mark.usefixtures('tenant_transactional_db')
class TestMultiEventNavigation:
    def test_load_event_list(self, client):
        now = timezone.now()
        past = EventFactory(end_date=now - timedelta(days=1))
        current = EventFactory(end_date=now + timedelta(days=1))
        current.activate()
        EpisodeFactory(start_date=now - timedelta(days=1))
        future = EventFactory(end_date=now + timedelta(days=365))
        future.activate()
        EpisodeFactory(start_date=now + timedelta(days=1))
        EventFactory(is_published=False)

        rsp = client.get(reverse('index', urlconf=settings.PUBLIC_SCHEMA_URLCONF))
        assert rsp.status_code == 200
        assert len(rsp.context['past_events']) == 1
        assert rsp.context['past_events'][0]['name'] == past.name
        assert len(rsp.context['current_events']) == 1
        assert rsp.context['current_events'][0]['name'] == current.name
        assert len(rsp.context['future_events']) == 1
        assert rsp.context['future_events'][0]['name'] == future.name

        tenant_client = TenantClient(current)
        rsp = tenant_client.get(reverse('index'))
        assert rsp.status_code == 200
        assert len(rsp.context['other_events']) == 2
        names = [e['name'] for e in rsp.context['other_events']]
        assert past.name in names
        assert future.name in names


@pytest.mark.usefixtures('db')
class TestConfigurationForm:
    # Test the commit=True behaviour on the form save explicitly since SingletonModelAdmin never calls it this way,
    # but we've implemented it for correctness' sake.
    def test_commit_configuration_form(self):
        configuration = Configuration.get_solo()
        expected_privacy_policy_version = configuration.privacy_policy_version + 1

        request = HttpRequest()
        request.POST = {
            'privacy_policy': configuration.privacy_policy,
            'bump_privacy_policy_version': 'on',
            'script': configuration.script,
            'style': configuration.style,
            'captcha_question': configuration.captcha_question,
            'captcha_answer': configuration.captcha_answer,
        }
        request.user = UserFactory(is_staff=True, is_superuser=True)

        form = ConfigurationAdmin(Configuration, django.contrib.admin.site).get_form(request, configuration, True)(request.POST, instance=configuration)
        form.save()

        configuration.refresh_from_db()
        assert configuration.privacy_policy_version == expected_privacy_policy_version


class TestConfigurationUpdate:

    @pytest.fixture
    def url(self):
        return reverse('admin:hunter2_configuration_change')

    def test_load_configuration_form(self, superuser_client, url):
        response = superuser_client.get(url)
        assert response.status_code == 200

    def test_keep_privacy_policy_version(self, superuser_client, url):
        configuration = Configuration.get_solo()
        expected_privacy_policy_version = configuration.privacy_policy_version
        response = superuser_client.post(url, {
            'privacy_policy': configuration.privacy_policy,
            'script': configuration.script,
            'style': configuration.style,
            'captcha_question': configuration.captcha_question,
            'captcha_answer': configuration.captcha_answer,
        })
        assert response.status_code == 302
        configuration.refresh_from_db()
        assert configuration.privacy_policy_version == expected_privacy_policy_version

    def test_bump_privacy_policy_version(self, superuser, superuser_client, url):
        configuration = Configuration.get_solo()
        expected_privacy_policy_version = configuration.privacy_policy_version + 1
        response = superuser_client.post(url, {
            'privacy_policy': configuration.privacy_policy,
            'bump_privacy_policy_version': 'on',
            'script': configuration.script,
            'style': configuration.style,
            'captcha_question': configuration.captcha_question,
            'captcha_answer': configuration.captcha_answer,
        })
        assert response.status_code == 302
        configuration.refresh_from_db()
        assert configuration.privacy_policy_version == expected_privacy_policy_version
        superuser.refresh_from_db()
        assert superuser.accepted_privacy_policy_version == expected_privacy_policy_version
