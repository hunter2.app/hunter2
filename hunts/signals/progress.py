# Copyright (C) 2021 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

import structlog
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import IntegrityError, transaction
from django.db.models import Q, Subquery, OuterRef, Prefetch
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from django.utils import timezone

from teams.models import Team
from .helpers import hybrid_save_handler, post_delete_handler, pre_save_handler, post_save_handler, pre_delete_handler
from .. import models, hint_scheduler

logger = structlog.get_logger(__name__)


@pre_save_handler(models.Guess)
def save_guess(sender, instance, raw, *args, **kwargs):
    if raw:
        return  # nocover
    guess = instance
    answers = guess.for_puzzle.answer_set.all()

    guess.correct_for = None
    guess.correct_current = True

    for answer in answers:
        if answer.validate_guess(guess):
            logger.debug(
                'Progress: (pre-save) guess correct',
                puzzle_id=guess.for_puzzle.id,
                puzzle_title=guess.for_puzzle.title,
                guess_id=guess.compact_id,
                answer_id=answer.id,
                answer=answer.answer,
                team_id=guess.by_team.id,
                user_id=guess.by.id,
            )
            guess.correct_for = answer
            break


@post_save_handler(models.Guess)
def saved_guess(sender, instance, raw, created, *args, **kwargs):
    # Progress-related stuff happens post-save so that we can update FKs to the guess
    if raw:
        return  # nocover
    # This should never happen in ordinary use
    # TODO: enforce (after removing `get_correct_for`?)
    if not created:
        return  # nocover
    guess = instance
    answers = guess.for_puzzle.answer_set.all()
    progress, created = models.TeamPuzzleProgress.objects.get_or_create(
        team=guess.by_team, puzzle=guess.for_puzzle,
        defaults={
            # If we do have to create this then we don't know the start time.
            # Use the guess time as the start time since it's the only bound we have.
            'start_time': guess.given,
            'late': guess.late,
            'team_size': guess.by_team.members.count(),
        },
    )
    if created:
        logger.warning(
            'Progress: created TeamPuzzleProgress after guess',
            puzzle_id=progress.puzzle.id,
            puzzle_title=progress.puzzle.title,
            guess_id=guess.compact_id,
            team_id=guess.by_team.id,
            user_id=guess.by.id,
        )
    if guess.late and not progress.late:
        logger.info(
            'Progress: recording Team lateness after guess',
            puzzle_id=progress.puzzle.id,
            puzzle_title=progress.puzzle.title,
            guess_id=guess.compact_id,
            team_id=guess.by_team.id,
            user_id=guess.by.id,
        )
        progress.late = True
        progress.save()

    # hints are prefetched for the consumer signal handler
    unlocks = guess.for_puzzle.unlock_set.all().prefetch_related(
        'unlockanswer_set',
        'unlockanswer_set__unlock__obsoletes',
        'dependent_hints',
    ).seal()

    if not progress.solved_by_id:
        for answer in answers:
            if answer.validate_guess(guess):
                logger.info(
                    'Progress: guess correct',
                    puzzle_id=guess.for_puzzle.id,
                    puzzle_title=guess.for_puzzle.title,
                    guess_id=guess.compact_id,
                    answer_id=answer.id,
                    answer=answer.answer,
                    team_id=guess.by_team.id,
                    user_id=guess.by.id,
                )
                progress.solved_by = guess
                progress._set_team_size()
                guess.is_correct = True
                progress.save()
                break

    for unlock in unlocks:
        for unlockanswer in unlock.unlockanswer_set.all():
            if unlockanswer.validate_guess(guess):
                logger.info(
                    'Progress: unlock obtained',
                    puzzle_id=guess.for_puzzle.id,
                    puzzle_title=guess.for_puzzle.title,
                    guess_id=guess.compact_id,
                    unlock_id=unlock.compact_id,
                    unlock=unlock.text,
                    unlock_hidden=unlock.hidden,
                    unlockanswer_id=unlockanswer.id,
                    unlockanswer=unlockanswer.guess,
                    team_id=guess.by_team.id,
                    user_id=guess.by.id,
                )
                models.TeamUnlock(
                    team_puzzle_progress=progress, unlockanswer=unlockanswer, unlocked_by=guess
                ).save()
                if not unlock.hidden:
                    guess.unlocked_something = True


@post_save_handler(models.Answer)
def saved_answer(sender, instance, raw, created, *args, **kwargs):
    if raw:
        return  # nocover
    answer = instance
    puzzle = answer.for_puzzle
    puzzle_answers = list(puzzle.answer_set.exclude(pk=answer.pk))
    puzzle_answers.append(answer)

    guesses = models.Guess.objects.filter(
        Q(for_puzzle=puzzle),
        Q(correct_for__isnull=True) | Q(correct_for=answer)
    )
    count = guesses.update(correct_for=None)
    if count > 0:
        logger.warning(
            'Progress: new/altered answer potentially affected correctness',
            num_potentially_affected_guesses=count,
            puzzle_id=puzzle.id,
            puzzle_title=puzzle.title,
            answer_id=answer.id,
            answer=answer.answer,
        )
    guesses.evaluate_correctness(puzzle_answers)
    models.TeamPuzzleProgress.objects.filter(puzzle=puzzle).reevaluate()


@pre_delete_handler(models.Answer)
def deleted_answer(sender, instance, *args, **kwargs):
    answer = instance
    puzzle = answer.for_puzzle
    puzzle_answers = list(puzzle.answer_set.exclude(pk=answer.pk))
    guesses = models.Guess.objects.filter(
        for_puzzle=puzzle,
        correct_for=answer
    ).order_by('given').seal()
    count = guesses.update(correct_current=False)
    if count > 0:
        logger.warning(
            'Progress: deleted answer and potentially affected correctness',
            num_potentially_affected_guesses=count,
            puzzle_id=puzzle.id,
            puzzle_title=puzzle.title,
            answer_id=answer.id,
            answer=answer.answer,
        )
    guesses.evaluate_correctness(puzzle_answers)

    models.TeamPuzzleProgress.objects.filter(puzzle=puzzle).reevaluate()


@post_save_handler(models.UnlockAnswer)
def saved_unlockanswer(sender, instance, raw, *args, **kwargs):
    if raw:
        return  # nocover
    unlockanswer = instance
    unlock = unlockanswer.unlock
    puzzle = unlock.puzzle

    guesses = models.Guess.objects.filter(for_puzzle=puzzle).seal()
    still_unlock = []
    for guess in guesses:
        if unlockanswer.validate_guess(guess):
            still_unlock.append(guess)
    _, counts = models.TeamUnlock.objects.filter(unlockanswer=unlockanswer).exclude(unlocked_by__in=still_unlock).delete()
    logger.warning(
        'Progress: altered unlockanswer removed unlocks from teams',
        deleted_teamunlocks=counts.get('hunts.TeamUnlock', 0),
        puzzle_id=puzzle.id,
        puzzle_title=puzzle.title,
        unlockanswer_id=unlockanswer.id,
        unlockanswer=unlockanswer.guess,
    )

    if not still_unlock:
        return

    # puzzle.episode is needed within the websocket signal handler
    progresses = {
        tpp.team_id: tpp
        for tpp in models.TeamPuzzleProgress.objects.filter(puzzle=puzzle).select_related(
            'team', 'puzzle', 'puzzle__episode',
        )  # This can't be sealed. We need to be able to fetch the updated teamunlock list in the signals triggered by `TeamUnlock.save()` below.
    }
    unlocking_guess_ids = set(i[0] for i in models.TeamUnlock.objects.filter(unlockanswer=unlockanswer).values_list('unlocked_by_id'))
    for guess in still_unlock:
        tpp = progresses[guess.by_team_id]
        if guess.id not in unlocking_guess_ids:
            models.TeamUnlock(team_puzzle_progress=tpp, unlockanswer=unlockanswer, unlocked_by=guess).save()
            logger.warning(
                'Progress: altered unlockanswer gave new unlock',
                puzzle_id=puzzle.id,
                puzzle_title=puzzle.title,
                unlock_id=unlock.compact_id,
                unlock=unlock.text,
                unlockanswer_id=unlockanswer.id,
                unlockanswer=unlockanswer.guess,
                team_id=tpp.team_id,
            )


@post_save_handler(models.TeamUnlock)
def saved_teamunlock(sender, instance, raw, *args,  **kwargs):
    if raw:
        return  # nocover

    teamunlock = instance

    tpp = teamunlock.team_puzzle_progress
    dependent = teamunlock.unlockanswer.unlock.dependent_hints.all()
    obsoleted = teamunlock.unlockanswer.unlock.obsoletes.filter(
        Q(start_after__in=teamunlock.team_puzzle_progress.teamunlock_set.values('unlockanswer__unlock')) |
        Q(start_after=None)
    )
    for hint in dependent:
        _, created = models.TeamHint.objects.get_or_create(
            team_puzzle_progress=tpp,
            hint=hint,
            defaults=dict(
                unlocked_at=teamunlock.unlocked_by.given + hint.time,
                started_by=teamunlock,
                obsoleted_by=models.TeamUnlock.objects.filter(
                    team_puzzle_progress=tpp,
                    unlockanswer__unlock__in=hint.obsoleted_by.all()
                ).order_by('unlocked_by__given').first()
            )
        )
        if created:
            logger.info(
                'Progress: unlock triggered dependent hint schedule',
                puzzle_id=tpp.puzzle.id,
                puzzle_title=tpp.puzzle.title,
                team_id=tpp.team_id,
                unlock_id=teamunlock.unlockanswer.unlock.compact_id,
                unlock=teamunlock.unlockanswer.unlock.text,
                unlockanswer_id=teamunlock.unlockanswer.id,
                unlockanswer=teamunlock.unlockanswer.guess,
                hint_id=hint.compact_id,
                hint=hint.text,
            )

    for hint in obsoleted:
        for th in models.TeamHint.objects.filter(
            team_puzzle_progress=tpp,
            hint=hint,
            obsoleted_by=None,
        ):
            th.obsoleted_by = teamunlock
            th.save()
            logger.info(
                'Progress: unlock obsoleted hint',
                puzzle_id=tpp.puzzle.id,
                puzzle_title=tpp.puzzle.title,
                team_id=tpp.team_id,
                unlock_id=teamunlock.unlockanswer.unlock.compact_id,
                unlock=teamunlock.unlockanswer.unlock.text,
                unlockanswer_id=teamunlock.unlockanswer.id,
                unlockanswer=teamunlock.unlockanswer.guess,
                hint_id=hint.compact_id,
                hint=hint.text,
            )


# Invalidate the cache of a guess's team when the team members change.
@receiver(m2m_changed, sender=Team.members.through)
def members_changed(sender, instance, action, pk_set, **kwargs):
    User = get_user_model()
    if action == 'post_add':
        users = User.objects.filter(pk__in=pk_set)
        guesses = models.Guess.objects.filter(by__in=users)
        count = guesses.update(by_team=instance, correct_current=False)
        if count > 0:
            logger.warning(
                'Progress: new team member potentially invalidated correctness',
                num_potentially_affected_guesses=count,
                team_id=instance.id,
                # django API does not specify the type of pk_set; fix it as a list for serialisability
                user_ids=list(pk_set),
            )


@post_save_handler(models.TeamPuzzleProgress)
def saved_teampuzzleprogress(sender, instance, raw, created, *args,  **kwargs):
    if raw:
        return  # nocover

    if not created:
        return

    tpp = instance

    hints = tpp.puzzle.hint_set.filter(start_after=None)
    compact_ids = [hint.compact_id for hint in hints]
    logger.info(
        'Progress: scheduling hints due to starting puzzle',
        puzzle_id=tpp.puzzle.id,
        puzzle_title=tpp.puzzle.title,
        team_id=tpp.team_id,
        hint_ids=compact_ids,
    )
    for hint in hints:
        try:
            with transaction.atomic():
                models.TeamHint(
                    team_puzzle_progress=tpp,
                    hint=hint,
                    unlocked_at=tpp.start_time + hint.time,
                ).save()
        except (ValidationError, IntegrityError) as e:
            logger.warning(
                'Integrity or Validation Error while creating TeamHint (probably due to close puzzle loads)',
                team_id=tpp.team_id,
                team_puzzle_progress_id=tpp.puzzle_id,
                exc_info=e,
            )


@post_save_handler(models.Hint, pass_old=True)
def save_hint(sender, instance, raw, *args, **kwargs):
    if raw:  # nocover
        return

    hint = instance
    old = instance._old_instance

    if old and hint.puzzle != old.puzzle:
        raise NotImplementedError

    tpps = models.TeamPuzzleProgress.objects.filter(
        puzzle=hint.puzzle
    ).select_related(
        'team', 'puzzle'
    ).prefetch_related(
        'teamhint_set__hint',
        'teamunlock_set__unlockanswer',
        'teamunlock_set__unlocked_by',
    ).seal()

    delete_from_tpps = []
    added_to_team_ids = []
    for tpp in tpps:
        unlocked_unlock_times = {
            tu.unlockanswer.unlock_id: tu.unlocked_by.given
            for tu in tpp.teamunlock_set.all()
        }
        unlocked_at = hint.unlocks_at(tpp.team, tpp, unlocked_unlocks=unlocked_unlock_times)
        if unlocked_at is None:
            # i.e. it depends on an unlock the team doesn't have
            delete_from_tpps.append(tpp)
            continue

        if hint.obsolete_for(tpp, unlocked_unlocks=unlocked_unlock_times):
            obsoleting = min(
                [
                    tu for tu in tpp.teamunlock_set.all()
                    if tu.unlockanswer.unlock_id in {
                        hint['id'] for hint in hint.obsoleted_by.all().values('id')
                    }
                ],
                key=lambda tu: tu.unlocked_by.given,
                default=None,
            )
        else:
            obsoleting = None

        if hint.start_after:
            dependent = min(
                [tu for tu in tpp.teamunlock_set.all() if tu.unlockanswer.unlock_id == hint.start_after_id],
                key=lambda tu: tu.unlocked_by.given,
                default=None,
            )
        else:
            dependent = None

        # Note update_or_create calls `save` on the instance if doing an update, so signals are fired.
        models.TeamHint.objects.update_or_create(
            team_puzzle_progress=tpp,
            hint=hint,
            defaults=dict(
                started_by=dependent,
                obsoleted_by=obsoleting,
                unlocked_at=unlocked_at,
            )
        )
        added_to_team_ids.append(tpp.team_id)

    logger.warning(
        'Progress: potentially scheduled new hints due to updated hint',
        puzzle_id=hint.puzzle_id,
        puzzle_title=hint.puzzle.title,
        team_count=len(added_to_team_ids),
        team_ids=added_to_team_ids,
        hint_id=hint.compact_id,
        hint=hint.text,
    )

    _, counts = models.TeamHint.objects.filter(team_puzzle_progress__in=delete_from_tpps, hint=hint).delete()
    if counts.get('hunts.TeamHint', 0) > 0:
        logger.warning(
            'Progress: withdrew hints from teams due to updated hint',
            team_count=counts['hunts.TeamHint'],
            puzzle_id=hint.puzzle_id,
            puzzle_title=hint.puzzle.title,
            hint_id=hint.compact_id,
            hint=hint.text,
        )


@receiver(m2m_changed, sender=models.Hint.obsoleted_by.through)
def hint_obsoleted_by_changed(sender, instance, action, pk_set, **kwargs):
    hint = instance
    # mapping of tpp ID -> TeamHint for this hint
    teamhints = {
        th.team_puzzle_progress_id: th
        for th in models.TeamHint.objects.filter(hint=hint)
    }
    # TODO the prefetch queryset can be limited to the first result after django 4.2
    progresses = models.TeamPuzzleProgress.objects.filter(puzzle=hint.puzzle).prefetch_related(
        Prefetch(
            'teamunlock_set',
            queryset=models.TeamUnlock.objects.filter(
                unlockanswer__unlock__in=hint.obsoleted_by.all()
            ).order_by('unlocked_by__given')
        )
    ).seal()
    now = timezone.now()
    if action == 'post_add':
        for tpp in progresses:
            unlocked_by = tpp.teamunlock_set.first()
            if unlocked_by:
                if teamhint := teamhints.get(tpp.id):
                    teamhint.obsoleted_by = unlocked_by
                    teamhint.save()

                    logger.warning(
                        'Progress: obsoleted scheduled or revealed hint',
                        puzzle_id=hint.puzzle_id,
                        puzzle_title=hint.puzzle.title,
                        team_id=tpp.team_id,
                        hint_id=hint.compact_id,
                        hint=hint.text,
                        revealed=teamhint.unlocked_at < now,
                        revealed_at=teamhint.unlocked_at,
                        obsoleted_by=unlocked_by,
                    )
    elif action == 'post_remove':
        for tpp in progresses:
            unlocked_by = tpp.teamunlock_set.first()
            # unlocked_by may now be None; we still want to set obsoleted_by to None then.
            if teamhint := teamhints.get(tpp.id):
                teamhint.obsoleted_by = unlocked_by
                teamhint.save()

                if not unlocked_by:
                    logger.warning(
                        'Progress: scheduled or revealed hint un-obsoleted',
                        puzzle_id=hint.puzzle_id,
                        puzzle_title=hint.puzzle.title,
                        team_id=tpp.team_id,
                        hint_id=hint.compact_id,
                        hint=hint.text,
                        revealed=teamhint.unlocked_at < now,
                        revealed_at=teamhint.unlocked_at,
                    )
                else:
                    logger.warning(
                        "Progress: scheduled or revealed hint's obsoletion changed",
                        puzzle_id=hint.puzzle_id,
                        puzzle_title=hint.puzzle.title,
                        team_id=tpp.team_id,
                        hint_id=hint.compact_id,
                        hint=hint.text,
                        revealed=True,
                        revealed_at=teamhint.unlocked_at,
                        obsoleted_by=unlocked_by,
                    )


@post_delete_handler(models.TeamUnlock)
def deleted_teamunlock(sender, instance, *args, **kwargs):
    teamunlock = instance
    # TODO: this incurs at least one query each time this handler runs, which runs many times in some situations
    #  like if the unlock itself is deleted, and/or teams had several guesses unlocking it multiple times

    unlock = teamunlock.unlockanswer.unlock
    progress = teamunlock.team_puzzle_progress

    # Get the TeamUnlock which, if it exists, corresponds to the first guess which still unlocks the unlock that was
    # unlocked by the deleted one.
    still_unlocked_by = models.TeamUnlock.objects.filter(
        team_puzzle_progress=progress, unlockanswer__unlock=unlock
    ).exclude(id=teamunlock.id).order_by('unlocked_by__given').first()

    if still_unlocked_by:
        logger.warning(
            "Progress: team lost a route to an unlock, but it is still unlocked",
            puzzle_id=progress.puzzle_id,
            puzzle_title=progress.puzzle.title,
            team_id=progress.team_id,
            unlock_id=unlock.compact_id,
            unlock=unlock.text,
        )
        # Update `started_by` and the unlocked at time to reflect the new guess
        models.TeamHint.objects.filter(hint__start_after=unlock).update(
            started_by=still_unlocked_by,
            unlocked_at=Subquery(models.Hint.objects.filter(pk=OuterRef('hint_id')).values('time')[:1]) + still_unlocked_by.unlocked_by.given
        )
        # Move over record of obsoletion
        models.TeamHint.objects.filter(team_puzzle_progress=progress, hint__obsoleted_by=unlock).update(obsoleted_by=still_unlocked_by)

        # NOTE: `update` does not fire signals. However, in this situation we don't need to update the player
        #  because the hint is still visible. Since the frontend is not informed of the link between the relevant guess
        #  and the hint, nothing needs to change.
    else:
        logger.warning(
            "Progress: team lost an unlock",
            puzzle_id=progress.puzzle_id,
            puzzle_title=progress.puzzle.title,
            team_id=progress.team_id,
            unlock_id=unlock.compact_id,
            unlock=unlock.text,
        )
        # mimic ON DELETE CASCADE
        was_dependent = models.TeamHint.objects.filter(
            team_puzzle_progress=teamunlock.team_puzzle_progress,
            hint__start_after=unlock
        )
        was_dependent.delete()
        was_obsoleted = models.TeamHint.objects.filter(
            team_puzzle_progress=teamunlock.team_puzzle_progress,
            hint__obsoleted_by=unlock
        )
        # mimic ON DELETE SET NULL, but send signals.
        for teamhint in was_obsoleted:
            teamhint.obsoleted_by = None
            teamhint.save()


@hybrid_save_handler(models.TeamHint)
def saved_teamhint(old, sender, instance, raw, *args, **kwargs):
    if raw:  # nocover:
        return

    # Always trigger the hint scheduler and let it decide what to do.
    # We could be smarter about this - it would possibly be neater to not go via the scheduler when hints are already
    # visible. Note that the websocket already takes care of sending a message when the hint has been accepted. This is
    # not necessary ordinarily, but allows the scheduler to cut down how many notifications it must send when starting
    # up, as it can assume they were sent by the websocket process.
    hint_scheduler.HintScheduler.schedule_hint_notifications([instance])


@post_delete_handler(models.TeamHint)
def deleted_teamhint(sender, instance, *args, **kwargs):
    hint_scheduler.HintScheduler.cancel_hint_notifications([instance])
