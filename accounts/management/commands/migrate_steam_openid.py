# Copyright (C) 2023 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.providers.openid.provider import OpenIDProvider
from allauth.socialaccount.providers.steam.provider import SteamOpenIDProvider
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Migrates Steam OpenID accounts to/from the SteamOpenIDProvider."

    def add_arguments(self, parser):
        parser.add_argument('-r', '--reverse', action='store_true', dest='reverse', help='Reverse migration (back to generic OpenIDProvider')

    def handle(self, *args, **options):
        if options['reverse']:
            self.migrate_backwards()
        else:
            self.migrate_forwards()

    @staticmethod
    def migrate_forwards():
        for account in SocialAccount.objects.filter(provider=OpenIDProvider.id, uid__startswith='https://steamcommunity.com/openid/id/'):
            account.uid = account.uid.split('/')[-1]
            account.provider = SteamOpenIDProvider.id
            account.save()

    @staticmethod
    def migrate_backwards():
        for account in SocialAccount.objects.filter(provider=SteamOpenIDProvider.id):
            account.uid = f'https://steamcommunity.com/openid/id/{account.uid}'
            account.provider = OpenIDProvider.id
            account.extra_data = '{}'
            account.save()
