# Copyright (C) 2023 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

from channels.management.commands.runworker import Command as BaseCommand

from hunts import hint_scheduler


class Command(BaseCommand):
    def handle(self, *args, **options):
        if hint_scheduler.CHANNEL_NAME in options["channels"]:
            hint_scheduler.HintScheduler.setup()
        super().handle(*args, **options)
