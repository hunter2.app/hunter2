# Copyright (C) 2023 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
import structlog
from django.db import connections
from django.db.models.signals import pre_save, pre_delete, post_delete
from django.utils import timezone
from django_tenants.utils import get_tenant_database_alias

from hunts import models
from hunts.websocket.consumers import HuntWebsocket, PuzzleEventWebsocket, UpdateStreamWebsocket
from .helpers import hybrid_save_handler

logger = structlog.get_logger(__name__)


@hybrid_save_handler(models.Announcement)
def saved_announcement(old, sender, announcement, raw, *args, **kwargs):
    if raw:  # nocover
        return
    # Announcement doesn't have a foreign key to the tenant, but the ID is required to build the channel layer keys
    event = connections[get_tenant_database_alias()].tenant
    HuntWebsocket.send_announcement_msg(event, announcement.puzzle, announcement)


def deleted_announcement(sender, instance, *args, **kwargs):
    # Announcement doesn't have a foreign key to the tenant, but the ID is required to build the channel layer keys
    event = connections[get_tenant_database_alias()].tenant
    HuntWebsocket.send_delete_announcement_msg(event, instance.puzzle, instance)


pre_save.connect(saved_announcement, sender=models.Announcement)
pre_delete.connect(deleted_announcement, sender=models.Announcement)


@hybrid_save_handler(models.TeamPuzzleProgress)
def saved_teampuzzleprogress(old, sender, progress, raw, *args, **kwargs):
    if raw:  # nocover
        return

    if progress.solved_by and (not old or not old.solved_by):
        PuzzleEventWebsocket.send_solved(progress)
        UpdateStreamWebsocket.send_puzzle_finished(progress.puzzle, progress.team)
        if progress.puzzle.episode.finished(progress.team):
            UpdateStreamWebsocket.send_episode_finished(progress.puzzle.episode, progress.team)


@hybrid_save_handler(models.Guess)
def saved_guess(old, sender, guess, raw, *args, **kwargs):
    # Do not trigger unless this was a newly created guess.
    # Note this means an admin modifying a guess will not trigger anything.
    if raw:  # nocover
        return
    if old:
        return

    logger.debug(
        'Saved guess, calling websocket',
        guess=guess.id,
    )
    PuzzleEventWebsocket.send_new_guess(guess)


@hybrid_save_handler(models.TeamUnlock)
def saved_teamunlock(old, sender, teamunlock, raw, *args, **kwargs):
    if raw:  # nocover:
        return

    if not teamunlock.unlockanswer.unlock.hidden or models.TeamHint.objects.visible().filter(
            hint__start_after=teamunlock.unlockanswer.unlock,
            team_puzzle_progress=teamunlock.team_puzzle_progress,
    ).exists():
        PuzzleEventWebsocket.send_new_unlock(teamunlock)


def deleted_teamunlock(sender, instance, *args, **kwargs):
    teamunlock = instance

    # To avoid doing more than necessary, we don't explicitly check if the unlock is still unlocked.
    # The client deletes unlocks from its store if there are no guesses unlocking them and
    # determines what to display based on the contents of the unlock order which we are asserting
    # the state of anyway.
    # This could result in some unlocks remaining visible incorrectly if there is a network interruption,
    # where doing these checks would mean we can always send a "delete unlock" event to the client.
    # Since this is rare and we assume the team has usually seen the unlock and gained any benefit
    # or confusion (if it's being changed because it was wrong!) from it, this is probably OK.
    PuzzleEventWebsocket.send_delete_unlockguess(teamunlock)


@hybrid_save_handler(models.Unlock)
def saved_unlock(old, sender, unlock, raw, *args, **kwargs):
    if raw:  # nocover
        return
    if not old:
        return

    if unlock.puzzle != old.puzzle:
        raise ValueError("Cannot move unlocks between puzzles")
    # Get list of teams which can see this unlock
    team_unlocks = models.TeamUnlock.objects.filter(
        unlockanswer__unlock=unlock
    ).select_related(
        'team_puzzle_progress',
        'unlockanswer__unlock__puzzle__episode',
        'unlocked_by',
    ).prefetch_related(
        'team_puzzle_progress__teamunlock_set__unlockanswer',
    ).seal()

    for team_unlock in team_unlocks:
        PuzzleEventWebsocket.send_change_unlock(team_unlock)


def deleted_teamhint(sender, instance, *arg, **kwargs):
    teamhint = instance

    PuzzleEventWebsocket.send_delete_hint(teamhint.team_puzzle_progress.team_id, teamhint.hint)


@hybrid_save_handler(models.TeamHint)
def saved_teamhint(old, sender, instance, raw, **kwargs):
    if raw:  # nocover
        return

    teamhint = instance

    now = timezone.now()

    if old:
        # Main case: Transitioning unaccepted -> accepted
        if not old.accepted_at and teamhint.accepted_at:
            PuzzleEventWebsocket.send_new_hint(
                teamhint.team_puzzle_progress.team_id, teamhint.hint, True, bool(teamhint.obsoleted_by_id)
            )

        # Transitioning unlocked in past -> unlocked in future
        if old.unlocked_at < now and teamhint.unlocked_at > now:
            PuzzleEventWebsocket.send_delete_hint(
                teamhint.team_puzzle_progress.team_id, teamhint.hint
            )

        # Transitioning obsolete -> not obsolete
        if old.obsoleted_by_id and not teamhint.obsoleted_by_id and teamhint.unlocked_at > now:
            PuzzleEventWebsocket.send_delete_hint(
                teamhint.team_puzzle_progress.team_id, teamhint.hint
            )


post_delete.connect(deleted_teamunlock, sender=models.TeamUnlock)
pre_delete.connect(deleted_teamhint, sender=models.TeamHint)
