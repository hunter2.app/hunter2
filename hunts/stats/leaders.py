# Copyright (C) 2020 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
from dataclasses import dataclass
from datetime import datetime

from schema import And, Schema

from teams.models import Team, SizeCategory
from .abstract import AbstractGenerator, CannotGenerate
from ..models import Episode


@dataclass
class FinishData:
    team: Team
    time: datetime
    size: int
    size_category: SizeCategory

    def __lt__(self, other):
        return self.team == other.team and (self.time, self.size) < (other.time, other.size)

    @classmethod
    def max(cls, a, b):
        return FinishData(a.team, max(a.time, b.time), max(a.size, b.size), max(a.size_category, b.size_category))


class LeadersGenerator(AbstractGenerator):
    """
    Generates a table of the hunt leaders

    Leaders are defined as the first teams to have finished all puzzles in all winning episodes.
    """
    title = 'Leaderboard'
    version = 2

    schema = Schema({
        'by_team': {
            And(str, len): {
                'size_category': And(str, len),
                'finish_time': datetime,
            },
        },
        'top': [
            (
                int,
                And(str, len),
                {
                    'size_category': And(str, len),
                    'finish_time': datetime,
                }
            ),
        ],
    })

    def __init__(self, number=10, **kwargs):
        super().__init__(**kwargs)
        self.number = number

    def generate(self):
        if self.episode is not None:
            episodes = [self.episode]
        else:
            episodes = Episode.objects.filter(no_stats=False, winning=True)
            # If any "winning" episodes have anything in their sequel graph which are also "winning" then discount them since completion times for the sequels
            # must be higher
            redundant = [e.id for e in episodes if len([s for s in e.all_sequels() if s.winning])]
            episodes = episodes.exclude(pk__in=redundant)
            if episodes.count() == 0:
                raise CannotGenerate("Event has no winning episodes")

        # We have at least one episode, copy the first episode's finish times
        finished_times = episodes[0].finished_times(with_sizes=True)
        team_finish_data = {
            team: FinishData(team, time, team.max_size, team.size_category)
            for team, time in finished_times
            if not self.sizes_relevant() or team.size_category is not None and (
                    self.size_category is None or
                    team.size_category == self.size_category
            )
        }
        # Iterate remaining episodes to find people who finished later than the existing time or did not finish
        for episode in episodes[1:]:
            old_data = team_finish_data
            team_finish_data = {}
            for team, time in episode.finished_times(with_sizes=True):
                if team in old_data:
                    old_entry = old_data[team]
                    team_finish_data[team] = FinishData.max(old_entry, FinishData(team, time, team.max_size, team.size_category))

        sorted_finish_data = list(sorted(team_finish_data.values()))
        top = [
            (
                position,
                finish_data.team.get_display_name(),
                {
                    'size_category': finish_data.size_category.display_label() if finish_data.size_category else None,
                    'finish_time': finish_data.time,
                },
            )
            for position, finish_data in enumerate(sorted_finish_data, start=1)
        ][:self.number]
        by_team = {
            finish_data.team.id: {
                'position': position,
                'payload': {
                    'size_category': finish_data.size_category.display_label() if finish_data.size_category else None,
                    'finish_time': finish_data.time,
                }
            } for position, finish_data in enumerate(sorted_finish_data, start=1)
        }

        return {
            'by_team': by_team,
            'top': top,
        }

    def render_data(self, data, team=None, user=None):
        data = {
            'teams': self._add_extra(data['by_team'], data['top'], team, 'payload'),
        }
        return super().render_data(data, team=team, user=user)
