# Copyright (C) 2022 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

import builtins
import importlib
import logging  # noqa: PRI001
import os
import random
import sys

from faker import Faker
from xmlrunner.extra.djangotestrunner import XMLTestRunner


class PytestTestRunner(XMLTestRunner):
    """Runs pytest to discover and run tests.

    Source: https://pytest-django.readthedocs.io/en/latest/faq.html#how-can-i-use-manage-py-test-with-pytest-django
    """

    def __init__(self, verbosity=1, failfast=False, keepdb=False, **kwargs):
        self.verbosity = verbosity
        self.failfast = failfast
        self.keepdb = keepdb

        super().__init__(**kwargs)

    @classmethod
    def add_arguments(cls, parser):
        super().add_arguments(parser)

    @staticmethod
    def translate_module_reference(label):
        """manage.py test expects labels of the form sub.module.Class.test_method. This translates this format to the
        pytest format of sub/module.py::Class::test_method"""

        # This logic is bad, but it's exactly what the django test runner does!

        parts = label.split('.')
        parts_left = parts[:]
        parts_right = []
        module = None
        # We don't know whether the last components are parts of the submodule path or methods or what, so we have to
        # do this by trial and error
        while parts_left:
            try:
                module_name = '.'.join(parts_left)
                module = importlib.import_module(module_name)
                break
            except ImportError:
                pass
            parts_right.insert(0, parts_left.pop())

        if module:
            return module.__file__ + '::' + '::'.join(parts_right)

        # if this doesn't work, maybe the unchanged label is supported after all; return it unchanged
        return label

    def run_tests(self, test_labels, extra_tests=None, **kwargs):
        """Run pytest and return the exitcode.

        It translates some of Django's test command option to pytest's.
        """
        import pytest

        argv = []
        if self.verbosity == 0:
            argv.append('--quiet')
        if self.verbosity == 2:
            argv.append('--verbose')
        if self.verbosity == 3:
            argv.append('-vv')
        if self.failfast:
            argv.append('--exitfirst')
        if self.keepdb:
            argv.append('--reuse-db')

        for label in test_labels:
            argv.append(self.translate_module_reference(label))

        # Disable non-critial logging for test runs
        logging.disable(logging.CRITICAL)

        # Seed the random generators extracting the seed used
        # https://stackoverflow.com/a/5012617/393688
        default_seed = random.randrange(sys.maxsize)  # nosec random is fine for testing
        random_seed = os.getenv('H2_TEST_SEED', default_seed)
        random.seed(random_seed)
        Faker.seed(random_seed)
        print(f'Testing Seed: {random_seed}')
        print("Run pytest directly (e.g. through the h2-test alias in h2tools.sh) for full access to pytest's command "
              "line arguments")

        return pytest.main(argv)


def mock_inputs(inputs):  # nocover
    """
    Decorator to temporarily replace input/getpass to allow interactive
    createsuperuser.
    """

    def inner(test_function):
        def wrap_input(*args):
            def mock_input(prompt):
                for key, value in inputs.items():
                    if key in prompt.lower():
                        return value
                return ""

            old_input = builtins.input
            builtins.input = mock_input
            try:
                test_function(*args)
            finally:
                builtins.input = old_input

        return wrap_input

    return inner


# Adapted from:
# https://github.com/django/django/blob/7588d7e439a5deb7f534bdeb2abe407b937e3c1a/tests/auth_tests/test_management.py
class MockTTY:
    """
    A fake stdin object that pretends to be a TTY to be used in conjunction
    with mock_inputs.
    """

    def isatty(self):  # nocover
        return True
