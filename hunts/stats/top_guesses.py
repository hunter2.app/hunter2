# Copyright (C) 2020 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime

from django.contrib.auth import get_user_model
from django.db.models import Count, Q, Max
from schema import And, Schema, Optional

from teams.models import Team, TeamRole, SizeCategory
from .abstract import AbstractGenerator


class TopGuessesGenerator(AbstractGenerator):
    """
    Generates table of the top guessing users and teams

    Guess counts are aggregated by user and team and those who submitted the most guesses are included in the output.
    """
    title = 'Most Guesses'
    version = 2

    schema = Schema({
        'by_team': {
            And(str, len): Schema({
                'position': int,
                'payload': {
                    'size_category': Optional(And(str, len)),
                    'guess_count': datetime,
                },
            }),
        },
        'by_user': {
            And(str, len): Schema({
                'position': int,
                'guess_count': datetime,
            }),
        },
        'top_teams': [
            Schema((
                int,
                And(str, len),
                {
                    'size_category': Optional(And(str, len)),
                    'guess_count': int,
                }
            )),
        ],
        'top_users': [
            Schema((
                int,
                And(str, len),
                datetime,
            )),
        ],
    })

    def __init__(self, number=10, **kwargs):
        super().__init__(**kwargs)
        self._number = number

    def generate(self):
        User = get_user_model()
        guesses_filter = Q(guess__for_puzzle__episode__no_stats=False, guess__late=False)
        if self.episode is not None:
            guesses_filter &= Q(guess__for_puzzle__episode=self.episode)
        teams = Team.objects.filter(
            disqualified=False,
            role=TeamRole.PLAYER
        ).annotate(
            guess_count=Count('guess', filter=guesses_filter, distinct=True),
            max_relevant_size=Max('teampuzzleprogress__team_size')
        ).order_by('-guess_count')
        users = User.objects.filter(
            teams__disqualified=False,
            teams__role=TeamRole.PLAYER
        ).annotate(
            guess_count=Count('guess', filter=guesses_filter)
        ).order_by('-guess_count')
        team_size_categories = {
            team.id: SizeCategory.for_size(team.max_relevant_size if team.max_relevant_size else team.members.count())
            for team in teams
        }
        filtered_teams = [team for team in teams if self.category_matches(team_size_categories[team.id])]

        return {
            'by_team': {
                team.id: {
                    'position': position,
                    'payload': {
                        'guess_count': team.guess_count,
                        'size_category': team_size_categories[team.id].display_label() if team_size_categories[team.id] else None,
                    },
                } for position, team in enumerate(filtered_teams, start=1)
            },
            'top_teams': [
                (
                    position,
                    team.get_display_name(),
                    {
                        'size_category': team_size_categories[team.id].display_label() if team_size_categories[team.id] else None,
                        'guess_count': team.guess_count,
                    }
                ) for position, team in enumerate(filtered_teams[:self._number], start=1)
            ],
            'by_user': {
                user.id: {
                    'position': position,
                    'guess_count': user.guess_count,
                } for position, user in enumerate(users, start=1)
            },
            'top_users': [
                (
                    position,
                    user.username,
                    user.guess_count,
                ) for position, user in enumerate(users[:self._number], start=1)
            ],
        }

    def render_data(self, data, team=None, user=None):
        data = {
            'teams': self._add_extra(data['by_team'], data['top_teams'], team, 'payload'),
            'users': self._add_extra(data['by_user'], data['top_users'], user, 'guess_count'),
        }
        return super().render_data(data, team=team, user=user)
