# Copyright (C) 2020 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

from datetime import timedelta

import pytest
from django.core.cache import caches
from django.db.models.signals import post_save
from django.test import SimpleTestCase
from django.utils import timezone
from faker import Faker
from schema import Schema

from accounts.factories import UserFactory
from events.models import Event
from teams.factories import TeamFactory, TeamMemberFactory, SizeCategoryFactory
from teams.models import TeamRole
from . import PuzzleTimesGenerator, SolveDistributionGenerator, CompletionGenerator
from .abstract import AbstractGenerator, CannotGenerate
from .leaders import LeadersGenerator
from .top_guesses import TopGuessesGenerator
from .totals import TotalsGenerator
from ..factories import GuessFactory, PuzzleFactory, EpisodeFactory, TeamPuzzleProgressFactory
from ..models import TeamPuzzleProgress, Answer


@pytest.fixture
def irrelevant_bystanders(event):
    before_end = event.end_date - timedelta(hours=1)
    after_end = event.end_date + timedelta(days=7)
    players = (
        (TeamMemberFactory(team__role=TeamRole.PLAYER), True, after_end),
        (TeamMemberFactory(team__disqualified=True, team__role=TeamRole.PLAYER), False, before_end),
        (TeamMemberFactory(team__role=TeamRole.ADMIN), False, before_end),
    )

    # every time an answer is created, guess on the corresponding puzzle with appropriate parameters.
    # this can't be registered to puzzle creation, because when it's created it won't have any answers.
    def create_and_guess(sender, instance, created, **kwargs):
        if created:
            for player, late, time in players:
                TeamPuzzleProgressFactory(team=player.teams.get(), puzzle=instance.for_puzzle, start_time=time - timedelta(minutes=1))
                GuessFactory(by=player, for_puzzle=instance.for_puzzle, late=late, correct=True, given=time)

    post_save.connect(create_and_guess, sender=Answer, dispatch_uid=(p.id for p in players))

    yield

    post_save.disconnect(sender=Answer, dispatch_uid=(p.id for p in players))


class MockStat(AbstractGenerator):
    id = 'mock'
    title = 'Mock Statistic'
    version = 2
    temp_cache_timeout = 0

    schema = Schema(dict)

    def generate(self, episode=None):
        # This generator always returns different data, so we can test cache hit/miss
        fake = Faker()
        return fake.pydict()


class StatCacheTests(SimpleTestCase):
    def setUp(self):
        # We don't want to depend on a database, but we need an event object with an ID
        self.event = Event(id=1)
        self.stat = MockStat(self.event, size_category=None)
        # We need to use a real cache for these tests, which will not be cleared for us by the testcase
        caches['stats'].delete(self.stat.cache_key(), version=self.stat.version)

    def test_cache_hit(self):
        data = self.stat.data(True)
        self.assertEqual(data, self.stat.data(True))

    def test_class_change_cache_miss(self):
        class OtherStat(MockStat):
            pass
        other_stat = OtherStat(self.event, size_category=None)
        data = self.stat.data(True)
        self.assertNotEqual(data, other_stat.data(True))

    def test_version_change_cache_miss(self):
        data = self.stat.data(True)
        self.stat.version = 3
        self.assertNotEqual(data, self.stat.data(True))

    def test_dont_use_cache(self):
        data = self.stat.data(False)
        self.assertNotEqual(data, self.stat.data(True))


@pytest.fixture
def tpps_for_sizes(event):
    puzzle = PuzzleFactory()
    for i in range(10):
        team = TeamFactory(role=TeamRole.PLAYER)
        team.members.add(*UserFactory.create_batch(i))
        TeamPuzzleProgressFactory(puzzle=puzzle, team=team).team.members.count()


class TestBaseMethods:
    def test_correct_sized_tpps_no_categories(self, event, tpps_for_sizes):
        """When there are no categories, None category should result in all tpps"""
        stat = MockStat(event=event, size_category=None)
        assert sorted([tpp.team.members.count() for tpp in stat.get_correct_sized_tpps()]) == list(range(1, 11))

    def test_correct_sized_tpps_use_category(self, event, tpps_for_sizes):
        """When a category is supplied, tpps should be filtered to that category only"""
        SizeCategoryFactory(max=3)
        cat = SizeCategoryFactory(max=6)
        stat = MockStat(event=event, size_category=cat)
        assert sorted([tpp.team.members.count() for tpp in stat.get_correct_sized_tpps()]) == list(range(4, 7))

    def test_correct_sized_tpps_use_unlimited_category(self, event, tpps_for_sizes):
        """When an unlimited category is supplied, tpps should be filtered to that category only"""
        SizeCategoryFactory(max=3)
        SizeCategoryFactory(max=6)
        cat = SizeCategoryFactory(max=None)
        stat = MockStat(event=event, size_category=cat)
        assert sorted([tpp.team.members.count() for tpp in stat.get_correct_sized_tpps()]) == list(range(7, 11))

    def test_correct_sized_tpps_one_limited_category(self, event, tpps_for_sizes):
        """When one limited category exists, None category should limit to that one"""
        SizeCategoryFactory(max=7)
        stat = MockStat(event=event, size_category=None)
        assert sorted([tpp.team.members.count() for tpp in stat.get_correct_sized_tpps()]) == list(range(1, 8))

    def test_correct_sized_tpps_one_unlimited_category(self, event, tpps_for_sizes):
        """When one unlimited category exists, None category should result in all tpps"""
        SizeCategoryFactory(max=None)
        stat = MockStat(event=event, size_category=None)
        assert sorted([tpp.team.members.count() for tpp in stat.get_correct_sized_tpps()]) == list(range(1, 11))

    def test_correct_sized_tpps_unlimited_and_others(self, event, tpps_for_sizes):
        """When one unlimited category exists, None category should result in all tpps"""
        SizeCategoryFactory(max=3)
        SizeCategoryFactory(max=6)
        SizeCategoryFactory(max=None)
        stat = MockStat(event=event, size_category=None)
        assert sorted([tpp.team.members.count() for tpp in stat.get_correct_sized_tpps()]) == list(range(1, 7))

    def test_sizes_relevant_none(self, event):
        assert not AbstractGenerator.sizes_relevant()

    def test_sizes_relevant_one_size(self, event):
        cat = SizeCategoryFactory(max=None)
        assert not AbstractGenerator.sizes_relevant()
        cat.max = 2
        cat.save()
        assert AbstractGenerator.sizes_relevant()

    def test_sizes_relevant_multiple(self, event):
        SizeCategoryFactory(max=2)
        SizeCategoryFactory(max=None)
        assert AbstractGenerator.sizes_relevant()

    def test_largest_size_restriction(self, event):
        SizeCategoryFactory(max=2)
        SizeCategoryFactory(max=None)
        assert AbstractGenerator.largest_size_restriction() == 2


class TestLeaders:
    def test_event_leaders(self, event):
        puzzle = PuzzleFactory(episode__winning=True)
        players = TeamMemberFactory.create_batch(4, team__role=TeamRole.PLAYER)
        players[3].teams.get().members.add(*UserFactory.create_batch(2))
        small, large = SizeCategoryFactory.create_batch(2)

        now = timezone.now()
        # Players finish the puzzle in order 1-4
        guesses = [GuessFactory(by=player, for_puzzle=puzzle, correct=True, given=now - timedelta(minutes=4 - i)) for i, player in enumerate(players)]
        # Player 4 also guessed wrong
        GuessFactory(by=players[3], for_puzzle=puzzle, correct=False, given=now - timedelta(minutes=5))

        data = LeadersGenerator(event=event, number=3, size_category=None).generate()
        LeadersGenerator.schema.is_valid(data)

        # "Top" has 3 entries, in order, with the correct times
        assert len(data['top']) == 3
        for i, player in enumerate(players[:3]):
            assert data['top'][i] == (i + 1, player.team_at(event).get_display_name(), {'size_category': small.label, 'finish_time': guesses[i].given})
        # The fourth team appears correctly in the indexed data
        team = players[3].team_at(event).id
        assert team in data['by_team']
        assert data['by_team'][team]['position'] == 4
        assert data['by_team'][team]['payload'] == {'size_category': large.label, 'finish_time': guesses[3].given}

    def test_episode_leaders(self, event):
        puzzle1 = PuzzleFactory(episode__winning=False)
        puzzle2 = PuzzleFactory(episode__winning=True, episode__prequels=puzzle1.episode)
        players = TeamMemberFactory.create_batch(4, team__role=TeamRole.PLAYER)
        players[3].teams.get().members.add(*UserFactory.create_batch(2))
        small, large = SizeCategoryFactory.create_batch(2)
        now = timezone.now()
        # Players finish puzzle 1 in order 1-3
        guesses = [GuessFactory(by=player, for_puzzle=puzzle1, correct=True, given=now - timedelta(minutes=6 - i)) for i, player in enumerate(players)]
        # Players finish puzzle 2 in order 3-1
        for i, player in enumerate(reversed(players)):
            GuessFactory(by=player, for_puzzle=puzzle2, correct=True, given=now - timedelta(minutes=3 - i))

        data = LeadersGenerator(event=event, episode=puzzle1.episode, number=3, size_category=small).generate()
        LeadersGenerator.schema.is_valid(data)

        # "Top" has 3 entries, in order, with the correct times
        assert len(data['top']) == 3
        for i, player in enumerate(players[:-1]):
            assert data['top'][i] == (i + 1, player.team_at(event).get_display_name(), {'size_category': small.label, 'finish_time': guesses[i].given})

    def test_leaders_not_enough_players(self, event):
        puzzle = PuzzleFactory(episode__winning=True)
        players = TeamMemberFactory.create_batch(2, team__role=TeamRole.PLAYER)
        big_team = TeamFactory()
        big_team.members.add(*UserFactory.create_batch(2))
        small = SizeCategoryFactory(max=2)
        SizeCategoryFactory(max=None)
        now = timezone.now()
        # Players finish the puzzle in order 1-2
        guesses = [GuessFactory(by=player, for_puzzle=puzzle, correct=True, given=now - timedelta(minutes=2-i)) for i, player in enumerate(players)]

        data = LeadersGenerator(event=event, number=3, size_category=None).generate()
        LeadersGenerator.schema.is_valid(data)

        # "Top" has 2 entries, in order, with the correct times
        assert len(data['top']) == 2
        for i, player in enumerate(players):
            assert data['top'][i] == (
                i + 1, player.team_at(event).get_display_name(), {'size_category': small.label, 'finish_time': guesses[i].given}
            )

    def test_leaders_render_extra_data(self, event):
        team = TeamFactory.build(name='Team 4')
        team.id = 4
        small, large = SizeCategoryFactory.create_batch(2)

        data = {
            'by_team': {
                1: {'position': 1, 'payload': {'size_category': small.label, 'finish_time': 4}},
                2: {'position': 2, 'payload': {'size_category': small.label, 'finish_time': 3}},
                3: {'position': 3, 'payload': {'size_category': small.label, 'finish_time': 2}},
                4: {'position': 4, 'payload': {'size_category': small.label, 'finish_time': 1}},
            },
            'top': [
                (1, 'Team 1', {'size_category': small.label, 'finish_time': 4}),
                (2, 'Team 2', {'size_category': small.label, 'finish_time': 3}),
                (3, 'Team 3', {'size_category': small.label, 'finish_time': 2}),
            ],
        }

        render = LeadersGenerator(event=event, number=3, size_category=None).render_data(data, team=team, user=None)

        assert 'Team 4' in render

    def test_leaders_no_winning_episode(self, event):
        puzzle = PuzzleFactory(episode__winning=False)
        player = TeamMemberFactory(team__role=TeamRole.PLAYER)
        GuessFactory(by=player, for_puzzle=puzzle, correct=True)

        with pytest.raises(CannotGenerate):
            LeadersGenerator(event=event, size_category=None).generate()


class TestTopGuesses:
    def test_event_top_guesses(self, event):
        puzzle = PuzzleFactory()
        players = (  # Not using create_batch because we want some of the middle ones to not be on teams
            TeamMemberFactory(team__role=TeamRole.PLAYER),
            UserFactory(),
            TeamMemberFactory(team__role=TeamRole.PLAYER),
            UserFactory(),
            TeamMemberFactory(team__role=TeamRole.PLAYER),
        )

        team2 = TeamFactory(members=(players[1], players[3]))
        team2.members.add(*UserFactory.create_batch(2))
        small, large = SizeCategoryFactory.create_batch(2)

        for i, player in enumerate(players):
            GuessFactory.create_batch(5 - i, by=player, for_puzzle=puzzle)

        data = TopGuessesGenerator(event=event, number=3, size_category=None).generate()
        TopGuessesGenerator.schema.is_valid(data)

        # Player 2 and 4 are on the same team, so they win by team
        assert len(data['top_teams']) == 3
        assert data['top_teams'][0] == (1, team2.get_display_name(), {'size_category': large.label, 'guess_count': 6})
        assert data['top_teams'][1] == (2, players[0].team_at(event).get_display_name(), {'size_category': small.label, 'guess_count': 5})
        assert data['top_teams'][2] == (3, players[2].team_at(event).get_display_name(), {'size_category': small.label, 'guess_count': 3})
        assert len(data['top_users']) == 3
        for i, player in enumerate(players[:3]):
            assert data['top_users'][i] == (i + 1, player.get_display_name(), 5 - i)
        # The fourth and fifth users, and fourth team appear correctly in the indexed data
        team5 = players[4].team_at(event).id
        assert team5 in data['by_team']
        assert data['by_team'][team5]['position'] == 4
        assert data['by_team'][team5]['payload']['size_category'] == small.label
        assert data['by_team'][team5]['payload']['guess_count'] == 1
        for i, player in enumerate(players[3:]):
            assert player.id in data['by_user']
            assert data['by_user'][player.id]['position'] == i + 4
            assert data['by_user'][player.id]['guess_count'] == 2 - i

    def test_episode_top_guesses(self, event):
        puzzles = PuzzleFactory.create_batch(2)
        players = TeamMemberFactory.create_batch(3, team__role=TeamRole.PLAYER)
        small, large = SizeCategoryFactory.create_batch(2)

        # Create guesses such that players won episode 1 in order 1-3 but episode 2 in order 3-1
        for i, player in enumerate(players):
            GuessFactory.create_batch(3 - i, by=player, for_puzzle=puzzles[0])
            GuessFactory.create_batch(i * 2 + 1, by=player, by_team=player.teams.get(), for_puzzle=puzzles[1])

        data = TopGuessesGenerator(event=event, episode=puzzles[0].episode, number=3, size_category=None).generate()
        TopGuessesGenerator.schema.is_valid(data)

        # "Top" has 3 entries, in order
        assert len(data['top_teams']) == 3
        for i, player in enumerate(players):
            assert data['top_teams'][i] == (i + 1, player.teams.get().get_display_name(), {'size_category': small.label, 'guess_count': 3 - i})

        assert len(data['top_users']) == 3
        for i, player in enumerate(players):
            assert data['top_users'][i] == (i + 1, player.get_display_name(), 3 - i)

    def test_top_guesses_unlimited_category(self, event):
        puzzle = PuzzleFactory()
        players = UserFactory.create_batch(2)
        team = TeamFactory(members=players, role=TeamRole.PLAYER)
        team2  = TeamFactory(members=UserFactory.create_batch(4), role=TeamRole.PLAYER)
        print(team2)
        cat = SizeCategoryFactory(max=2)
        SizeCategoryFactory(max=None)
        for i, player in enumerate(players):
            GuessFactory.create_batch(2 - i, by=player, for_puzzle=puzzle)

        data = TopGuessesGenerator(event=event, number=3, size_category=None).generate()
        TopGuessesGenerator.schema.is_valid(data)

        # "Top Users" has 3 entries, in order
        assert len(data['top_users']) == 3
        for i, player in enumerate(players):
            assert data['top_users'][i] == (i + 1, player.get_display_name(), 2 - i)
        # "Top Teams" has 1 entry
        assert len(data['top_teams']) == 1
        assert data['top_teams'][0] == (1, team.get_display_name(), {'size_category': cat.label, 'guess_count': 3})

    def test_top_guesses_not_enough_players(self, event):
        puzzle = PuzzleFactory()
        players = UserFactory.create_batch(2)
        team = TeamFactory(members=players, role=TeamRole.PLAYER)
        for i, player in enumerate(players):
            GuessFactory.create_batch(2 - i, by=player, for_puzzle=puzzle)

        data = TopGuessesGenerator(event=event, number=3, size_category=None).generate()
        TopGuessesGenerator.schema.is_valid(data)

        # "Top Users" has 2 entries, in order
        assert len(data['top_users']) == 2
        for i, player in enumerate(players):
            assert data['top_users'][i] == (i + 1, player.get_display_name(), 2 - i)
        # "Top Teams" has 1 entry
        assert len(data['top_teams']) == 1
        assert data['top_teams'][0] == (1, team.get_display_name(), {'size_category': None, 'guess_count': 3})

    def test_episode_excluded(self, event):
        puzzle1 = PuzzleFactory()
        puzzle2 = PuzzleFactory(episode__no_stats=True)
        players = TeamMemberFactory.create_batch(4, team__role=TeamRole.PLAYER)
        # player 0 is on a team with 4 players
        players[0].teams.get().members.add(*UserFactory.create_batch(3))
        # large category has max of 4
        small, large = SizeCategoryFactory.create_batch(2)

        for i, player in enumerate(players):
            GuessFactory.create_batch(4 - i, by=player, for_puzzle=puzzle1)
        GuessFactory.create_batch(6, by=players[2], for_puzzle=puzzle2)

        data = TopGuessesGenerator(event=event, number=3, size_category=small).generate()
        TopGuessesGenerator.schema.is_valid(data)

        assert len(data['top_teams']) == 3
        for i, player in enumerate(players[1:]):
            assert data['top_teams'][i] == (
                i + 1, player.team_at(event).get_display_name(), {'size_category': small.label, 'guess_count': 4 - i - 1}
            )
        assert len(data['top_users']) == 3
        for i, player in enumerate(players[:3]):
            assert data['top_users'][i] == (i + 1, player.get_display_name(), 4 - i)

    def test_render_extra_data(self, event):
        team = TeamFactory.build(name='Team 4')
        team.id = 4
        user = UserFactory.build(username='User 4')
        user.id = 4
        data = {
            'by_team': {
                1: {'position': 1, 'payload': {'size_category': None, 'guess_count': 4}},
                2: {'position': 2, 'payload': {'size_category': None, 'guess_count': 3}},
                3: {'position': 3, 'payload': {'size_category': None, 'guess_count': 2}},
                4: {'position': 4, 'payload': {'size_category': None, 'guess_count': 1}},
            },
            'by_user': {
                1: {'position': 1, 'guess_count': 4},
                2: {'position': 2, 'guess_count': 3},
                3: {'position': 3, 'guess_count': 2},
                4: {'position': 4, 'guess_count': 1},
            },
            'top_teams': [
                (1, 'Team 1', {'size_category': None, 'guess_count': 4}),
                (2, 'Team 2', {'size_category': None, 'guess_count': 3}),
                (3, 'Team 3', {'size_category': None, 'guess_count': 2}),
            ],
            'top_users': [
                (1, 'User 1', 4),
                (2, 'User 2', 3),
                (3, 'User 3', 2),
            ],
        }

        render = TopGuessesGenerator(event=event, number=3, size_category=None).render_data(data, team=team, user=user)

        assert 'Team 4' in render
        assert 'User 4' in render

    def test_render_no_duplicate(self, event):
        team = TeamFactory.build(name='Team 3')
        team.id = 3
        user = UserFactory.build(username='User 3')
        user.id = 3

        data = {
            'by_team': {
                1: {'position': 1, 'guess_count': 1},
                2: {'position': 2, 'guess_count': 1},
                3: {'position': 3, 'guess_count': 1},
                4: {'position': 4, 'guess_count': 1},
            },
            'by_user': {
                1: {'position': 1, 'guess_count': 4},
                2: {'position': 2, 'guess_count': 3},
                3: {'position': 3, 'guess_count': 2},
                4: {'position': 4, 'guess_count': 1},
            },
            'top_teams': [
                (1, 'Team 1', 4),
                (2, 'Team 2', 3),
                (3, 'Team 3', 2),
            ],
            'top_users': [
                (1, 'User 1', 4),
                (2, 'User 2', 3),
                (3, 'User 3', 2),
            ],
        }

        render = TopGuessesGenerator(event=event, number=3, size_category=None).render_data(data, team=team, user=user)

        assert 1 == render.count('Team 3')
        assert 1 == render.count('User 3')


@pytest.mark.usefixtures("irrelevant_bystanders")
class TestTotals:
    def test_event_totals(self, event):
        puzzle = PuzzleFactory(episode__winning=True)
        puzzle2 = PuzzleFactory(episode__winning=True)
        puzzle3 = PuzzleFactory(episode__winning=False)
        players = TeamMemberFactory.create_batch(3, team__role=TeamRole.PLAYER)
        players += UserFactory.create_batch(2)
        TeamFactory(members=(players[3], players[4]))
        for i, player in enumerate(players[1:]):  # Player 0 is not active
            GuessFactory(by=player, for_puzzle=puzzle, correct=False)
        for player in players[2:]:  # Player 1 did not get the puzzle right
            GuessFactory(by=player, for_puzzle=puzzle, correct=True)
        GuessFactory(by=players[3], for_puzzle=puzzle2, correct=True)
        GuessFactory(by=players[4], for_puzzle=puzzle3, correct=True)

        data = TotalsGenerator(event=event, size_category=None).generate()
        TotalsGenerator.schema.is_valid(data)

        assert data['active_players'] == 4
        assert data['active_teams'] == 3
        assert data['correct_teams'] == 2
        assert data['finished_teams'] == 1
        assert data['puzzles_solved'] == 4
        assert data['guess_count'] == 9

    def test_episode_totals(self, event):
        episode = EpisodeFactory(winning=False)
        puzzles = PuzzleFactory.create_batch(2, episode=episode)
        irrelevant_puzzle = PuzzleFactory()
        players = TeamMemberFactory.create_batch(2, team__role=TeamRole.PLAYER)
        players[0].teams.get().members.add(*UserFactory.create_batch(2))
        # size categories are not taken into account for totals
        cat = SizeCategoryFactory(max=2)

        GuessFactory(by=players[0], for_puzzle=puzzles[0], correct=True)
        GuessFactory(by=players[0], for_puzzle=puzzles[1], correct=True)
        GuessFactory(by=players[1], for_puzzle=puzzles[1], correct=True)
        GuessFactory(by=players[1], for_puzzle=irrelevant_puzzle, correct=True)

        data = TotalsGenerator(event=event, episode=puzzles[0].episode, size_category=cat).generate()
        TotalsGenerator.schema.is_valid(data)

        assert data['active_players'] == 4
        assert data['active_teams'] == 2
        assert data['correct_teams'] == 2
        assert data['finished_teams'] == 1
        assert data['puzzles_solved'] == 3
        assert data['guess_count'] == 3

    def test_episode_excluded(self, event):
        puzzle1 = PuzzleFactory()
        puzzle2 = PuzzleFactory(episode__no_stats=True)
        players = UserFactory.create_batch(4)
        TeamFactory(members=players, role=TeamRole.PLAYER)
        player = players[0]
        SizeCategoryFactory(max=2)
        SizeCategoryFactory(max=None)
        GuessFactory(by=player, for_puzzle=puzzle1, correct=True)
        GuessFactory(by=player, for_puzzle=puzzle2, correct=True)

        data = TotalsGenerator(event=event, size_category=None).generate()
        TotalsGenerator.schema.is_valid(data)

        assert data['active_players'] == 4
        assert data['active_teams'] == 1
        assert data['correct_teams'] == 1
        assert data['finished_teams'] == 0  # There is no winning episode
        assert data['puzzles_solved'] == 1
        assert data['guess_count'] == 1


@pytest.mark.usefixtures("irrelevant_bystanders")
class TestPuzzleTimes:
    def test_event_puzzle_times(self, event):
        puzzle = PuzzleFactory(episode__winning=True)
        players = TeamMemberFactory.create_batch(4, team__role=TeamRole.PLAYER)
        small, large = SizeCategoryFactory.create_batch(2)
        now = timezone.now()
        # All teams started at the same time
        for player in players:
            TeamPuzzleProgressFactory(team=player.team_at(event), puzzle=puzzle, start_time=now - timedelta(hours=1))
        # Players finish the puzzle in order 1-4
        guesses = [GuessFactory(by=player, for_puzzle=puzzle, correct=True, given=now - timedelta(minutes=4 - i)) for i, player in enumerate(players)]
        # Player 4 also guessed wrong
        GuessFactory(by=players[3], for_puzzle=puzzle, correct=False, given=now - timedelta(minutes=5))

        data = PuzzleTimesGenerator(event=event, number=3, size_category=None).generate()
        PuzzleTimesGenerator.schema.is_valid(data)

        # "Top" has 3 entries, in order, with the correct times
        assert len(data[0]['puzzles'][0]['top']) == 3
        for i, player in enumerate(players[:3]):
            team = player.teams.get()
            tpp = TeamPuzzleProgress.objects.get(puzzle=puzzle, team=team)
            assert data[0]['puzzles'][0]['top'][i] == (
                i + 1,
                team.get_display_name(),
                {
                    'size_category': small.label,
                    'time': PuzzleTimesGenerator.format_solve_time(guesses[i].given - tpp.start_time),
                },
            )
        # The fourth team appears correctly in the indexed data
        team = players[3].teams.get()
        tpp = TeamPuzzleProgress.objects.get(puzzle=puzzle, team=team)
        assert team.id in data[0]['puzzles'][0]['by_team']
        assert data[0]['puzzles'][0]['by_team'][team.id]['position'] == 4
        assert data[0]['puzzles'][0]['by_team'][team.id]['solve_time'] == PuzzleTimesGenerator.format_solve_time(guesses[3].given - tpp.start_time)

    def test_episode_puzzle_times(self, event):
        puzzle1 = PuzzleFactory(episode__winning=False)
        puzzle2 = PuzzleFactory(episode__winning=True,
                                episode__prequels=puzzle1.episode,
                                episode__start_date=puzzle1.episode.start_date + timedelta(hours=1))
        players = TeamMemberFactory.create_batch(4, team__role=TeamRole.PLAYER)
        large_team = TeamFactory(members=UserFactory.create_batch(5), role=TeamRole.PLAYER)
        small, large = SizeCategoryFactory.create_batch(2)
        SizeCategoryFactory(max=None)

        now = timezone.now()
        # large team finishes quickest
        TeamPuzzleProgressFactory(
            team=large_team, puzzle=puzzle1, start_time=now - timedelta(minutes=10), late=False
        )
        GuessFactory(by=large_team.members.first(), for_puzzle=puzzle1, correct=True, given=now - timedelta(9))
        # Players finish the puzzle in order 1-4
        for i, player in enumerate(players):
            # Create Progress. Otherwise an automatic progress will be created simultaneously with the correct guess.
            TeamPuzzleProgressFactory(
                team=player.teams.get(), puzzle=puzzle1, start_time=now - timedelta(minutes=10),
                late=False
            )
            TeamPuzzleProgressFactory(
                team=player.teams.get(), puzzle=puzzle2, start_time=now - timedelta(minutes=10),
                late=False
            ).save()
            GuessFactory(by=player, for_puzzle=puzzle1, correct=True, given=now - timedelta(minutes=4 - i))
        # Players finish the puzzle in order 4-1
        guesses = [
            GuessFactory(by=player, for_puzzle=puzzle2, correct=True, given=now - timedelta(minutes=4 - i))
            for i, player in enumerate(reversed(players))
        ]

        data = PuzzleTimesGenerator(event=event, number=3, size_category=None).generate()
        PuzzleTimesGenerator.schema.is_valid(data)

        assert data[0]['name'] == puzzle1.episode.name
        assert data[1]['name'] == puzzle2.episode.name

        # "Top" has 3 entries, in order, with the correct times
        assert len(data[1]['puzzles'][0]['top']) == 3
        for i, guess in enumerate(guesses[:3]):
            team = guess.by_team
            tpp = TeamPuzzleProgress.objects.get(puzzle=puzzle2, team=team)
            assert data[1]['puzzles'][0]['top'][i] == (
                i + 1, team.get_display_name(), {
                        'size_category': small.label,
                        'time': PuzzleTimesGenerator.format_solve_time(guess.given - tpp.start_time),
                }
            )
        # The fourth team appears correctly in the indexed data
        team = players[0].teams.get()
        tpp = TeamPuzzleProgress.objects.get(puzzle=puzzle2, team=team)
        assert team.id in data[1]['puzzles'][0]['by_team']
        assert data[1]['puzzles'][0]['by_team'][team.id]['position'] == 4
        assert data[1]['puzzles'][0]['by_team'][team.id]['solve_time'] == PuzzleTimesGenerator.format_solve_time(guesses[3].given - tpp.start_time)

    def test_puzzle_times_render_extra_data(self, event):
        team = TeamFactory.build(name='Team 4')
        team.id = 4

        data = {
            'by_team': {
                1: {'position': 1, 'payload': {'size_category': 'test_large', 'time': 4}},
                2: {'position': 2, 'payload': {'size_category': 'test_large', 'time': 3}},
                3: {'position': 3, 'payload': {'size_category': 'test_small', 'time': 2}},
                4: {'position': 4, 'payload': {'size_category': 'test_small', 'time': 1}},
            },
            'top': [
                (1, 'Team 1', {'size_category': 'test_large', 'time': 4}),
                (2, 'Team 2', {'size_category': 'test_large', 'time': 3}),
                (3, 'Team 3', {'size_category': 'test_small', 'time': 2}),
            ],
        }

        render = LeadersGenerator(event=event, number=3, size_category=None).render_data(data, team=team, user=None)

        assert 'Team 4' in render

    def test_episode_excluded(self, event):
        puzzle1 = PuzzleFactory()
        puzzle2 = PuzzleFactory(episode__no_stats=True)
        cat = SizeCategoryFactory(max=2)
        SizeCategoryFactory(max=4)
        players = TeamMemberFactory.create_batch(2, team__role=TeamRole.PLAYER)
        players[1].teams.get().members.add(*UserFactory.create_batch(2))

        GuessFactory(by=players[0], for_puzzle=puzzle1, correct=True)
        GuessFactory(by=players[0], for_puzzle=puzzle2, correct=True)
        GuessFactory(by=players[1], for_puzzle=puzzle1, correct=True)
        GuessFactory(by=players[1], for_puzzle=puzzle2, correct=True)

        data = PuzzleTimesGenerator(event=event, size_category=cat).generate()
        PuzzleTimesGenerator.schema.is_valid(data)

        assert len(data) == 1


@pytest.mark.usefixtures("irrelevant_bystanders")
class TestSolveDistribution:
    def test_episode_puzzle_times(self, event):
        puzzle1 = PuzzleFactory(episode__winning=False)
        puzzle2 = PuzzleFactory(episode__winning=True, episode__prequels=puzzle1.episode)
        # the fifth player and team will not be in the results due to the team having more than 2 players
        players = TeamMemberFactory.create_batch(5, team__role=TeamRole.PLAYER)
        teams = [player.team_at(event) for player in players]
        teams[4].members.add(*UserFactory.create_batch(4))
        SizeCategoryFactory(max=2)
        SizeCategoryFactory(max=None)
        now = timezone.now()
        for i, (player, team) in enumerate(zip(players[:4], teams[:4])):
            TeamPuzzleProgressFactory(puzzle=puzzle1, team=team, start_time=now - timedelta(minutes=5))
            TeamPuzzleProgressFactory(puzzle=puzzle2, team=team, start_time=now - timedelta(minutes=5))
            GuessFactory(by=player, for_puzzle=puzzle1, correct=True, given=now - timedelta(minutes=4 - i))
            GuessFactory(by=player, for_puzzle=puzzle2, correct=True, given=now - timedelta(minutes=3))

        data = SolveDistributionGenerator(event=event, size_category=None).generate()
        assert SolveDistributionGenerator.schema.is_valid(data)
        ep1 = 0 if data['episodes'][0]['id'] == puzzle1.episode.id else 1
        ep2 = 1 - ep1
        assert data['episodes'][ep1]['max_q3'] >= 180.0
        assert data['episodes'][ep1]['max_q3'] <= 240.0
        assert len(data['episodes'][ep1]['puzzles']) == 1
        assert (
            data['episodes'][ep1]['puzzles'][0]['solve_times'] ==
            {team.id: 60 * (i+1) for i, team in enumerate(teams[:4])}
        )
        assert data['episodes'][ep1]['puzzles'][0]['90%'] >= data['episodes'][ep1]['max_q3']
        assert data['episodes'][ep1]['puzzles'][0]['90%'] <= 240.0
        assert data['episodes'][ep2]['max_q3'] == 120.0
        assert len(data['episodes'][ep2]['puzzles']) == 1
        assert (
            data['episodes'][ep2]['puzzles'][0]['solve_times'] ==
            {team.id: 60 * 2 for team in teams[:4]}
        )

    def test_episode_excluded(self, event):
        puzzle1 = PuzzleFactory()
        puzzle2 = PuzzleFactory(episode__no_stats=True)
        player = TeamMemberFactory(team__role=TeamRole.PLAYER)
        team = player.team_at(event)
        now = timezone.now()
        TeamPuzzleProgressFactory(puzzle=puzzle1, team=team, start_time=now - timedelta(minutes=5))
        TeamPuzzleProgressFactory(puzzle=puzzle2, team=team, start_time=now - timedelta(minutes=5))
        GuessFactory(by=player, for_puzzle=puzzle1, correct=True, given=now - timedelta(minutes=4))
        GuessFactory(by=player, for_puzzle=puzzle2, correct=True, given=now - timedelta(minutes=3))

        data = SolveDistributionGenerator(event=event, size_category=None).generate()
        assert SolveDistributionGenerator.schema.is_valid(data)
        assert len(data['episodes']) == 1


@pytest.mark.usefixtures("irrelevant_bystanders")
class TestCompletion:
    def test_puzzle_completion(self, event):
        episode = EpisodeFactory()
        puzzles = PuzzleFactory.create_batch(4, episode=episode)
        # The fifth player will be on an excluded team due to size
        players = TeamMemberFactory.create_batch(5, team__role=TeamRole.PLAYER)
        players[4].teams.get().members.add(*UserFactory.create_batch(3))
        SizeCategoryFactory(max=2)
        SizeCategoryFactory(max=None)
        now = timezone.now()
        for i, player in enumerate(players):
            for pz in puzzles[:i]:
                TeamPuzzleProgressFactory(team=player.teams.get(), puzzle=pz, start_time=now)
                GuessFactory(by=player, for_puzzle=pz, guess='__INCORRECT__')

        for i, player in enumerate(players[1:], 1):
            for pz in puzzles[:i-1]:
                GuessFactory(by=player, for_puzzle=pz, correct=True)

        admin = TeamMemberFactory(team__role=TeamRole.AUTHOR)
        TeamPuzzleProgressFactory(team=admin.teams.get(), puzzle=puzzles[0], start_time=now)
        GuessFactory(by=admin, for_puzzle=puzzles[0], guess='__INCORRECT__')
        TeamPuzzleProgressFactory(team=admin.teams.get(), puzzle=puzzles[1], start_time=now)
        GuessFactory(by=admin, for_puzzle=puzzles[1], correct=True)

        data = CompletionGenerator(event=event, size_category=None).generate()
        CompletionGenerator.schema.is_valid(data)

        episode_data = data['episodes'][episode.id]
        assert episode_data['started_any'] == 3

        for i, puzzle_data in enumerate(episode_data['puzzles']):
            assert puzzle_data['started'] == 4 - i - 1
            assert puzzle_data['solved'] == max(4 - i - 2, 0)
            assert puzzle_data['title'] == puzzles[i].title
