import { createApp } from 'vue'
import * as Sentry from '@sentry/vue'
import 'hunter2/js/base'

import ProgressTable from './progress-table.vue'

const el = document.getElementById('admin_progress_widget')

const progress = createApp(ProgressTable, {
  href: el.attributes.href.value,
})
progress.mixin(Sentry.createTracingMixins({ trackComponents: true }))
Sentry.attachErrorHandler(progress, { logErrors: true })
progress.mount(el)
