import 'bootstrap/js/dist/collapse'
import 'bootstrap/js/dist/dropdown'
import 'hunter2/js/base'

import '../scss/stats.scss'

const prefix = 'stats_expand_'

window.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('.collapse').forEach(elt => elt.addEventListener('hidden.bs.collapse', function() {
    localStorage.setItem(prefix + this.id, 'false')
  }))

  document.querySelectorAll('.collapse').forEach(elt => elt.addEventListener('shown.bs.collapse', function() {
    localStorage.setItem(prefix + this.id, 'true')
  }))

  document.querySelectorAll('.collapse').forEach(elt => {
    if (localStorage.getItem(prefix + elt.id) === 'true') {
      elt.classList.add('show')
      elt.classList.remove('collapsed')
    }
  })

  document.getElementById('size-filter').addEventListener('change', e => {
    let sizeCategory = e.target.value
    document.querySelectorAll('.stat-container').forEach(statContainer => {
      statContainer.style.display = (statContainer.dataset.sizeCategory === sizeCategory ? 'block' : 'none')
    })
  })
})
