# Copyright (C) 2022 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import random
import re
import string
from os import path
from unittest import mock
from uuid import UUID

import freezegun
import pytest
from django.test import RequestFactory, override_settings
from django.urls import reverse
from django.utils import timezone

from accounts.factories import UserFactory
from events.factories import EventFileFactory, AttendanceFactory
from events.test import EventTestCase, AsyncEventTestCase
from hunter2.factories import FileFactory
from hunter2.models import Configuration
from teams.factories import TeamMemberFactory, TeamFactory
from teams.models import TeamRole, SizeCategory
from ..context_processors import announcements
from ..factories import (
    EpisodeFactory,
    GuessFactory,
    PuzzleFactory,
    PuzzleFileFactory,
    SolutionFileFactory,
    TeamPuzzleProgressFactory,
    HintFactory, UnlockAnswerFactory,
)
from ..models import Guess, Puzzle
from ..utils import encode_uuid


class ErrorTests(EventTestCase):
    def test_unauthenticated_404(self):
        url = '/does/not/exist'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


class TestHomePage:

    @pytest.fixture(scope='class')
    def url(self):
        return reverse('index')

    @pytest.fixture
    def stylish_event(self, event):
        event.script = 'console.log("hello");'
        event.script_file = EventFileFactory(event=event)
        event.style = 'body {width: 1234px;}'
        event.style_file = EventFileFactory(event=event)
        event.save()
        return event

    @pytest.fixture
    def stylish_site(self, db):
        configuration = Configuration.get_solo()
        configuration.script = 'console.log("hello");'
        configuration.script_file = FileFactory()
        configuration.style = 'body {width: 1234px;}'
        configuration.style_file = FileFactory()
        configuration.save()
        return configuration

    def test_load_homepage(self, tenant_client, url):
        response = tenant_client.get(url)
        assert response.status_code == 200

    def test_site_script_and_style(self, stylish_site, tenant_client, url):
        response = tenant_client.get(url)
        content = response.content.decode('utf-8')
        assert stylish_site.script in content
        assert stylish_site.script_file.file.url in content
        assert stylish_site.style in content
        assert stylish_site.style_file.file.url in content

    def test_event_script_and_style(self, stylish_event, tenant_client, url):
        response = tenant_client.get(url)
        content = response.content.decode('utf-8')
        assert stylish_event.script in content
        assert stylish_event.script_file.file.url in content
        assert stylish_event.style in content
        assert stylish_event.style_file.file.url in content


class EventIndexTests(EventTestCase):
    def setUp(self):
        self.url = reverse('event')
        self.player = TeamMemberFactory()
        self.admin = TeamMemberFactory(team__role=TeamRole.ADMIN)

    def test_load_event_index(self):
        self.client.force_login(self.player)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.client.force_login(self.admin)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_future_episode_not_visible(self):
        episode = EpisodeFactory(start_date=timezone.now() + datetime.timedelta(hours=1))
        self.client.force_login(self.player)
        response = self.client.get(self.url)
        self.assertNotContains(response, episode.name)

    def test_started_episode_visible(self):
        episode = EpisodeFactory(start_date=timezone.now() - datetime.timedelta(hours=1))
        self.client.force_login(self.player)
        response = self.client.get(self.url)
        self.assertContains(response, episode.name)

    def test_independent_started_episode_visible(self):
        episode1 = EpisodeFactory(start_date=timezone.now() - datetime.timedelta(hours=1))
        episode2 = EpisodeFactory(start_date=timezone.now() - datetime.timedelta(minutes=1))
        self.client.force_login(self.player)
        response = self.client.get(self.url)
        self.assertContains(response, episode1.name)
        self.assertContains(response, episode2.name)

    def test_dependent_locked_episode_not_visible(self):
        episode1 = EpisodeFactory(start_date=timezone.now() - datetime.timedelta(hours=1))
        PuzzleFactory(episode=episode1)
        episode2 = EpisodeFactory(start_date=timezone.now() - datetime.timedelta(minutes=1))
        episode2.add_prequel(episode1)
        self.client.force_login(self.player)
        response = self.client.get(self.url)
        self.assertContains(response, episode1.name)
        self.assertNotContains(response, episode2.name)

    def test_dependent_unlocked_episode_visible(self):
        episode1 = EpisodeFactory(start_date=timezone.now() - datetime.timedelta(hours=1))
        GuessFactory(for_puzzle__episode=episode1, by=self.player, correct=True)
        episode2 = EpisodeFactory(start_date=timezone.now() - datetime.timedelta(minutes=1))
        episode2.add_prequel(episode1)
        self.client.force_login(self.player)
        response = self.client.get(self.url)
        self.assertContains(response, episode1.name)
        self.assertContains(response, episode2.name)

    def test_finished_hunt(self):
        small = SizeCategory(max=2)
        small.save()
        big = SizeCategory(max=4)
        big.save()
        players = TeamMemberFactory.create_batch(4)
        big_team = players[3].teams.get()
        big_team.members.add(UserFactory())
        big_team.members.add(UserFactory())
        puzzle = PuzzleFactory(episode__winning=True)
        now = timezone.now()
        for i, player in enumerate(players):
            GuessFactory(by=player, for_puzzle=puzzle, given=now - datetime.timedelta(minutes=4 - i), correct=True)

        positions = (1, 2, 3, 1)
        position_texts = ("first", "second", "third", "first")
        categories = (small, small, small, big)
        for i, player in enumerate(players):
            self.client.force_login(player)
            response = self.client.get(self.url)

            assert response.context['position'] == positions[i]
            assert response.context['position_text'] == position_texts[i]
            assert response.context['finished_category'] == categories[i]

    def test_finished_hunt_size_changed(self):
        puzzles = PuzzleFactory.create_batch(3, episode__winning=True)

        small = SizeCategory(max=2)
        small.save()
        big = SizeCategory(max=4)
        big.save()

        variable_team = TeamFactory()
        variable_player = variable_team.members.get()
        fixed_team = TeamFactory(members=UserFactory.create_batch(4))
        fixed_player = fixed_team.members.first()
        start = timezone.now() - datetime.timedelta(minutes=30)

        GuessFactory(by=fixed_player, for_puzzle=puzzles[0], given=start + datetime.timedelta(minutes=1), correct=True)
        GuessFactory(by=variable_player, for_puzzle=puzzles[0], given=start + datetime.timedelta(minutes=2), correct=True)
        variable_team.members.add(*UserFactory.create_batch(3))
        GuessFactory(by=fixed_player, for_puzzle=puzzles[1], given=start + datetime.timedelta(minutes=3), correct=True)
        GuessFactory(by=variable_player, for_puzzle=puzzles[1], given=start + datetime.timedelta(minutes=4), correct=True)
        variable_team.members.set([variable_player])
        GuessFactory(by=fixed_player, for_puzzle=puzzles[2], given=start + datetime.timedelta(minutes=4), correct=True)
        GuessFactory(by=variable_player, for_puzzle=puzzles[2], given=start + datetime.timedelta(minutes=5), correct=True)

        self.client.force_login(variable_player)
        response = self.client.get(self.url)

        self.assertEqual(response.context['position'], 2)
        self.assertEqual(response.context['position_text'], 'second')
        self.assertEqual(response.context['finished_category'], big)


class TestEpisodeContent:
    def test_solved_position(self, tenant_client):
        small = SizeCategory(max=2)
        small.save()
        big = SizeCategory(max=4)
        big.save()
        players = TeamMemberFactory.create_batch(4)
        big_team = players[3].teams.get()
        big_team.members.add(UserFactory())
        big_team.members.add(UserFactory())
        puzzle = PuzzleFactory()
        now = timezone.now()
        for i, player in enumerate(players):
            GuessFactory(by=player, for_puzzle=puzzle, given=now - datetime.timedelta(minutes=4 - i), correct=True)

        url = reverse('episode_content', kwargs={'episode_number': puzzle.episode.get_relative_id()})
        positions = (1, 2, 3, 1)
        position_texts = ("first", "second", "third", "first")
        categories = (small, small, small, big)
        for i, player in enumerate(players):
            tenant_client.force_login(player)
            response = tenant_client.get(url)

            assert response.context['finished'] is True
            assert response.context['position'] == positions[i]
            assert response.context['position_text'] == position_texts[i]
            assert response.context['finished_category'] == categories[i]

    def test_disqualified_position(self, tenant_client):
        good_player = TeamMemberFactory()
        bad_player = TeamMemberFactory(team__disqualified=True)
        puzzle = PuzzleFactory()
        now = timezone.now()
        GuessFactory(by=bad_player, for_puzzle=puzzle, given=now - datetime.timedelta(minutes=2), correct=True)
        GuessFactory(by=good_player, for_puzzle=puzzle, given=now - datetime.timedelta(minutes=1), correct=True)

        url = reverse('episode_content', kwargs={'episode_number': puzzle.episode.get_relative_id()})
        tenant_client.force_login(good_player)
        response = tenant_client.get(url)
        assert response.context['finished'] is True
        assert response.context['position'] == 1
        assert response.context['position_text'] == 'first'
        tenant_client.force_login(bad_player)
        response = tenant_client.get(url)
        assert response.context['finished'] is True
        assert response.context['position'] is None
        assert response.context['position_text'] is None

    def test_category_present(self, tenant_client):
        players = TeamMemberFactory.create_batch(4)
        SizeCategory(max=2).save()
        SizeCategory(max=4).save()
        puzzle = PuzzleFactory()
        now = timezone.now()
        for i, player in enumerate(players):
            GuessFactory(by=player, for_puzzle=puzzle, given=now - datetime.timedelta(minutes=4 - i), correct=True)

        url = reverse('episode_content', kwargs={'episode_number': puzzle.episode.get_relative_id()})
        for i, player in enumerate(players):
            tenant_client.force_login(player)
            response = tenant_client.get(url)

            assert response.context['finished_category']

    def test_category_absent(self, tenant_client):
        players = TeamMemberFactory.create_batch(4)
        puzzle = PuzzleFactory()
        now = timezone.now()
        for i, player in enumerate(players):
            GuessFactory(by=player, for_puzzle=puzzle, given=now - datetime.timedelta(minutes=4 - i), correct=True)

        # Category information should not be shown when there are 0 or 1 categories.
        url = reverse('episode_content', kwargs={'episode_number': puzzle.episode.get_relative_id()})
        for i, player in enumerate(players):
            tenant_client.force_login(player)
            response = tenant_client.get(url)

            assert not response.context['finished_category']

        SizeCategory(max=2).save()

        url = reverse('episode_content', kwargs={'episode_number': puzzle.episode.get_relative_id()})
        for i, player in enumerate(players):
            tenant_client.force_login(player)
            response = tenant_client.get(url)

            assert not response.context['finished_category']

    def test_correct_answers_provided(self, player_team, tenant_client):
        player = player_team.members.first()
        ep = EpisodeFactory()
        PuzzleFactory.create_batch(5, episode=ep)
        for pz in Puzzle.objects.all()[:3]:
            GuessFactory(by=player, for_puzzle=pz, correct=True)

        url = reverse('episode_content', kwargs={'episode_number': ep.get_relative_id()})
        tenant_client.force_login(player)
        response = tenant_client.get(url)

        for i, pz in enumerate(response.context['puzzles']):
            if i < 3:
                assert pz.answer_set.get().validate_guess(Guess(guess=pz.correct_answer))
            else:
                assert pz.correct_answer is None


class AnswerSubmissionTests(EventTestCase):
    def setUp(self):
        self.puzzle = PuzzleFactory()
        self.episode = self.puzzle.episode
        self.event = self.episode.event
        self.user = TeamMemberFactory(team__at_event=self.event)
        self.url = reverse('answer', kwargs={
            'episode_number': self.episode.get_relative_id(),
            'puzzle_number': self.puzzle.get_relative_id()
        },)
        self.client.force_login(self.user)

    def test_answer_correct(self):
        response = self.client.post(self.url, {
            'last_updated': '0',
            'answer': GuessFactory.build(for_puzzle=self.puzzle, correct=True).guess
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['correct'], 'true')

    def test_answer_correct_with_hints(self):
        HintFactory(puzzle=self.puzzle)

        answer_url = reverse('answer', kwargs={
            'episode_number': self.puzzle.episode.get_relative_id(),
            'puzzle_number': self.puzzle.get_relative_id()
        },)
        response = self.client.post(answer_url, {
            'last_updated': '0',
            'answer': GuessFactory.build(for_puzzle=self.puzzle, correct=True).guess
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['correct'], 'true')

    def test_no_answer_given(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()['error'], 'no answer given')
        response = self.client.post(self.url, {
            'last_updated': '0',
            'answer': ''
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()['error'], 'no answer given')

    def test_answer_long(self):
        response = self.client.post(self.url, {
            'last_updated': '0',
            'answer': ''.join(random.choice(string.ascii_letters) for _ in range(512)),
        })
        self.assertEqual(response.status_code, 200)

    def test_answer_too_long(self):
        response = self.client.post(self.url, {
            'last_updated': '0',
            'answer': ''.join(random.choice(string.ascii_letters) for _ in range(513)),
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()['error'], 'answer too long')

    def guess_and_assert(self, status_code):
        response = self.client.post(
            self.url, {
                'last_updated': '0',
                'answer': GuessFactory.build(for_puzzle=self.puzzle, correct=False).guess
            }
        )
        self.assertEqual(response.status_code, status_code)
        return response

    def test_answer_cooldown(self):
        with freezegun.freeze_time() as frozen_datetime:
            self.guess_and_assert(200)
            response = self.guess_and_assert(429)
            self.assertTrue(b'error' in response.content)
            frozen_datetime.tick(delta=datetime.timedelta(seconds=5))
            self.guess_and_assert(200)

    def test_answer_non_default_cooldown(self):
        self.episode.cooldown_parameters = {'delay': 10}
        self.episode.save()

        with freezegun.freeze_time() as frozen_datetime:
            self.guess_and_assert(200)
            response = self.guess_and_assert(429)
            self.assertTrue(b'error' in response.content)
            frozen_datetime.tick(delta=datetime.timedelta(seconds=5))
            self.guess_and_assert(429)
            frozen_datetime.tick(delta=datetime.timedelta(seconds=5))
            self.guess_and_assert(200)

    def test_cooldown_skipped_after_unlock(self):
        ua = UnlockAnswerFactory(unlock__puzzle=self.puzzle)

        with freezegun.freeze_time():
            response = self.client.post(
                self.url, {
                    'last_updated': '0',
                    'answer': GuessFactory.build(for_puzzle=self.puzzle, guess=ua.guess).guess
                }
            )
            self.assertEqual(response.status_code, 200)
            data = response.json()
            self.assertTrue('timeout_length_ms' in data)
            self.assertEqual(data['timeout_length_ms'], 0)
            self.guess_and_assert(200)

    def test_cooldown_not_skipped_after_hidden_unlock(self):
        ua = UnlockAnswerFactory(unlock__puzzle=self.puzzle, unlock__text='')

        with freezegun.freeze_time() as frozen_datetime:
            response = self.client.post(
                self.url, {
                    'last_updated': '0',
                    'answer': GuessFactory.build(for_puzzle=self.puzzle, guess=ua.guess).guess
                }
            )
            data = response.json()
            self.assertTrue('timeout_length_ms' in data)
            self.assertEqual(data['timeout_length_ms'], 5000)
            self.guess_and_assert(429)
            frozen_datetime.tick(delta=datetime.timedelta(seconds=5))
            self.guess_and_assert(200)

    def test_cooldown_teamwide(self):
        self.episode.cooldown_behaviour = 'TeamWideDelay'
        self.episode.save()
        user2 = UserFactory()
        self.user.teams.get().members.add(user2)

        with freezegun.freeze_time() as frozen_datetime:
            # initially behaviour is the same as for constant delay
            self.guess_and_assert(200)
            self.guess_and_assert(429)
            self.client.force_login(user2)
            self.guess_and_assert(200)
            self.guess_and_assert(429)

            # now the delay should increase
            self.client.force_login(self.user)
            frozen_datetime.tick(datetime.timedelta(seconds=5))
            self.guess_and_assert(200)
            self.guess_and_assert(429)
            frozen_datetime.tick(datetime.timedelta(seconds=5))
            self.guess_and_assert(429)
            frozen_datetime.tick(datetime.timedelta(seconds=5))
            self.guess_and_assert(200)

    def test_cooldown_ramping(self):
        self.episode.cooldown_behaviour = 'ExponentialRampConstantDecay'
        self.episode.cooldown_parameters = {
            'initial': 5, 'base': 1.5, 'decay': 60, 'cap': 60,
        }
        self.episode.save()

        with freezegun.freeze_time() as frozen_datetime:
            self.guess_and_assert(200)
            self.guess_and_assert(429)
            frozen_datetime.tick(datetime.timedelta(seconds=5))
            self.guess_and_assert(200)
            self.guess_and_assert(429)
            frozen_datetime.tick(datetime.timedelta(seconds=5))
            self.guess_and_assert(429)
            frozen_datetime.tick(datetime.timedelta(seconds=5 * 1.5 - 5))
            self.guess_and_assert(200)

    def test_answer_after_end(self):
        self.client.force_login(self.user)
        with freezegun.freeze_time() as frozen_datetime:
            self.event.end_date = timezone.now() + datetime.timedelta(seconds=5)
            self.event.save()
            self.assertFalse(Guess.objects.exists())
            response = self.client.post(self.url, {
                'last_updated': '0',
                'answer': '__FIRST__',
            })
            self.assertEqual(response.status_code, 200)
            new_guess = Guess.objects.order_by('given').last()
            self.assertEqual(new_guess.guess, '__FIRST__')
            self.assertEqual(new_guess.late, False)
            frozen_datetime.tick(delta=datetime.timedelta(seconds=10))
            response = self.client.post(self.url, {
                'last_updated': '0',
                'answer': '__SECOND__',
            })
            self.assertEqual(response.status_code, 200)
            new_guess = Guess.objects.order_by('given').last()
            self.assertEqual(new_guess.guess, '__SECOND__')
            self.assertEqual(new_guess.late, True)


class TestHintAcceptance:
    @pytest.fixture
    def puzzle(self, event):
        return PuzzleFactory()

    @pytest.fixture
    def team(self, user, event):
        return user.team_at(event)

    @pytest.fixture
    def user(self, event):
        return TeamMemberFactory()

    @pytest.fixture
    def progress(self, team, puzzle):
        return TeamPuzzleProgressFactory(team=team, puzzle=puzzle, start_time=timezone.now())

    @pytest.fixture
    def hint(self, puzzle):
        return HintFactory(puzzle=puzzle)

    @pytest.fixture
    def client(self, tenant_client):
        # return TenantClient(event)
        return tenant_client

    def test_not_unlocked(self, puzzle, user, team, progress, hint, client):
        client.force_login(user)
        assert not hint.unlocked_by(team, progress)
        url = reverse(
            'accept_hint', kwargs={
                'episode_number': puzzle.episode.get_relative_id(),
                'puzzle_number': puzzle.get_relative_id()
            }
        )
        resp = client.post(url, {'id': hint.compact_id})
        assert resp.status_code == 400

    def test_nonexistent(self, puzzle, user, hint, client):
        client.force_login(user)
        url = reverse(
            'accept_hint', kwargs={
                'episode_number': puzzle.episode.get_relative_id(),
                'puzzle_number': puzzle.get_relative_id()
            }
        )
        resp = client.post(url, {'id': encode_uuid(UUID(bytes=b'__NONEXISTENT!__'))})
        assert resp.status_code == 400

    def test_absent_id(self, puzzle, user, hint, client):
        client.force_login(user)
        url = reverse(
            'accept_hint', kwargs={
                'episode_number': puzzle.episode.get_relative_id(),
                'puzzle_number': puzzle.get_relative_id()
            }
        )
        resp = client.post(url, {})
        assert resp.status_code == 400

    def test_accept_unlocked_hint(self, puzzle, user, team, progress, hint, client):
        client.force_login(user)
        assert hint.teamhints.get(team_puzzle_progress=progress).accepted_at is None
        with freezegun.freeze_time() as frozen_datetime:
            remaining = timezone.now() - progress.start_time + hint.time
            frozen_datetime.tick(remaining * 1.01)
            assert hint.unlocked_by(team, progress)
            url = reverse(
                'accept_hint', kwargs={
                    'episode_number': puzzle.episode.get_relative_id(),
                    'puzzle_number': puzzle.get_relative_id()
                }
            )
            resp = client.post(url, {'id': hint.compact_id})
            assert resp.status_code == 200, resp.content
            assert hint.teamhints.get(team_puzzle_progress=progress).accepted_at == timezone.now()

    def test_accept_obsolete_hint(self, puzzle, user, team, progress, hint, client):
        client.force_login(user)
        assert hint.teamhints.get(team_puzzle_progress=progress).accepted_at is None
        obsoleter = UnlockAnswerFactory(unlock__puzzle=puzzle)
        hint.obsoleted_by.add(obsoleter.unlock)
        GuessFactory(for_puzzle=puzzle, by=user, guess=obsoleter.guess).save()
        with freezegun.freeze_time():
            assert hint.obsolete_for(progress)
            url = reverse(
                'accept_hint', kwargs={
                    'episode_number': puzzle.episode.get_relative_id(),
                    'puzzle_number': puzzle.get_relative_id()
                }
            )
            resp = client.post(url, {'id': hint.compact_id})
            assert resp.status_code == 200, resp.content
            assert hint.teamhints.get(team_puzzle_progress=progress).accepted_at == timezone.now()


class PuzzleAccessTests(EventTestCase):
    def setUp(self):
        self.episode = EpisodeFactory(event=self.tenant, parallel=False)
        self.puzzles = PuzzleFactory.create_batch(3, episode=self.episode)
        self.user = TeamMemberFactory(team__at_event=self.tenant)

    def _check_load_callback_answer_solution(self, puzzle, load_response, callback_response, answer_response, solution_response):
        kwargs = {
            'episode_number': self.episode.get_relative_id(),
            'puzzle_number': puzzle.get_relative_id(),
        }

        # Load
        resp = self.client.get(reverse('puzzle', kwargs=kwargs))
        self.assertEqual(resp.status_code, load_response)

        # Callback
        resp = self.client.post(
            reverse('callback', kwargs=kwargs),
            content_type='application/json',
            HTTP_ACCEPT='application/json',
            HTTP_X_REQUESTED_WITH='XMLHttpRequest',
        )
        self.assertEqual(resp.status_code, callback_response)

        # Answer
        resp = self.client.post(
            reverse('answer', kwargs=kwargs),
            {'answer': 'NOT_CORRECT'},  # Deliberately incorrect answer
            HTTP_X_REQUESTED_WITH='XMLHttpRequest',
        )
        self.assertEqual(resp.status_code, answer_response)

        # Solution
        resp = self.client.get(
            reverse('solution_content', kwargs=kwargs),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest',
        )
        self.assertEqual(resp.status_code, solution_response)

    def test_puzzle_view_authorisation(self):
        self.client.force_login(self.user)

        # This test submits two answers on the same puzzle so we have to jump forward 5 seconds
        with freezegun.freeze_time() as frozen_datetime:
            # Episode index redirects to event page episode URL
            response = self.client.get(
                reverse('episode_index', kwargs={
                    'episode_number': self.episode.get_relative_id(),
                })
            )
            self.assertEqual(response.status_code, 301)
            self.assertEqual(response.headers['Location'], self.episode.get_absolute_url())
            # and the event page loads
            response = self.client.get(self.episode.get_absolute_url())
            self.assertEqual(response.status_code, 200)
            # and the episode content loads
            response = self.client.get(
                reverse('episode_content', kwargs={
                    'episode_number': self.episode.get_relative_id(),
                })
            )
            self.assertEqual(response.status_code, 200)

            # Can load, callback and answer the first puzzle
            self._check_load_callback_answer_solution(self.puzzles[0], 200, 200, 200, 403)

            # Answer the puzzle correctly, wait, then try again. This should fail because it's already done.
            GuessFactory(
                by=self.user,
                for_puzzle=self.puzzles[0],
                correct=True
            )
            frozen_datetime.tick(delta=datetime.timedelta(seconds=5))
            # We should be able to load the puzzle but not answer it
            self._check_load_callback_answer_solution(self.puzzles[0], 200, 200, 422, 403)

            self._check_load_callback_answer_solution(self.puzzles[1], 200, 200, 200, 403)
            # Can't load, callback or answer the third puzzle
            self._check_load_callback_answer_solution(self.puzzles[2], 404, 404, 404, 403)

            # Can load third puzzle, and still callback or answer after event ends
            old_time = frozen_datetime()
            frozen_datetime.move_to(self.tenant.end_date + datetime.timedelta(seconds=1))

            # Event is over, now solutions are available
            self._check_load_callback_answer_solution(self.puzzles[2], 200, 200, 200, 200)

            # Revert to current time
            frozen_datetime.move_to(old_time)

            # Answer the second puzzle after a delay of 5 seconds
            frozen_datetime.tick(delta=datetime.timedelta(seconds=5))
            response = self.client.post(
                reverse('answer', kwargs={
                    'episode_number': self.episode.get_relative_id(),
                    'puzzle_number': self.puzzles[1].get_relative_id()}
                        ), {
                    'answer': GuessFactory.build(for_puzzle=self.puzzles[1], correct=True).guess
                },
                HTTP_X_REQUESTED_WITH='XMLHttpRequest'
            )
            self.assertEqual(response.status_code, 200)

    def test_anonymous_access(self):
        with freezegun.freeze_time(self.tenant.end_date - datetime.timedelta(hours=1)) as frozen_datetime:
            # While event is live:
            # Episode URL and event index redirect
            response = self.client.get(
                reverse('episode_index', kwargs={
                    'episode_number': self.episode.get_relative_id(),
                })
            )
            self.assertEqual(response.status_code, 302)
            response = self.client.get(self.episode.get_absolute_url())
            self.assertEqual(response.status_code, 302)
            # Episode content returns 404
            response = self.client.get(
                reverse('episode_content', kwargs={
                    'episode_number': self.episode.get_relative_id(),
                })
            )
            self.assertEqual(response.status_code, 403)
            # load redirects, callback and answer 403 and solution 404s
            self._check_load_callback_answer_solution(self.puzzles[0], 302, 403, 403, 403)

            frozen_datetime.tick(datetime.timedelta(hours=2))

            # Once event is over: load and solution are available, but callback and answer still redirect
            self._check_load_callback_answer_solution(self.puzzles[0], 200, 403, 403, 200)
            response = self.client.get(
                reverse('episode_index', kwargs={
                    'episode_number': self.episode.get_relative_id(),
                })
            )
            self.assertEqual(response.status_code, 301)
            self.assertEqual(response.headers['Location'], self.episode.get_absolute_url())
            # and the event page loads
            response = self.client.get(self.episode.get_absolute_url())
            self.assertEqual(response.status_code, 200)
            # and the episode content loads
            response = self.client.get(
                reverse('episode_content', kwargs={
                    'episode_number': self.episode.get_relative_id(),
                })
            )
            self.assertEqual(response.status_code, 200)

    def test_solution_404(self):
        self.client.force_login(self.user)

        kwargs = {
            'episode_number': self.episode.get_relative_id(),
            'puzzle_number': self.puzzles[2].get_relative_id() + 1,
        }

        with freezegun.freeze_time(self.tenant.end_date + datetime.timedelta(hours=1)):
            resp = self.client.get(
                reverse('solution_content', kwargs=kwargs),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest'
            )

        self.assertEqual(resp.status_code, 404)

    def test_solution_file_404(self):
        self.tenant.end_date = timezone.now()
        self.tenant.save()
        self.client.force_login(self.user)

        kwargs = {
            'episode_number': self.episode.get_relative_id(),
            'puzzle_number': self.puzzles[2].get_relative_id() + 1,
            'file_path': 'test'
        }

        # Remove another source of 404s
        def mock_get_object_or_404(*args, **kwargs):
            assert False

        with (
            mock.patch('hunts.views.player.get_object_or_404', mock_get_object_or_404),
            freezegun.freeze_time(self.tenant.end_date + datetime.timedelta(hours=1)),
        ):
            resp = self.client.get(
                reverse('solution_file', kwargs=kwargs),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest'
            )

            self.assertEqual(resp.status_code, 404)
            print(resp.content)


class TestPuzzlePage:
    def test_static_content(self, tenant_client):
        puzzle = PuzzleFactory()
        url = puzzle.get_absolute_url()
        user = UserFactory()

        tenant_client.force_login(user)
        response = tenant_client.get(url)
        assert puzzle.content in response.content.decode(response.charset)

    def test_no_clues_in_flavour(self, tenant_client):
        tenant_client.tenant.clues_in_flavour = False
        tenant_client.tenant.save()
        puzzle = PuzzleFactory()
        url = puzzle.get_absolute_url()
        user = UserFactory()
        assert puzzle.flavour

        flavour_between_comments = re.compile(
            rf'<!-- PUZZLE STARTS HERE -->.*{puzzle.flavour}.*<!-- PUZZLE ENDS HERE -->', re.DOTALL
        )
        content_between_comments = re.compile(
            rf'<!-- PUZZLE STARTS HERE -->.*{puzzle.content}.*<!-- PUZZLE ENDS HERE -->', re.DOTALL
        )

        tenant_client.force_login(user)
        response = tenant_client.get(url)
        content = response.content.decode(response.charset)
        assert not flavour_between_comments.search(content)
        assert content_between_comments.search(content)
        assert puzzle.flavour in content

    def test_clues_in_flavour(self, tenant_client):
        tenant_client.tenant.clues_in_flavour = True
        tenant_client.tenant.save()
        puzzle = PuzzleFactory()
        url = puzzle.get_absolute_url()
        user = UserFactory()
        assert puzzle.flavour

        flavour_between_comments = re.compile(
            rf'<!-- PUZZLE STARTS HERE -->.*{puzzle.flavour}.*<!-- PUZZLE ENDS HERE -->', re.DOTALL
        )
        content_between_comments = re.compile(
            rf'<!-- PUZZLE STARTS HERE -->.*{puzzle.content}.*<!-- PUZZLE ENDS HERE -->', re.DOTALL
        )

        tenant_client.force_login(user)
        response = tenant_client.get(url)
        content = response.content.decode(response.charset)
        assert flavour_between_comments.search(content)
        assert content_between_comments.search(content)

    def test_unlocks(self, tenant_client):
        puzzle = PuzzleFactory()
        # this will remain locked
        UnlockAnswerFactory(unlock__puzzle=puzzle)
        ua_unlocked = UnlockAnswerFactory(unlock__puzzle=puzzle)
        ua_unlocked_hidden = UnlockAnswerFactory(unlock__puzzle=puzzle, unlock__text='')
        url = puzzle.get_absolute_url()
        user = TeamMemberFactory()
        GuessFactory(by=user, for_puzzle=puzzle, guess=ua_unlocked.guess)
        GuessFactory(by=user, for_puzzle=puzzle, guess=ua_unlocked_hidden.guess)

        tenant_client.force_login(user)
        unlock_data = tenant_client.get(url).context['seed_data']['unlocks']
        assert len(unlock_data) == 1
        assert ua_unlocked.unlock.compact_id in unlock_data


class FileTests(EventTestCase):
    def setUp(self):
        self.eventfile = EventFileFactory()
        self.user = UserFactory()
        self.client.force_login(self.user)

    def test_load_episode_content_with_eventfile(self):
        episode = EpisodeFactory(flavour=f'${{{self.eventfile.slug}}}')
        response = self.client.get(
            reverse('episode_content', kwargs={'episode_number': episode.get_relative_id()}),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.eventfile.file.url)

    def test_load_puzzle_with_eventfile(self):
        puzzle = PuzzleFactory(content=f'${{{self.eventfile.slug}}}')
        response = self.client.get(puzzle.get_absolute_url())
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.eventfile.file.url)

    def test_load_puzzle_with_puzzlefile(self):
        puzzle = PuzzleFactory()
        puzzlefile = PuzzleFileFactory(puzzle=puzzle)
        puzzle.content = f'${{{puzzlefile.slug}}}'
        puzzle.save()
        response = self.client.get(puzzle.get_absolute_url())
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, puzzlefile.url_path)

    def test_puzzlefile_overrides_eventfile(self):
        puzzle = PuzzleFactory()
        puzzlefile = PuzzleFileFactory(puzzle=puzzle, slug=self.eventfile.slug)
        puzzle.content = f'${{{puzzlefile.slug}}}'
        puzzle.save()
        response = self.client.get(puzzle.get_absolute_url())
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, puzzlefile.url_path)

    def test_load_solution_with_eventfile(self):
        puzzle = PuzzleFactory(content='content', soln_content=f'${{{self.eventfile.slug}}}')
        episode_number = puzzle.episode.get_relative_id()
        puzzle_number = puzzle.get_relative_id()
        self.tenant.save()  # To ensure the date we're freezing is correct after any factory manipulation
        with freezegun.freeze_time(self.tenant.end_date + datetime.timedelta(seconds=1)):
            response = self.client.get(
                reverse('solution_content', kwargs={'episode_number': episode_number, 'puzzle_number': puzzle_number}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest'
            )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.eventfile.file.url)

    def test_load_solution_with_puzzlefile(self):
        puzzle = PuzzleFactory(content='content')
        puzzlefile = PuzzleFileFactory(puzzle=puzzle)
        puzzle.soln_content = f'${{{puzzlefile.slug}}}'
        puzzle.save()
        episode_number = puzzle.episode.get_relative_id()
        puzzle_number = puzzle.get_relative_id()
        self.tenant.save()  # To ensure the date we're freezing is correct after any factory manipulation
        with freezegun.freeze_time(self.tenant.end_date + datetime.timedelta(seconds=1)):
            response = self.client.get(
                reverse('solution_content', kwargs={'episode_number': episode_number, 'puzzle_number': puzzle_number}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest'
            )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, puzzlefile.url_path)

    def test_load_solution_with_solutionfile(self):
        puzzle = PuzzleFactory(content='content')
        solutionfile = SolutionFileFactory(puzzle=puzzle)
        puzzle.soln_content = f'${{{solutionfile.slug}}}'
        puzzle.save()
        episode_number = puzzle.episode.get_relative_id()
        puzzle_number = puzzle.get_relative_id()
        self.tenant.save()  # To ensure the date we're freezing is correct after any factory manipulation
        with freezegun.freeze_time(self.tenant.end_date + datetime.timedelta(seconds=1)):
            response = self.client.get(
                reverse('solution_content', kwargs={'episode_number': episode_number, 'puzzle_number': puzzle_number}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest'
            )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, solutionfile.url_path)

    def test_solutionfile_overrides_other_files(self):
        puzzle = PuzzleFactory(content='content')
        puzzlefile = PuzzleFileFactory(puzzle=puzzle, slug=self.eventfile.slug)
        solutionfile = SolutionFileFactory(puzzle=puzzle, slug=puzzlefile.slug)
        puzzle.soln_content = f'${{{solutionfile.slug}}}'
        puzzle.save()
        episode_number = puzzle.episode.get_relative_id()
        puzzle_number = puzzle.get_relative_id()
        self.tenant.save()  # To ensure the date we're freezing is correct after any factory manipulation
        with freezegun.freeze_time(self.tenant.end_date + datetime.timedelta(seconds=1)):
            response = self.client.get(
                reverse('solution_content', kwargs={'episode_number': episode_number, 'puzzle_number': puzzle_number}),
                HTTP_X_REQUESTED_WITH='XMLHttpRequest'
            )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, solutionfile.url_path)

    def test_file_content_disposition(self):
        puzzle = PuzzleFactory(content='content')
        puzzle_file = PuzzleFileFactory(puzzle=puzzle)
        solution_file = SolutionFileFactory(puzzle=puzzle)
        episode_number = puzzle.episode.get_relative_id()
        puzzle_number = puzzle.get_relative_id()
        response = self.client.get(
            reverse('puzzle_file', kwargs={'episode_number': episode_number, 'puzzle_number': puzzle_number, 'file_path': puzzle_file.url_path})
        )
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(
            path.basename(puzzle_file.file.name),
            response.headers['Content-Disposition'],
            'PuzzleFile response should not include the real filename in Content-Disposition'
        )
        with freezegun.freeze_time(self.tenant.end_date + datetime.timedelta(seconds=1)):
            response = self.client.get(
                reverse('solution_file', kwargs={'episode_number': episode_number, 'puzzle_number': puzzle_number, 'file_path': solution_file.url_path})
            )
        self.assertEqual(response.status_code, 200)
        self.assertNotIn(
            path.basename(solution_file.file.name),
            response.headers['Content-Disposition'],
            'SolutionFile response should not include the real filename in Content-Disposition'
        )


class PlayerStatsViewTests(EventTestCase):
    def setUp(self):
        self.url = reverse('player_stats')

    def test_access(self):
        now = timezone.now()
        with freezegun.freeze_time(now) as frozen_datetime:
            users = TeamMemberFactory.create_batch(11, team__role=TeamRole.PLAYER)
            admin = TeamMemberFactory(team__role=TeamRole.ADMIN)
            # Ensure the event has a winning episode containing a puzzle, with a correct guess by a user
            puzzle = PuzzleFactory(episode__event=self.tenant, episode__winning=True)
            for i, user in enumerate(users):
                TeamPuzzleProgressFactory(puzzle=puzzle, team=user.team_at(self.tenant))
                GuessFactory(for_puzzle=puzzle, correct=True, by=user, given=now - datetime.timedelta(minutes=len(users) - i))
            self.tenant.end_date = now + datetime.timedelta(minutes=1)
            self.tenant.save()
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 403)
            self.client.force_login(admin)
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 200)
            self.client.force_login(users[-1])
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 403)
            frozen_datetime.tick(datetime.timedelta(minutes=2))
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 200)
            self.client.logout()
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 200)

    @override_settings(CACHES={'default': {'BACKEND': 'django.core.cache.backends.dummy.DummyCache'},
                               'stats': {'BACKEND': 'django.core.cache.backends.dummy.DummyCache'}})
    def test_no_winning_episode(self):
        EpisodeFactory(event=self.tenant, winning=False)
        user = TeamMemberFactory()
        self.client.force_login(user)
        self.tenant.end_date = timezone.now()
        self.tenant.save()
        response = self.client.get(self.url)
        # Leaders cannot be computed without a winning episode, so it should be missing
        self.assertTrue(any(gen_id == 'progress' for gen_id, _ in response.context['stats']))
        self.assertFalse(any(gen_id == 'leaders' for gen_id, _ in response.context['stats']))


class ContextProcessorTests(AsyncEventTestCase):
    def setUp(self):
        super().setUp()
        self.rf = RequestFactory()
        self.user = UserFactory()
        self.user.save()
        self.client.force_login(self.user)

        self.request = self.rf.get('/')
        self.request.user = self.user
        self.request.tenant = self.tenant

    def test_shows_seat_announcement_if_enabled_and_user_has_no_seat(self):
        AttendanceFactory(user=self.user, event=self.tenant, seat='').save()

        self.tenant.seat_assignments = True

        output = announcements(self.request)

        self.assertEqual(1, len(output['announcement_data']))
        self.assertIn('no_seat', output['announcement_data'].keys())

    def test_does_not_show_seat_announcement_if_enabled_and_user_has_seat(self):
        AttendanceFactory(user=self.user, event=self.tenant, seat='A1').save()

        self.tenant.seat_assignments = True

        output = announcements(self.request)

        self.assertEqual(0, len(output['announcement_data']))

    def test_does_not_show_seat_announcement_if_disabled(self):
        AttendanceFactory(user=self.user, event=self.tenant, seat='').save()

        self.tenant.seat_assignments = False

        output = announcements(self.request)

        self.assertEqual(0, len(output['announcement_data']))

    def test_shows_contact_request_announcement_if_user_has_no_pref(self):
        user = UserFactory(contact=None)
        AttendanceFactory(user=user, event=self.tenant)

        request = self.rf.get('/')
        request.user = user
        request.tenant = self.tenant

        output = announcements(request)

        self.assertIn('no_contact', output['announcement_data'].keys(), 'Contact request announcement missing from context')

    def test_does_not_show_contact_request_announcement_if_user_has_pref_true(self):
        user = UserFactory(contact=True)
        AttendanceFactory(user=user, event=self.tenant)

        request = self.rf.get('/')
        request.user = user
        request.tenant = self.tenant

        output = announcements(request)

        self.assertNotIn('no_contact', output['announcement_data'].keys(), 'Unexpected contact request announcement in context')

    def test_does_not_show_contact_request_announcement_if_user_has_pref_false(self):
        user = UserFactory(contact=False)
        AttendanceFactory(user=user, event=self.tenant)

        request = self.rf.get('/')
        request.user = user
        request.tenant = self.tenant

        output = announcements(request)

        self.assertNotIn('no_contact', output['announcement_data'].keys(), 'Unexpected contact request announcement in context')
