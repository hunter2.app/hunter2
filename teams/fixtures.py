# Copyright (C) 2023 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

import pytest


@pytest.fixture
def player_team(event):
    from accounts.factories import UserFactory
    from teams.factories import TeamFactory
    from teams.models import TeamRole

    player = UserFactory()
    return TeamFactory(role=TeamRole.PLAYER, members=[player])


@pytest.fixture(autouse=True)
def reset_sizecategory_sizes():
    from teams.factories import SizeCategoryFactory

    SizeCategoryFactory.reset_sequence()
