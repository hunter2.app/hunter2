# Copyright (C) 2018-2022 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.contrib import admin
from solo.admin import SingletonModelAdmin

from . import models


@admin.register(models.APIToken)
class APITokenAdmin(admin.ModelAdmin):
    readonly_fields = ('token', )
    list_display = ('token', )


@admin.register(models.Configuration)
class ConfigurationAdmin(SingletonModelAdmin):
    class Form(forms.ModelForm):
        bump_privacy_policy_version = forms.BooleanField(required=False)

        def save(self, commit=True):
            m = super().save(commit=False)
            if self.cleaned_data['bump_privacy_policy_version']:
                m.privacy_policy_version += 1
                # To avoid a really annoying jarring experience for the superuser performing the update,
                # update their accepted version automatically.
                self.user.accepted_privacy_policy_version = m.privacy_policy_version
                self.user.save()
            if commit:
                m.save()
            return m

    def get_form(self, request, *args, **kwargs):
        form = super().get_form(request, *args, **kwargs)
        form.user = request.user  # Stash the requesting user here so we can update their acceptance automatically
        return form

    form = Form
    fields = (
        ('privacy_policy', 'bump_privacy_policy_version'),
        ('script', 'script_file'),
        ('style', 'style_file'),
        ('captcha_question', 'captcha_answer'),
    )


admin.site.register(models.File)
admin.site.register(models.Icon)
