# Generated by Django 5.0.7 on 2024-10-20 17:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0017_merge_20240613_0845'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ('end_date',)},
        ),
    ]
