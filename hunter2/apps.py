# Copyright (C) 2018 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.


import structlog
from django.apps import AppConfig
from django.contrib import admin
from django.contrib.admin.apps import AdminConfig
from django.dispatch import receiver
from django_structlog import signals


@receiver(signals.update_failure_response)
@receiver(signals.bind_extra_request_finished_metadata)
def add_request_id_to_error_response(response, logger, **kwargs):
    context = structlog.contextvars.get_merged_contextvars(logger)
    response['X-Request-ID'] = context['request_id']


class Hunter2Config(AppConfig):
    name = 'hunter2'

    def ready(self):
        super(Hunter2Config, self).ready()
        from . import privacy  # noqa


class Hunter2AdminSite(admin.AdminSite):
    prioritised_apps = [
        'hunts',
        'events',
        'hunter2',
        'teams',
        'accounts',
    ]
    prioritised_models = {
        'hunts': ['Puzzle', 'Episode', 'Announcement', 'Headstart', 'TeamPuzzleProgress'],
        'events': ['Event', 'Domain'],
        'hunter2': ['Configuration'],
        'teams': ['Team', 'SizeCategory'],
    }

    @staticmethod
    def prioritise(reference_order, label):
        try:
            idx = reference_order.index(label)
        except ValueError:
            idx = None

        # Order all things in the reference order before everything not in it, and order those in it according to
        # position in the reference order.
        return idx is None, idx if idx is not None else label

    def get_app_list(self, request, app_label=None):
        apps = super().get_app_list(request, app_label)

        apps.sort(key=lambda app: self.prioritise(self.prioritised_apps, app['app_label']))
        for app in apps:
            if app['app_label'] in self.prioritised_models:
                app['models'].sort(
                    key=lambda model: self.prioritise(self.prioritised_models[app['app_label']], model['object_name'])
                )

        return apps


class H2AdminConfig(AdminConfig):
    default_site = 'hunter2.apps.Hunter2AdminSite'
