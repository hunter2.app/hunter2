# Copyright (C) 2018-2022 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.


from allauth.account import views as allauth_views
from allauth.account.forms import ChangePasswordForm, SetPasswordForm
from allauth.socialaccount.providers.discord.provider import DiscordProvider
from allauth.socialaccount.providers.openid.provider import OpenIDProvider
from allauth.socialaccount.providers.steam.provider import SteamOpenIDProvider
from dal import autocomplete
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy
from django.utils.safestring import mark_safe
from django.views import View
from django.views.generic import FormView
from django.views.generic.detail import DetailView
from django_tenants.utils import tenant_context

from teams.models import Team, TeamRole
from . import forms
from .forms import PrivacyPolicyForm

User = get_user_model()


class UserIDAutoComplete(UserPassesTestMixin, autocomplete.Select2QuerySetView):
    """
    User autocomplete view returning the ID.

    Since these IDs are enumerable and we don't want to expose that to regular users we permit only "staff" to use this view.
    """
    raise_exception = True

    def test_func(self):
        return self.request.user.is_staff

    def get_queryset(self):
        qs = User.objects.order_by('username')

        if self.q:
            qs = qs.filter(username__istartswith=self.q)

        return qs


class UserUUIDAutoComplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    """
    User autocomplete view returning the UUID.

    This auto-complete view is used by end users for team creation, so returns the user-visible user UUID instead of the ID.
    """
    raise_exception = True

    def get_result_value(self, result):
        return str(result.uuid)

    def get_queryset(self):
        qs = User.objects.exclude(pk=self.request.user.pk).order_by('username')

        if self.q:
            qs = qs.filter(username__istartswith=self.q)

        return qs


class ProfileView(DetailView):
    model = User
    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'
    template_name = 'accounts/profile.html'

    def get_context_data(self, object):
        context = super().get_context_data(object=object)
        context.update({
            'authorships': [],
            'administrations': [],
            'participations': [],
        })
        context_keys = {
            TeamRole.AUTHOR: 'authorships',
            TeamRole.ADMIN: 'administrations',
            TeamRole.PLAYER: 'participations',
        }
        for attendance in object.attendance_set.all().order_by('-event__end_date').select_related('event'):
            event = attendance.event
            with tenant_context(event):
                try:
                    team = Team.objects.filter(members=object).get()
                    attendance_info = {
                        "event": event.name,
                        "team": team.name,
                    }
                    context[context_keys[team.role]].append(attendance_info)
                except Team.DoesNotExist:
                    pass
        return context


class PasswordChangeView(allauth_views.PasswordChangeView):
    success_url = reverse_lazy('edit_profile')


class EditProfileView(LoginRequiredMixin, View):
    def _get_attendance_formset(self, request):
        AttendanceFormset = forms.attendance_formset_factory(request.tenant.seat_assignments)
        return AttendanceFormset(instance=request.user, queryset=request.user.attendance_set.filter(event=request.tenant))

    @classmethod
    def _account_name(cls, account):
        if account.provider == DiscordProvider.id:
            return account.extra_data['global_name']
        if account.provider == OpenIDProvider.id:
            return account.uid.split('/')[-1]
        if account.provider == SteamOpenIDProvider.id:
            if 'personaname' in account.extra_data:
                return account.extra_data['personaname']
            return account.uid
        return account.uid

    @staticmethod
    def _account_type(account):
        return 'steam' if account.provider == OpenIDProvider.id else account.provider

    @staticmethod
    def _account_url(account):
        if account.provider == DiscordProvider.id:
            return f'https://discordapp.com/users/{account.uid}'
        if account.provider == OpenIDProvider.id and account.uid.startswith('https://steamcommunity.com'):
            return account.uid.replace('openid/id', 'profiles')
        if account.provider == SteamOpenIDProvider.id:
            if 'profileurl' in account.extra_data:
                return account.extra_data['profileurl']
            return f'https://steamcommunity.com/openid/id/{account.uid}'
        return None

    def _render(self, request, user_form, attendance_formset):
        password_form = ChangePasswordForm(user=request.user) if request.user.has_usable_password() else SetPasswordForm(user=request.user)
        context = {
            'user_form': user_form,
            'password_form': password_form,
            'attendance_formset': attendance_formset,
            'social_accounts': [{
                'name': self._account_name(account),
                'type': self._account_type(account),
                'url': self._account_url(account),
            } for account in request.user.socialaccount_set.all()],
        }
        return TemplateResponse(
            request,
            'accounts/profile_edit.html',
            context=context,
        )

    def get(self, request):
        user_form = forms.UserForm(instance=request.user)
        attendance_formset = self._get_attendance_formset(request) if request.tenant else None
        return self._render(request, user_form, attendance_formset)

    def post(self, request):
        user_form = forms.UserForm(request.POST, instance=request.user)
        attendance_formset = None
        if user_form.is_valid():
            created_user = user_form.save(commit=False)
            AttendanceFormset = forms.attendance_formset_factory(request.tenant.seat_assignments)
            attendance_formset = AttendanceFormset(request.POST, instance=created_user)
            if attendance_formset.is_valid():
                created_user.save()
                attendance_formset.save()
                return HttpResponseRedirect(reverse('edit_profile'))
        return self._render(request, 400, user_form, attendance_formset)


class ReviewPrivacyPolicyView(LoginRequiredMixin, FormView):
    template_name = 'accounts/profile_privacy.html'
    form_class = PrivacyPolicyForm

    def get_success_url(self):
        if 'next' in self.request.GET:
            return self.request.GET['next']
        else:
            return reverse('index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['privacy_policy'] = mark_safe(self.request.site_configuration.privacy_policy)
        return context

    def form_valid(self, form):
        self.request.user.accepted_privacy_policy_version = self.request.site_configuration.privacy_policy_version
        self.request.user.save()
        return super().form_valid(form)
