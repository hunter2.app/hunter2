#!/bin/sh
set -eu

for f in /etc/nginx/templates/*
do
    # shellcheck disable=SC2094
    envsubst '$H2_MAX_UPLOAD_SIZE' < "${f}" > "/etc/nginx/conf.d/$(basename "${f}")"
done

exec "$@"
