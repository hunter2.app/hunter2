# Copyright (C) 2018 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.


from base64 import urlsafe_b64encode, urlsafe_b64decode
from collections import defaultdict

from uuid import UUID

from teams.models import SizeCategory


def event_episode(event, episode_number):

    episodes = event.episode_set.order_by('start_date')
    ep_int = int(episode_number)
    ep = episodes[ep_int - 1:ep_int].get()

    ep.relative_id = ep_int
    return ep


def event_episode_puzzle(event, episode_number, puzzle_number):

    episode = event_episode(event, episode_number)
    return episode, episode.get_puzzle(puzzle_number)


def finishing_positions(event, in_category=None, include_late=False):
    """Get an iterable of teams in the order in which they finished the whole Event"""
    from .models import Episode
    winning_episodes = Episode.objects.filter(event=event, winning=True)

    team_times = defaultdict(list)
    if in_category is not None:
        team_sizes = {}
        for ep in winning_episodes:
            for team, time in ep.finished_times(with_sizes=True, include_late=include_late):
                team_times[team].append(time)
                if team in team_sizes:
                    team_sizes[team] = max(team_sizes[team], team.size_category)
                else:
                    team_sizes[team] = team.size_category
        teams = list(team_times.keys())
        for team in teams:
            if team_sizes[team] != in_category:
                del team_times[team]
    else:
        for ep in winning_episodes:
            for team, time in ep.finished_times(include_late=include_late):
                team_times[team].append(time)

    num_winning_episodes = len(winning_episodes)
    for team, times in list(team_times.items()):
        if len(times) < num_winning_episodes:
            # To win an event you have to win every winning episode
            del team_times[team]
        else:
            # Your position is dictated by the maximum of your episode win times
            team_times[team] = max(times)

    return sorted(team_times.keys(), key=lambda t: team_times[t])


def position_for_display(position):
    """Return numerical position and text description (in English)

    Args: position (int): 0-indexed finishing position
    Returns:
        1-indexed integer finishing position
        text description suitable for template
    """
    position += 1
    if position < 4:
        text = {1: 'first', 2: 'second', 3: 'third'}[position]
    else:
        text = f'in position {position}'
    return position, text


def finished_context(category, finished, positions, team):
    """Return the bits of template context required to display information about a team having finished something or not

    Args:
        category: SizeCategory of the team (at the relevant point in time; end of the episode for example)
        finished: whether the team has finished
        positions: list of Teams ordered by finishing position
        team: the team in question

    Note that normally `finished` can be derived from `positions` but not necessarily, because we don't return positions
    for teams which have finished but should be excluded from leaderboards. This includes admin and disqualified teams.
    In this case we need a separate indicator that the team has finished.
    """
    if team in positions:
        finished = True
        position = positions.index(team)
        position, position_text = position_for_display(position)
    else:
        finished = finished
        position = None
        position_text = None

    # No need to display the team's size if there are 0 or 1 configured.
    finished_category = category if finished and SizeCategory.objects.count() > 1 else None
    return {
        'finished': finished,
        'finished_category': finished_category,
        'position': position,
        'position_text': position_text,
    }


def encode_uuid(uuid):
    return urlsafe_b64encode(uuid.bytes).strip(b'=').decode('ascii')


def decode_uuid(compact_id):
    return UUID(bytes=urlsafe_b64decode(compact_id + '=' * (len(compact_id) % 4)))
