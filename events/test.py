# Copyright (C) 2018 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

import asyncio
from datetime import timedelta
import threading
from urllib.parse import unquote, urlparse

from channels.testing import WebsocketCommunicator, ApplicationCommunicator
from asgiref.timeout import timeout as async_timeout
from django.core.management import call_command
from django.db import connections
from django.test import TransactionTestCase
from django.utils import timezone
from django_tenants.test.cases import FastTenantTestCase
from django_tenants.test.client import TenantClient

from hunts import hint_scheduler
from .factories import EventFactory, DomainFactory
from .models import Event


TEST_SCHEMA_NAME = '_unittest_schema'


class EventAwareTestCase(TransactionTestCase):
    @staticmethod
    def _delete_events_and_schemas():
        """Delete all events and drop their associated schemas

        Note "all": this allows tests to be sure there are no other events in the database messing things up,
        and also helps keep these tests fast (see note in truncate_tables), but imposes a cost when we switch back to
        the fast TestCase and makes --reuse-db less effective.
        """
        Event.deactivate()
        for event in Event.objects.all():
            event.delete(force_drop=True)

    def _fixture_setup(self):
        # We have to delete events on setup because the default fast TestCase leaves its Event lying around to be
        # reused, which tends to break tests that expect to have a clean event table.
        self._delete_events_and_schemas()
        super()._fixture_setup()

    def _fixture_teardown(self):
        self._delete_events_and_schemas()
        # As well as dropping the event schema(s) and the event from the public schema, the public schema may have
        # been populated with other stuff so still needs to be cleaned.
        self.truncate_tables()

    @classmethod
    def truncate_tables(cls):
        db_names = cls._databases_names(include_mirrors=False)
        for db_name in db_names:
            conn = connections[db_name]
            inhibit_post_migrate = (
                cls.available_apps is not None or
                (
                    # Inhibit the post_migrate signal when using serialized
                    # rollback to avoid trying to recreate the serialized data.
                    cls.serialized_rollback and
                    hasattr(conn, '_test_serialized_contents')
                )
            )
            conn.set_schema_to_public()
            # The default truncation behaviour in the superclass does not (generally) cascade. This is wrong in our
            # case because there are always FKs from the event schemas to the public schemas, so it's not possible
            # to truncate the public schema without cascading.
            # NOTE: TRUNCATE ... CASCADE is quite slow if there are a lot of tables. It is therefore important that
            # not only are tables truncated, but that *schemas are dropped*
            call_command(
                'flush',
                verbosity=0,
                interactive=False,
                database=db_name,
                reset_sequences=False,
                allow_cascade=True,
                inhibit_post_migrate=inhibit_post_migrate
            )


class EventTestCase(FastTenantTestCase):
    def _pre_setup(self):
        super()._pre_setup()

        self.client = TenantClient(self.tenant)

    @classmethod
    def setup_tenant(cls, tenant):
        tenant.current = True
        tenant.end_date = timezone.now() + timedelta(days=5)
        tenant.name = 'Test Event'

    @classmethod
    def get_test_schema_name(cls):
        return TEST_SCHEMA_NAME


class ScopeOverrideCommunicator(WebsocketCommunicator):
    def __init__(self, application, path, scope=None, headers=None, subprotocols=None):
        if not isinstance(path, str):
            raise TypeError("Expected str, got {}".format(type(path)))
        if scope is None:
            scope = {}
        parsed = urlparse(path)
        self.scope = {
            "type": "websocket",
            "path": unquote(parsed.path),
            "query_string": parsed.query.encode("utf-8"),
            "headers": headers or [],
            "subprotocols": subprotocols or [],
        }
        self.scope.update(scope)
        ApplicationCommunicator.__init__(self, application, self.scope)

    async def receive_output(self, timeout=1):
        """
        Receives a single message from the application, with optional timeout.
        """
        # Make sure there's not an exception to raise from the task
        if self.future.done():
            self.future.result()
        # Wait and receive the message
        try:
            async with async_timeout(timeout):
                return await self.output_queue.get()
        except asyncio.TimeoutError as e:
            # See if we have another error to raise inside
            if self.future.done():
                self.future.result()
            # NOTE: original code would call self.future.cancel() here, breaking the application forever. Why? Who knows
            raise e


class EventLoopTestHelper:
    """
    This helper class maintains an event loop in a separate thread. It is designed to be used for an entire test
    session.

    This is important because channels-redis has no mechanism for clearing out locks from an old event loop,
    which produces errors if you try to interact with it on a new one.

    It also runs the hint scheduler on the event loop.
    """
    def __init__(self):
        self.thread = None
        self.loop = None
        self.scheduler_ready = threading.Event()

    def get_loop(self):
        self.ensure_started()
        return self.loop

    def ensure_started(self):
        if self.loop is None:
            self.thread = threading.Thread(target=self.run_event_loop, daemon=True)
            self.thread.start()
            if not self.scheduler_ready.wait(timeout=1):
                raise RuntimeError('Timeout while setting up event loop thread')
            self.scheduler_ready.clear()
            asyncio.set_event_loop(self.loop)
            if not self.thread.is_alive():
                raise RuntimeError("Event loop thread died during setup")

    def run_event_loop(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)

        self.run_hint_scheduler()

    def run_hint_scheduler(self):
        # Exceptions which happen in the hint scheduler don't always produce tracebacks, and there's otherwise no way
        # to know when the hint scheduler starts.
        def mock_run(_self):
            """
            Runs the asyncio event loop with our handler loop.
            """
            event_loop = asyncio.get_event_loop()
            asyncio.ensure_future(_self.application_checker())
            try:
                # The scheduler presumably isn't 100% ready at this point, but it's pretty close.
                self.scheduler_ready.set()
                event_loop.run_until_complete(_self.handle())
            except KeyboardInterrupt:
                print("Exiting due to Ctrl-C/interrupt")
            except Exception:
                print("Error in event loop thread:")
                import traceback
                traceback.print_exc()
                raise

        # This will ultimately call `run_until_complete` on the current event loop (i.e. self.loop) with the scheduler
        # worker, and then block the thread forever.
        from unittest import mock
        with mock.patch('channels.worker.Worker.run', mock_run):
            call_command('runworker', hint_scheduler.CHANNEL_NAME)


event_loop_test_helper = EventLoopTestHelper()


class AsyncEventTestCase(EventAwareTestCase):
    """TestCase to support consumer tests

    This cannot use transactions to achieve test isolation, because the signal handlers need to call across threads
    to use the asyncio-based consumer, hence transactions must be committed for db changes to show up consistently.

    However, we don't want to create an Event every test, because setting up and tearing down the schema and its
    tables is very slow, so this class is something of a hybrid between the EventAware- and Event-TestCases.
    """
    def setUp(self):
        self.client = TenantClient(self.tenant)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        Event.deactivate()
        cls.tenant = EventFactory()
        cls.domain = DomainFactory(tenant=cls.tenant)
        cls.tenant.activate()
        cls.headers = [
            (b'origin', b'hunter2.local'),
            (b'host', cls.domain.domain.encode('idna'))
        ]
        cls.loop = event_loop_test_helper.get_loop()

    @classmethod
    def tearDownClass(cls):
        cls._delete_events_and_schemas()
        cls.truncate_tables()
        super().tearDownClass()

    def _fixture_setup(self):
        pass

    def _fixture_teardown(self):
        pass

    def assertMatchesAll(self, first, second):
        """assert that everything on the left matches with something on the right, and that nothing on the right is
        unmatched.

        left and right are assumed to be iterables of dictionaries.
        A pair matches if every key on the left *which exists on the right* matches - recursively, if the value is a
            dictionary.
        This is purposefully asymmetric so that not all data needs to be specified.
        """
        def matches(a, b):
            if isinstance(a, list) and isinstance(b, list):
                return len(a) == len(b) and all(matches(ax, bx) for ax, bx in zip(a, b))
            elif isinstance(a, dict) and isinstance(b, dict):
                for k, v in b.items():
                    if k not in a:
                        return False
                    if (
                            v != a[k] and
                            not matches(a[k], v)
                    ):
                        return False

                return True
            else:
                return False

        first_ = list(first)
        second_ = list(second).copy()
        for a in first:
            for i, b in enumerate(second_):
                if matches(a, b):
                    first_.pop(first_.index(a))
                    second_.pop(i)
                    break

        if first_ or second_:
            first_msg = ''.join('\n + ' + str(a) for a in first_)
            second_msg = ''.join('\n - ' + str(b) for b in second_)
            newline = '\n'
            self.fail(
                f'Not all values could be matched.'
                f'{" Extra:" if first_ else " Missing:"}'
                f'{first_msg if first_ else ""}'
                f'{newline + "missing:" if first_ and second_ else ""}{second_msg if second_ else ""}'
            )

    def assertReceiveMultiple(self, comm, expected):
        """Convenience method to receive as much json as possible from the communicator, then run it
        through assertMatchesAll against expected."""
        outputs = []
        while True:
            try:
                # Set a short timeout because we are guaranteed to incur this as a delay
                outputs.append(self.run_async(comm.receive_json_from)(timeout=0.1))
            except asyncio.TimeoutError:
                break
        self.assertMatchesAll(outputs, expected)

    def get_communicator(self, app, url, scope=None):
        return ScopeOverrideCommunicator(app, url, scope, headers=self.headers)

    def receive_json(self, comm, msg='', timeout=1, no_fail=False):
        try:
            output = self.run_async(comm.receive_json_from)(timeout=timeout)
        except asyncio.TimeoutError:
            if no_fail:
                return {}
            else:
                self.fail(msg)
        return output

    def run_async(self, coro):
        def inner(*args, **kwargs):
            if not self.loop.is_running():
                raise RuntimeError('TestCase event loop not running')
            result = asyncio.run_coroutine_threadsafe(coro(*args, **kwargs), self.loop)
            return result.result()

        return inner
