docker compose exec --user postgres db /bin/sh -c '
vacuumdb --all --analyze-in-stages
sh /docker-entrypoint-initdb.d/20-init-exporter.sh
'
