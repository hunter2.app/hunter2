# Copyright (C) 2018-2024 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

import asyncio
from datetime import datetime
from datetime import timezone as dt_timezone
from json import JSONDecodeError

import inflection
import structlog
from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
from channels.layers import get_channel_layer
from django.core.exceptions import ValidationError as DjangoValidationError
from django.db.models import Case, When, Value, Count, Q, Prefetch
from django.utils import timezone
from h2api.protocol import update_stream
from jsonmodels.errors import ValidationError as JsonValidationError

from events.consumers import EventMixin
from hunter2.models import APIToken
from hunts import models, utils
from teams.consumers import TeamMixin
from teams.models import Team
from .protocol import web as web_protocol
from ..models import Episode, TeamPuzzleProgress

logger = structlog.get_logger(__name__)


class BaseWebsocket(JsonWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _error(self, message):
        logger.warn('Error handling websocket message', error=message)
        self.send_json({'type': 'error', 'content': {'error': message}})

    @classmethod
    def _send_message(cls, group, message):
        layer = get_channel_layer()
        async_to_sync(layer.group_send)(group, {'type': 'send_json_msg', 'content': message})

    def send_json_msg(self, content, **kwargs):
        # For some reason consumer dispatch doesn't strip off the outer dictionary with 'type': 'send_json'
        # (or whatever method name) so we override and do it here. This saves us having to define a separate
        # method which just calls send_json for each type of message.
        self.send_json(content['content'], **kwargs)

    def _scope_log_kwargs(self):
        log_kwargs = {}
        # These fields are missing when testing using WebsocketConsumer
        if 'client' in self.scope:
            log_kwargs['client_host'] = self.scope['client'][0]
            log_kwargs['client_host'] = self.scope['client'][1]
        if 'path' in self.scope:
            log_kwargs['path'] = self.scope['path']
        return log_kwargs

    def send_json(self, content, **kwargs):
        log_kwargs = self._scope_log_kwargs()
        log_kwargs['type'] = content['type']
        super().send_json(content, **kwargs)
        logger.info('websocket_message_sent', **log_kwargs)

    def receive(self, *args, **kwargs):
        try:
            super().receive(*args, **kwargs)
        except JSONDecodeError:
            self.close(4400)

    def receive_json(self, content, **kwargs):
        log_kwargs = self._scope_log_kwargs()
        log_kwargs['handled'] = False
        try:
            if 'type' not in content:
                self._error('no type in message')
                return

            log_kwargs['type'] = content['type']

            if content['type'][-4:] != '-plz':
                self._error('impolite request')
                return

            try:
                # Extract the message type before -plz and convert to a handler function name starting handle_
                # The static prefix prevents selection of in-built attributes which could lead to security vulnerabilities
                type = inflection.underscore(content['type'][:-4])
                handler = getattr(self, f"handle_{type}")
                log_kwargs['handled'] = True
            except AttributeError:
                self._error('invalid request')
                return
        finally:
            logger.info('websocket_message_received', **log_kwargs)

        try:
            message = self.protocol_module.Message.from_struct(content) if hasattr(self, 'protocol_module') else content
        except AttributeError:
            self._error('invalid message type')
            return
        try:
            handler(message, **kwargs)
            logger.info('websocket_message_handled', **log_kwargs)
        except Exception as e:
            logger.error('websocket_message_error', exc_info=e, **log_kwargs)


class HuntWebsocket(EventMixin, TeamMixin, BaseWebsocket):
    def connect(self):
        async_to_sync(self.channel_layer.group_add)(
           self._announcement_groupname(self.scope['tenant']), self.channel_name
        )
        super().connect()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self._announcement_groupname(self.scope['tenant']), self.channel_name
        )

    @classmethod
    def _announcement_groupname(cls, event, puzzle=None):
        if puzzle:
            return f'event-{event.id}.puzzle-{puzzle.id}.announcements'
        else:
            return f'event-{event.id}.announcements'

    @classmethod
    def send_announcement_msg(cls, event, puzzle, announcement):
        cls._send_message(cls._announcement_groupname(event, puzzle), web_protocol.new_announcement(announcement))

    @classmethod
    def send_delete_announcement_msg(cls, event, puzzle, announcement):
        cls._send_message(cls._announcement_groupname(event, puzzle), web_protocol.delete_announcement(announcement))


class PuzzleEventWebsocket(HuntWebsocket):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def __call__(self, scope, receive, send):
        scope['loop'] = asyncio.get_running_loop()
        await super().__call__(scope, receive, send)

    @classmethod
    def puzzle_groupname(cls, puzzle, team_id=None):
        event_id = puzzle.episode.event_id
        if team_id:
            return f'event-{event_id}.puzzle-{puzzle.id}.events.team-{team_id}'
        else:
            return f'event-{event_id}.puzzle-{puzzle.id}.events'

    def connect(self):
        logger.debug(
            'websocket connect',
            scope=self.scope,
        )
        keywords = self.scope['url_route']['kwargs']
        episode_number = keywords['episode_number']
        puzzle_number = keywords['puzzle_number']
        self.episode, self.puzzle = utils.event_episode_puzzle(self.scope['tenant'], episode_number, puzzle_number)
        async_to_sync(self.channel_layer.group_add)(
            self.puzzle_groupname(self.puzzle, self.team.id), self.channel_name
        )
        async_to_sync(self.channel_layer.group_add)(
            self._announcement_groupname(self.episode.event, self.puzzle), self.channel_name
        )
        super().connect()

    def disconnect(self, close_code):
        super().disconnect(close_code)
        async_to_sync(self.channel_layer.group_discard)(
            self.puzzle_groupname(self.puzzle, self.team.id), self.channel_name
        )

    def handle_guesses(self, content, **kwargs):
        if 'from' not in content:
            self._error('required field "from" is missing')
            return
        self.send_old_guesses(content['from'])

    def handle_unlocks(self, content, **kwargs):
        self.send_old_unlocks()

    def handle_hints(self, content, **kwargs):
        if 'from' not in content:
            self._error('required field "from" is missing')
            return
        self.send_old_hints(content['from'])

    def handle_cooldown(self, content, **kwargs):
        self.send_cooldown()

    @classmethod
    def send_new_unlock(cls, teamunlock):
        cls._send_message(
            cls.puzzle_groupname(teamunlock.team_puzzle_progress.puzzle, teamunlock.team_puzzle_progress.team_id),
            web_protocol.new_unlock(teamunlock)
        )

    @classmethod
    def send_delete_unlockguess(cls, teamunlock):
        cls._send_message(
            cls.puzzle_groupname(teamunlock.unlockanswer.unlock.puzzle, teamunlock.unlocked_by.by_team_id),
            web_protocol.delete_unlockguess(teamunlock)
        )

    @classmethod
    def send_new_guess(cls, guess):
        cls._send_message(cls.puzzle_groupname(guess.for_puzzle, guess.by_team_id), web_protocol.new_guesses([guess]))

    @classmethod
    def send_solved(cls, progress):
        cls._send_message(cls.puzzle_groupname(progress.puzzle, progress.team_id), web_protocol.solved(progress))

    @classmethod
    def send_change_unlock(cls, teamunlock):
        cls._send_message(
            cls.puzzle_groupname(teamunlock.unlockanswer.unlock.puzzle, teamunlock.team_puzzle_progress.team_id),
            web_protocol.change_unlock(teamunlock),
        )

    @classmethod
    def send_new_hint(cls, team_id, hint, accepted, obsolete):
        cls._send_message(cls.puzzle_groupname(hint.puzzle, team_id), web_protocol.new_hint(hint, accepted, obsolete))

    @classmethod
    def send_delete_hint(cls, team_id, hint):
        cls._send_message(cls.puzzle_groupname(hint.puzzle, team_id), web_protocol.delete_hint(hint))

    def send_old_guesses(self, start):
        guesses = models.Guess.objects.filter(for_puzzle=self.puzzle, by_team=self.team).order_by('given')
        if start != 'all':
            start = datetime.fromtimestamp(int(start) / 1000, dt_timezone.utc)
            # TODO: `start` is given by the client and is the timestamp of the most recently received guess.
            # the following could miss guesses if guesses get the same timestamp, though this is very unlikely.
            guesses = guesses.filter(given__gt=start).select_related('by', 'correct_for').seal()

            # If the puzzle was solved since the time requested, inform the client.
            try:
                progress = models.TeamPuzzleProgress.objects.select_related('puzzle', 'solved_by__by').seal().get(
                    puzzle=self.puzzle, team=self.team)
                # .get() doesn't transfer members to the retrieved object: avoid select_related for them
                progress.puzzle = self.puzzle
                progress.team = self.team
                if progress.solved_by and progress.solved_by.given > start:
                    self.send_json(web_protocol.solved(progress))
            except models.TeamPuzzleProgress.DoesNotExist:
                pass

            # The client requested guesses from a certain point in time, i.e. it already has some.
            # Even though these are "old" they're "new" in the sense that the user will never have
            # seen them before so should trigger the same UI effect.
            self.send_json(web_protocol.new_guesses(guesses))
        else:
            guesses = guesses.select_related('by').seal()
            self.send_json(web_protocol.old_guesses(guesses))

    @classmethod
    def send_all_unlocked_hints(cls):
        pass

    def send_old_hints(self, start):
        now = timezone.now()
        teamhints = models.TeamHint.objects.filter(
            team_puzzle_progress__team=self.team,
            team_puzzle_progress__puzzle=self.puzzle,
        ).unlocked(now)
        if start != 'all':
            teamhints = teamhints.filter(
                unlocked_at__gte=datetime.fromtimestamp(int(start) // 1000, dt_timezone.utc)
            )
            protocol_func = web_protocol.new_hint
        else:
            protocol_func = web_protocol.old_hint

        teamhints = teamhints.select_related('hint').seal()

        for th in teamhints:
            self.send_json(protocol_func(th.hint, bool(th.accepted_at), bool(th.obsoleted_by)))

    def send_old_unlocks(self):
        progress = models.TeamPuzzleProgress.objects.get(team=self.team, puzzle=self.puzzle)

        teamunlocks = models.TeamUnlock.objects.filter(
            team_puzzle_progress=progress,
        ).annotate(
            hidden=Case(
                When(unlockanswer__unlock__text='', then=Value(True)),
                default_value=Value(False),
            )
        ).order_by(
            'unlockanswer__unlock__text',
        ).select_related(
            'team_puzzle_progress',
            'unlockanswer__unlock',
            'unlocked_by',
        ).prefetch_related(
            'team_puzzle_progress__teamunlock_set__unlockanswer',
            Prefetch(
                'team_puzzle_progress__teamhint_set',
                to_attr='_visible_hints',
                queryset=models.TeamHint.objects.visible().select_related('hint'),
            ),
        ).seal()
        hints = progress.structured_hints()

        for tu in teamunlocks:
            if not tu.hidden or hints[tu.unlockanswer.unlock_id]:
                self.send_json(web_protocol.old_unlock(tu))

    def send_cooldown(self):
        behaviour = self.puzzle.episode.get_cooldown_behaviour()
        end_time = behaviour.end_time(self.scope['user'], self.team, self.puzzle)
        last_guessed = behaviour.last_guessed(self.scope['user'], self.team, self.puzzle)
        self.send_json(web_protocol.cooldown(end_time, last_guessed))


class UpdateStreamWebsocket(EventMixin, BaseWebsocket):
    protocol_module = update_stream

    @staticmethod
    def event_groupname(event, type):
        return f'event-{event.id}.{type}'

    def disconnect(self, close_code):
        discard = async_to_sync(self.channel_layer.group_discard)
        discard(self.event_groupname(self.scope['tenant'], 'episode-finishers'), self.channel_name)
        discard(self.event_groupname(self.scope['tenant'], 'puzzle-finishers'), self.channel_name)

    def receive_json(self, content, **kwargs):
        if 'type' not in content:
            self.send_json(update_stream.Error(code="MISSING_TYPE", message="Missing type in message").to_struct())
            return

        if content['type'][-4:] != '-plz':
            self.send_json(update_stream.Error(code="IMPOLITE_MESSAGE", message="Message is impolite").to_struct())
            return

        try:
            type = inflection.underscore(content['type'][:-4])
            handler = getattr(self, f'handle_{type}')
        except AttributeError:
            self.send_json(update_stream.Error(code="INVALID_TYPE", message="Invalid type in message").to_struct())
            return

        try:
            message = self.protocol_module.Message.from_struct(content)
        except JsonValidationError:
            self.send_json(update_stream.Error(code="INVALID_MESSAGE", message="Malformed message").to_struct())
            return

        handler(message, **kwargs)

    @classmethod
    def send_episode_finished(cls, episode, team):
        cls._send_message(
            cls.event_groupname(episode.event, 'episode-finishers'),
            update_stream.EpisodeFinished(episode_id=episode.id, team_id=team.id).to_struct(),
        )

    @classmethod
    def send_puzzle_finished(cls, puzzle, team):
        cls._send_message(
            cls.event_groupname(puzzle.episode.event, 'puzzle-finishers'),
            update_stream.PuzzleFinished(puzzle_id=puzzle.id, team_id=team.id).to_struct(),
        )

    def _authenticate(self, message):
        if not message.api_token:
            self.send_json(update_stream.Error(code="MISSING_TOKEN", message="Missing API token in message").to_struct())
            return None
        try:
            return APIToken.objects.filter(token=message.api_token).get()
        except DjangoValidationError:
            self.send_json(update_stream.Error(code="MALFORMED_TOKEN", message="Malformed API token in message").to_struct())
            return None
        except APIToken.DoesNotExist:
            self.send_json(update_stream.Error(code="INVALID_TOKEN", message="Unrecognised API token in message").to_struct())
            return None

    def handle_episode_finishers(self, message, **kwargs):
        token = self._authenticate(message)
        if not token:
            return
        if not token.authorise(self.scope['tenant'], 'stream', 'episode-finishers', message):
            self.send_json(update_stream.Error(code="UNAUTHORISED_TOKEN", message="API token not authorised for operation").to_struct())
            return

        # First subscribe to receive new updates
        async_to_sync(self.channel_layer.group_add)(self.event_groupname(self.scope['tenant'], 'episode-finishers'), self.channel_name)
        # Then bulk-send all existing finishers to ensure none have been missed
        for episode in Episode.objects.all().prefetch_related('puzzle_set'):
            for team in Team.objects.annotate(
                solved_count=Count('teampuzzleprogress', filter=Q(
                    teampuzzleprogress__puzzle__episode=episode,
                    teampuzzleprogress__solved_by__isnull=False,
                )),
            ).filter(
                solved_count__gt=0,
                solved_count=episode.puzzle_set.count(),
            ):
                self.send_json(update_stream.EpisodeFinished(episode_id=episode.id, team_id=team.id).to_struct())

    def handle_puzzle_finishers(self, message, **kwargs):
        token = self._authenticate(message)
        if not token:
            return
        if not token.authorise(self.scope['tenant'], 'stream', 'puzzle-finishers', message):
            self.send_json(update_stream.Error(code="UNAUTHORISED_TOKEN", message="API token not authorised for operation").to_struct())
            return

        # First subscribe to receive new updates
        async_to_sync(self.channel_layer.group_add)(self.event_groupname(self.scope['tenant'], 'puzzle-finishers'), self.channel_name)
        # Then bulk-send all existing finishers to ensure none have been missed
        for tpp in TeamPuzzleProgress.objects.filter(solved_by__isnull=False).seal():
            self.send_json(update_stream.PuzzleFinished(puzzle_id=tpp.puzzle_id, team_id=tpp.team_id).to_struct())
