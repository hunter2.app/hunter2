# Copyright (C) 2018 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
from django.conf import settings
from django.db.models import Prefetch
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.views.generic import TemplateView
from django_tenants.urlresolvers import reverse
from django_tenants.utils import tenant_context

from events.models import Event, Domain


class EventList(TemplateView):
    template_name = 'eventlist.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        now = timezone.now()
        context['current_events'] = []
        context['future_events'] = []
        context['past_events'] = []
        for event in Event.objects.filter(
            is_published=True
        ).prefetch_related(
            Prefetch('domains', queryset=Domain.objects.filter(is_primary=True))
        ):
            with tenant_context(event):
                url = reverse('event', settings.ROOT_URLCONF)
                start_date = event.puzzles_available_time()
                e = {
                    'name': event.name,
                    'url': f'//{event.domains.all()[0]}{url}',
                    'start_date': start_date,
                    'end_date': event.end_date,
                }
                match start_date, event.end_date:
                    case (_, end) if end < now:
                        context['past_events'].append(e)
                    case (start, _) if start > now:
                        context['future_events'].append(e)
                    case _:
                        context['current_events'].append(e)
        return context


class PrivacyView(TemplateView):
    template_name = "privacy.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['privacy_policy'] = mark_safe(self.request.site_configuration.privacy_policy)
        return context
