#!/usr/bin/env bash
# Copyright (C) 2024 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

set -euo pipefail
IFS=$'\n\t'

# This script is intended to be run before upgrading to 3.0.0
# It will remove the DB exporter stuff from the DB then shutdown hunter2 and migrate the DB data to a new volume in PostgreSQL 14.x format

docker compose exec -T db psql -v ON_ERROR_STOP=1 -U postgres <<EOSQL
    DO \$\$ BEGIN
        DROP SCHEMA IF EXISTS postgres_exporter CASCADE;
        DROP EXTENSION IF EXISTS pg_stat_statements CASCADE;
        IF EXISTS (
            SELECT * FROM pg_roles WHERE rolname = 'postgres_exporter'
        ) THEN
            DROP OWNED BY postgres_exporter;
            DROP ROLE postgres_exporter;
        END IF;
    END \$\$;
EOSQL

docker compose down
docker run -i --rm \
    -v pg_upgrade_logs:/var/lib/postgresql \
    -v hunter2_db14:/var/lib/postgresql/14/data \
    -v hunter2_db16:/var/lib/postgresql/16/data \
    alpine:3.19 /bin/sh -c '
addgroup -g 104 postgres
adduser -G postgres -u 999 -D postgres
apk add --no-cache postgresql14 postgresql14-contrib postgresql16 postgresql16-contrib
chown postgres: .
chown -R postgres: /var/lib/postgresql/14/data
chown -R postgres: /var/lib/postgresql/16/data
su postgres -c '\''
initdb /var/lib/postgresql/16/data
pg_upgrade -b /usr/libexec/postgresql14 -d /var/lib/postgresql/14/data -D /var/lib/postgresql/16/data
cp /var/lib/postgresql/14/data/postgresql.conf /var/lib/postgresql/16/data/postgresql.conf
echo "host all all all scram-sha-256" >> /var/lib/postgresql/16/data/pg_hba.conf
'\''
'
