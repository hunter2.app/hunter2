# Copyright (C) 2018 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.


import json
import uuid
from unittest.mock import Mock

import pytest
from django.apps import apps
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.urls import reverse

from accounts.factories import UserFactory
from events.factories import EventFactory, EventFileFactory
from events.test import EventAwareTestCase, EventTestCase
from hunter2.factories import APITokenFactory
from . import permissions, views
from .factories import TeamFactory, TeamMemberFactory, SizeCategoryFactory
from .forms import TeamForm
from .middleware import TeamMiddleware
from .models import Team, TeamRole, TeamMembership, SizeCategory


class FactoryTests(EventTestCase):

    def test_team_factory_default_construction(self):
        TeamFactory.create()

    def test_team_member_factory_default_construction(self):
        TeamMemberFactory.create()

    def test_size_category_factory_default_construction(self):
        SizeCategoryFactory.create()

    def test_size_category_factory_batch(self):
        SizeCategoryFactory.create_batch(2)

        assert SizeCategory.max_team_size() == 4


@pytest.mark.usefixtures('event')
class TestDjangoAdmin:
    @pytest.fixture
    def admin_client(self, tenant_client):
        admin_user = TeamMemberFactory(team__role=TeamRole.ADMIN, is_staff=True)
        tenant_client.force_login(admin_user)
        return tenant_client

    def test_models_registered(self):
        models = apps.get_app_config('teams').get_models()
        # Models which don't need to be directly registered due to being a through model
        inline_models = (TeamMembership, )
        for model in models:
            if model not in inline_models:
                assert isinstance(admin.site._registry[model], admin.ModelAdmin)

    def test_load_create_forms(self, admin_client):
        response = admin_client.get(reverse('admin:teams_team_add'))
        assert response.status_code == 200

    def test_load_change_list(self, admin_client):
        response = admin_client.get(reverse('admin:teams_team_changelist'))
        assert response.status_code == 200

    def test_team_change_form_regular_error(self):
        users = UserFactory.create_batch(3)
        team = TeamFactory(members=users)
        form = TeamForm(instance=team, data={
            "name": team.name,
            "role": "",  # Attempt to set empty string for required field
            "at_event": str(team.at_event_id),
            "move_user": "",
            "invites": "",
            "requests": "",
            "members": [str(m.id) for m in team.members.all()],
        })
        assert not form.is_valid()
        assert 'role' in form.errors

    def test_team_change_form_valid(self):
        users = UserFactory.create_batch(3)
        team = TeamFactory(members=users)
        form = TeamForm(instance=team, data={
            "name": team.name,
            "role": team.role,
            "at_event": str(team.at_event_id),
            "move_user": "",
            "invites": "",
            "requests": "",
            "members": [str(m.id) for m in team.members.all()],
        })
        assert form.is_valid()

    def test_team_change_move_member(self):
        users = UserFactory.create_batch(3)
        team = TeamFactory(members=users)
        other_users = UserFactory.create_batch(2)
        TeamFactory(members=other_users)
        form = TeamForm(instance=team, data={
            "name": team.name,
            "role": team.role,
            "at_event": str(team.at_event_id),
            "move_user": "",
            "invites": "",
            "requests": "",
            "members": [str(m.id) for m in team.members.all()] + [str(other_users[0].id)],
        })
        assert not form.is_valid()
        assert 'move_user' in form.errors

    def test_team_change_move_members(self):
        users = UserFactory.create_batch(3)
        team = TeamFactory(members=users)
        other_users = UserFactory.create_batch(2)
        TeamFactory(members=other_users)
        form = TeamForm(instance=team, data={
            "name": team.name,
            "role": team.role,
            "at_event": str(team.at_event_id),
            "move_user": "",
            "invites": "",
            "requests": "",
            "members": [str(m.id) for m in team.members.all()] + [str(m.id) for m in other_users],
        })
        assert not form.is_valid()
        assert 'move_user' in form.errors

    def test_team_change_move_member_confirm(self):
        users = UserFactory.create_batch(3)
        team = TeamFactory(members=users)
        other_users = UserFactory.create_batch(2)
        other_team = TeamFactory(members=other_users)
        form = TeamForm(instance=team, data={
            "name": team.name,
            "role": team.role,
            "at_event": str(team.at_event_id),
            "move_user": "on",
            "invites": "",
            "requests": "",
            "members": [str(m.id) for m in team.members.all()] + [str(other_users[0].id)],
        })
        assert form.is_valid()
        other_team.refresh_from_db()
        assert other_team.members.get() == other_users[1]

    def test_team_change_move_all_members_confirm(self):
        users = UserFactory.create_batch(3)
        team = TeamFactory(members=users)
        other_users = UserFactory.create_batch(2)
        other_team = TeamFactory(members=other_users)
        form = TeamForm(instance=team, data={
            "name": team.name,
            "role": team.role,
            "at_event": str(team.at_event_id),
            "move_user": "on",
            "invites": "",
            "requests": "",
            "members": [str(m.id) for m in team.members.all()] + [str(m.id) for m in other_users],
        })
        assert form.is_valid()
        with pytest.raises(Team.DoesNotExist):
            other_team.refresh_from_db()


class TeamMultiEventTests(EventAwareTestCase):
    def test_team_name_uniqueness(self):
        old_event = EventFactory()
        new_event = EventFactory()

        old_event.activate()
        team1 = TeamFactory(at_event=old_event)

        # Check that creating a team with the same name on the old event is not allowed.
        with self.assertRaises(ValidationError):
            Team(name=team1.name, at_event=old_event).full_clean()

        new_event.activate()
        # Check that the new event team does not raise a validation error
        TeamFactory(name=team1.name, at_event=new_event)

        new_event.deactivate()


class TeamRulesTests(EventTestCase):
    def test_max_team_size(self):
        event = self.tenant
        SizeCategory(max=2).save()
        team = TeamFactory(at_event=event)

        # Add 3 users to a team when that max is less than that.
        self.assertLess(SizeCategory.max_team_size(), 3)
        users = UserFactory.create_batch(3)

        with self.assertRaises(ValidationError):
            for user in users:
                team.members.add(user)

    def test_one_team_per_member_per_event(self):
        event = self.tenant
        teams = TeamFactory.create_batch(2, at_event=event)
        user = UserFactory()

        with self.assertRaises(ValidationError):
            teams[0].members.add(user)
            teams[1].members.add(user)


class TestSizeCategories:
    def test_max_team_size(self, event):
        SizeCategory(max=4).save()
        SizeCategory(max=2).save()

        assert SizeCategory.max_team_size() == 4

    def test_max_team_size_none(self, event):
        assert SizeCategory.max_team_size() is None

    def test_max_team_size_unlimited(self, event):
        SizeCategory(max=None).save()
        SizeCategory(max=2).save()

        assert SizeCategory.max_team_size() is None

    def test_team_get_size_category(self, player_team):
        player_team.members.add(UserFactory())
        player_team.members.add(UserFactory())

        size = player_team.members.count()
        assert size == 3

        SizeCategory(max=size-1).save()
        target = SizeCategory(max=size)
        target.save()
        SizeCategory(max=size+1).save()
        SizeCategory(max=None).save()

        assert player_team.get_size_category() == target

    def test_lower_limit(self, event):
        SizeCategory(max=2).save()
        SizeCategory(max=5).save()
        SizeCategory(max=None).save()

        lower_limits = [sc.min for sc in SizeCategory.objects.all().with_lower_limit()]
        assert lower_limits == [1, 3, 6]

    def test_display_range_minmax(self, event):
        cat = SizeCategory(max=4)
        cat.save()
        cat.min = 3

        assert cat.display_range() == '3&ndash;4'

    def test_display_range_max(self, event):
        cat = SizeCategory(max=4)
        cat.save()

        assert cat.display_range() == 'up to 4'

    def test_display_range_min(self, event):
        cat = SizeCategory()
        cat.save()
        cat.min = 3

        assert cat.display_range() == '3 or more'

    def test_display_range_unbounded(self, event):
        cat = SizeCategory()
        cat.save()

        assert cat.display_range() == 'unlimited'

    def test_display_label_some(self, event):
        cat = SizeCategory(label='some')
        cat.save()

        assert cat.display_label() == 'some'

    def test_display_label_none(self, event):
        cat = SizeCategory(max=2, label='')
        cat.save()

        assert cat.display_label() == 'up to 2'

    def test_with_size(self, player_team):
        # player_team has size 1
        team2 = TeamFactory(members=UserFactory.create_batch(2))
        # team2 has size 2
        team3 = TeamFactory(members=UserFactory.create_batch(3))
        # team3 has size 3
        team4 = TeamFactory(members=UserFactory.create_batch(4))
        # team4 has size 4

        cat1 = SizeCategory(max=2)
        cat1.save()
        cat2 = SizeCategory(max=3)
        cat2.save()
        cat3 = SizeCategory(max=None)
        cat3.save()

        with_sizes = Team.objects.all().with_size()
        assert len(with_sizes) == 4
        for team in with_sizes:
            if team == player_team:
                assert team.size_category == cat1
            elif team == team2:
                assert team.size_category == cat1
            elif team == team3:
                assert team.size_category == cat2
            elif team == team4:
                assert team.size_category == cat3


class TeamCreateTests(EventTestCase):
    def test_team_create(self):
        creator = UserFactory()
        team_template = TeamFactory.build()

        self.client.force_login(creator)
        response = self.client.post(
            reverse('create_team'),
            {
                'name': team_template.name,
            },
        )
        self.assertEqual(response.status_code, 302)
        team = Team.objects.get(name=team_template.name)
        self.assertTrue(creator in team.members.all())

    def test_team_middleware(self):
        request = Mock()
        request.tenant = self.tenant
        request.user = UserFactory()
        # Apply the middleware (ignore the result; we only care about what it does in the db)
        TeamMiddleware(Mock())(request)
        Team.objects.get(members=request.user)

    def test_automatic_creation(self):
        user = UserFactory()
        self.client.force_login(user)
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        Team.objects.get(members=user)


class TestTeamManagementView:
    url = reverse('manage_team')

    def test_no_team(self, tenant_client):
        tenant_client.force_login(UserFactory())
        response = tenant_client.get(self.url)
        assert response.status_code == 200

    def test_on_team(self, tenant_client):
        tenant_client.force_login(TeamMemberFactory())
        response = tenant_client.get(self.url)
        assert response.status_code == 200

    def test_accept_size_data(self, tenant_client):
        cat1 = SizeCategoryFactory()
        cat2 = SizeCategoryFactory()
        team = TeamFactory(members=UserFactory.create_batch(2))
        tenant_client.force_login(team.members.first())
        response = tenant_client.get(self.url)
        assert response.status_code == 200
        data = response.context['size_data']

        assert data['num_size_categories'] == 2
        assert data['current_category'] == {
            'id': cat1.id,
            'label': cat1.label,
        }
        assert data['category_after_accept'] == {
            'id': cat2.id,
            'label': cat2.label,
        }
        assert data['is_full'] == team.is_full()

    def test_size_data(self, event):
        cat1 = SizeCategoryFactory()
        cat2 = SizeCategoryFactory()
        team = TeamMemberFactory().teams.get()
        data = views.size_data(2, cat1, cat2, 10, team)

        assert data['num_size_categories'] == 2
        assert data['current_category'] == {
            'id': cat1.id,
            'label': cat1.label,
        }
        assert data['category_after_accept'] == {
            'id': cat2.id,
            'label': cat2.label,
        }
        assert data['is_full'] == team.is_full()


class RequestTests(EventTestCase):
    def setUp(self):
        self.event = self.tenant
        self.team_admin = UserFactory()
        self.applicant = UserFactory()
        self.team = TeamFactory(at_event=self.event, members={self.team_admin})

        # The "applicant" is requesting a place on "team".
        self.client.force_login(self.applicant)
        response = self.client.post(
            reverse('request', kwargs={'team_id': self.team.id}),
            json.dumps({}),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 200)

    def test_request_accept(self):
        self.client.force_login(self.team_admin)

        # Send an invalid UUID request
        response = self.client.post(
            reverse('acceptrequest', kwargs={'team_id': self.team.id}),
            json.dumps({
                'user': '123',
                'currentSize': 1,
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()['message'], 'Invalid User UUID')

        # Send a valid UUID that doesn't exist
        response = self.client.post(
            reverse('acceptrequest', kwargs={'team_id': self.team.id}),
            json.dumps({
                'user': '01234567-89ab-cdef-0123-456789abcdef',
                'currentSize': 1,
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json()['message'], 'User does not exist')

        response = self.client.post(
            reverse('acceptrequest', kwargs={'team_id': self.team.id}),
            json.dumps({
                'user': str(self.applicant.uuid),
                'currentSize': 1,
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.applicant in self.team.members.all())
        self.assertFalse(self.applicant in self.team.requests.all())

        # Now try to send a request to the full team
        SizeCategory(max=2).save()
        applicant2 = UserFactory()
        self.client.force_login(applicant2)
        response = self.client.post(
            reverse('request', kwargs={'team_id': self.team.id}),
            json.dumps({}),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 400)
        self.assertFalse(applicant2 in self.team.members.all())
        self.assertFalse(applicant2 in self.team.requests.all())

        # Now bypass the request mechanism to add a request anyway and
        # check it can't be accepted
        self.team.requests.add(applicant2)
        self.client.force_login(self.team_admin)
        response = self.client.post(
            reverse('acceptrequest', kwargs={'team_id': self.team.id}),
            json.dumps({
                'user': str(applicant2.uuid),
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 400)
        self.assertFalse(applicant2 in self.team.members.all())
        # Finally check we cleaned up the request after failing
        self.assertFalse(applicant2 in self.team.requests.all())

    def test_accept_size_data(self):
        members = UserFactory.create_batch(2)
        team = TeamFactory(members=members)
        SizeCategoryFactory(max=2)
        cat2 = SizeCategoryFactory(max=3)

        team.requests.add(self.applicant)

        self.client.force_login(members[0])
        response = self.client.post(
            reverse('acceptrequest', kwargs={'team_id': team.id}),
            json.dumps({
                'user': str(self.applicant.uuid),
                'currentSize': 2,
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 200)
        size_data = response.json()['size_data']
        self.assertEqual(size_data['num_size_categories'], 2)
        self.assertEqual(size_data['current_size'], 3)
        self.assertEqual(size_data['current_category'], {
            'id': cat2.id,
            'label': cat2.label,
        })
        self.assertEqual(size_data['category_after_accept'], {
            'id': None,
            'label': None,
        })
        self.assertEqual(size_data['is_full'], True)

    def test_accept_on_stale_size(self):
        members = UserFactory.create_batch(2)
        team = TeamFactory(members=members)
        team.requests.add(self.applicant)

        self.client.force_login(members[0])
        response = self.client.post(
            reverse('acceptrequest', kwargs={'team_id': team.id}),
            json.dumps({
                'user': str(self.applicant.uuid),
                'currentSize': 1,
            }),
            content_type='application/json',
        )

        self.assertEqual(response.status_code, 400)

        response = self.client.post(
            reverse('acceptrequest', kwargs={'team_id': team.id}),
            json.dumps({
                'user': str(self.applicant.uuid),
                'currentSize': 2,
            }),
            content_type='application/json',
        )

        self.assertEqual(response.status_code, 200)

    def test_request_cancel(self):
        response = self.client.post(
            reverse('cancelrequest', kwargs={'team_id': self.team.id}),
            json.dumps({}),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.applicant in self.team.members.all())
        self.assertFalse(self.applicant in self.team.requests.all())

    def test_request_deny(self):
        # Send an invalid UUID request
        response = self.client.post(
            reverse('denyrequest', kwargs={'team_id': self.team.id}),
            json.dumps({
                'user': '123',
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 400)

        self.client.force_login(self.team_admin)
        response = self.client.post(
            reverse('denyrequest', kwargs={'team_id': self.team.id}),
            json.dumps({
                'user': str(self.applicant.uuid),
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.applicant in self.team.members.all())
        self.assertFalse(self.applicant in self.team.requests.all())

    def test_request_views_forbidden(self):
        self.client.logout()
        response = self.client.post(
            reverse('request', kwargs={'team_id': self.team.id}),
            json.dumps({
                'user': str(self.team_admin.uuid),
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 403)
        response = self.client.post(
            reverse('cancelrequest', kwargs={'team_id': self.team.id}),
            json.dumps({
                'user': str(self.team_admin.uuid),
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 403)
        response = self.client.post(
            reverse('acceptrequest', kwargs={'team_id': self.team.id}),
            json.dumps({
                'user': str(self.team_admin.uuid),
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 403)
        response = self.client.post(
            reverse('denyrequest', kwargs={'team_id': self.team.id}),
            json.dumps({
                'user': str(self.team_admin.uuid),
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 403)


class RulesTests(EventTestCase):
    def test_is_admin_for_event_true(self):
        user = TeamMemberFactory(team__role=TeamRole.ADMIN)
        self.assertTrue(permissions.is_admin_for_event.test(user, None))
        self.assertTrue(permissions.is_admin_for_event.test(user, self.tenant))

    def test_is_admin_for_event_false(self):
        user = TeamMemberFactory(team__role=TeamRole.PLAYER)
        self.assertFalse(permissions.is_admin_for_event.test(user, None))
        self.assertFalse(permissions.is_admin_for_event.test(user, self.tenant))

    def test_is_admin_for_event_with_no_team(self):
        user = UserFactory()
        self.assertFalse(permissions.is_admin_for_event.test(user, self.tenant))

    def test_is_admin_for_event_child_true(self):
        user = TeamMemberFactory(team__role=TeamRole.ADMIN)
        child = EventFileFactory()
        self.assertTrue(permissions.is_admin_for_event_child.test(user, None))
        self.assertTrue(permissions.is_admin_for_event_child.test(user, self.tenant))
        self.assertTrue(permissions.is_admin_for_event_child.test(user, child))

    def test_is_admin_for_event_child_false(self):
        user = TeamMemberFactory(team__role=TeamRole.PLAYER)
        child = EventFileFactory()
        self.assertFalse(permissions.is_admin_for_event_child.test(user, None))
        self.assertFalse(permissions.is_admin_for_event_child.test(user, self.tenant))
        self.assertFalse(permissions.is_admin_for_event_child.test(user, child))

    def test_is_admin_for_event_child_type_error(self):
        user = UserFactory()
        with self.assertRaises(TypeError):
            permissions.is_admin_for_event_child(user, "A string is not an event child")

    def test_is_admin_for_schema_event_true(self):
        user = TeamMemberFactory(team__role=TeamRole.ADMIN)
        self.assertTrue(permissions.is_admin_for_schema_event(user, None))

    def test_is_admin_for_schema_event_false(self):
        user = TeamMemberFactory(team__role=TeamRole.PLAYER)
        self.assertFalse(permissions.is_admin_for_schema_event(user, None))


class NoSchemaRulesTests(EventAwareTestCase):
    def test_is_admin_for_schema_event_no_event(self):
        event = EventFactory()
        event.activate()
        user = TeamMemberFactory(team__at_event=event, team__role=TeamRole.ADMIN)
        event.deactivate()
        self.assertFalse(permissions.is_admin_for_schema_event(user, None))


class TeamListTests(EventTestCase):
    def setUp(self):
        self.team = TeamFactory()
        self.player = UserFactory()
        self.player_team = TeamFactory(name='', members=self.player)
        self.url = reverse('team_list')

    def test_negative_auth(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 401)

    def test_json_list(self):
        api_token = APITokenFactory(scopes=[{"operation": "list", "resource": "teams", "fields": {}}])
        response = self.client.get(self.url, HTTP_AUTHORIZATION=f'Bearer {api_token.token}')
        self.assertEqual(response.status_code, 200)
        response_data = response.json()
        self.assertEqual(response_data['items'][0]['id'], self.team.id)
        self.assertEqual(response_data['items'][0]['type'], 'team')
        self.assertEqual(response_data['items'][0]['name'], self.team.name)
        self.assertEqual(response_data['items'][1]['id'], self.player_team.id)
        self.assertEqual(response_data['items'][1]['type'], 'player')
        self.assertEqual(response_data['items'][1]['name'], self.player.username)

    def test_text_list(self):
        api_token = APITokenFactory(scopes=[{"operation": "list", "resource": "teams", "fields": {}}])
        response = self.client.get(self.url, HTTP_ACCEPT='text/plain', HTTP_AUTHORIZATION=f'Bearer {api_token.token}')
        self.assertEqual(response.status_code, 200)
        response_data = response.content.decode('utf-8').split('\n')
        self.assertEqual(response_data[0], f'+{self.team.name}-')
        self.assertEqual(response_data[1], f'-{self.player.username}-')


class TestTeamMethods:
    def test_get_verbose_name_normal(self, event):
        team = TeamFactory()
        assert Team.objects.get().get_verbose_name() == team.name

    def test_get_verbose_name_anonymous(self, event):
        player = UserFactory()
        team = TeamFactory(name=None, members={player})
        assert str(team.id) in Team.objects.get().get_verbose_name()

    def test_get_verbose_name_anonymous_prefetched(self, event):
        player = UserFactory()
        TeamFactory(name=None, members={player})
        assert player.username in Team.objects.all().prefetch_related('members').get().get_verbose_name()

    def test_get_verbose_name_edge_cases(self, event):
        # We don't really care what these edge cases produce as long as they don't break
        team = TeamFactory(name=None, members={})
        Team.objects.get().get_verbose_name()
        Team.objects.all().prefetch_related('members').get().get_verbose_name()

        team.members.add(UserFactory())
        team.members.add(UserFactory())
        Team.objects.get().get_verbose_name()
        Team.objects.all().prefetch_related('members').get().get_verbose_name()


@pytest.mark.usefixtures('event')
class TestTeamInfo:
    def test_bad_token(self, tenant_client):
        team = TeamFactory()
        url = reverse('team_info', kwargs={
            'team_token': team.token,
        })
        api_token = APITokenFactory(scopes=[{"operation": "test", "resource": "things", "fields": {}}])
        response = tenant_client.get(url, HTTP_AUTHORIZATION=f'Bearer {api_token.token}')
        assert response.status_code == 403

    def test_invalid_team_token(self, tenant_client):
        TeamFactory()
        url = reverse('team_info', kwargs={
            'team_token': uuid.uuid4(),
        })
        api_token = APITokenFactory(scopes=[{"operation": "read", "resource": "team", "fields": {}}])
        response = tenant_client.get(url, HTTP_AUTHORIZATION=f'Bearer {api_token.token}')
        assert response.status_code == 404

    def test_team_found(self, tenant_client):
        team = TeamFactory()
        url = reverse('team_info', kwargs={
            'team_token': team.token,
        })
        api_token = APITokenFactory(scopes=[{"operation": "read", "resource": "team", "fields": {}}])
        response = tenant_client.get(url, HTTP_AUTHORIZATION=f'Bearer {api_token.token}')
        assert response.status_code == 200
        assert response.json()['team']['name'] == team.name
