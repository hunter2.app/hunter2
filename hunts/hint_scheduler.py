# Copyright (C) 2023 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
import asyncio
from collections import defaultdict
from datetime import timedelta

import channels
import structlog
from asgiref.sync import async_to_sync
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from channels.layers import get_channel_layer
from django.db import connection
from django.db.models import Prefetch
from django.utils import timezone
from django_tenants.utils import get_tenant_model

from events.models import Event
from hunts import models
from hunts.models import TeamPuzzleProgress
from hunts.websocket.consumers import PuzzleEventWebsocket
from hunts.websocket.protocol import web as web_protocol

CHANNEL_NAME = 'hint_scheduler'

logger = structlog.get_logger(__name__)


class HintScheduler(AsyncConsumer):
    def __init__(self, *args, **kwargs):
        self.hint_notification_tasks = defaultdict(dict)

    @classmethod
    def setup(cls):
        now = timezone.now()
        cutoff = now - timedelta(days=30)
        layer = get_channel_layer()

        logger.info("Hint scheduler setting up")
        for event in Event.objects.all():
            event.activate()
            try:
                # There are two "types" of hints we need to schedule during startup:
                # 1. unrevealed hints. These have unlocked_at in the future.
                # 2. hints which may have been scheduled to be revealed while the scheduler was down or broken.
                #    These were unlocked in the past, but we can assume they aren't accepted, otherwise it is the
                #    corresponding signal handler's responsibility to send the websocket messages; a broken hint
                #    scheduler doesn't matter. (In comparison if the websocket process breaks the client knows and
                #    will reconnect and request hints when it does; the client knows nothing about the scheduler)
                #    To limit the amount of work done here, we only look for hints that should've been sent within
                #    the last 30 days. The rationale is that players won't actually stay connected for that long; they
                #    will in practice reconnect/reload the page and get those old hints directly, and this effectively
                #    caps the amount of hints that will be sent on startup.
                cls.schedule_hint_notifications(
                    models.TeamHint.objects.filter(unlocked_at__gt=now).seal()
                )

                for tpp in TeamPuzzleProgress.objects.all():
                    groupname = PuzzleEventWebsocket.puzzle_groupname(tpp.puzzle, tpp.team_id)
                    teamhints = list(cls._prefetch_teamhints(models.TeamHint.objects.visible(now).filter(
                        team_puzzle_progress=tpp, unlocked_at__gt=cutoff, accepted_at=None
                    )))

                    # async_to_sync has a non-trivial overhead, so don't call it unless there's actually work to do
                    if teamhints:
                        async_to_sync(cls.reveal_hints)(groupname, layer, teamhints)
            finally:
                event.deactivate()

    @database_sync_to_async
    def _get_teamhints(self, message):
        # This all needs to be done in a single sync_to_async function so that we get one
        # transaction and persist the tenant schema activation throughout.
        tenant = get_tenant_model().objects.get(id=message['tenant_id'])
        tenant.activate()
        teamhints = list(self._prefetch_teamhints(models.TeamHint.objects.filter(
            id__in=message['teamhint_ids']
        )))

        return teamhints

    @classmethod
    def _prefetch_teamhints(cls, teamhint_queryset):
        return teamhint_queryset.select_related(
            'team_puzzle_progress__team',
            'hint__puzzle__episode',
            'hint__start_after',
        ).seal()

    @classmethod
    def _log_params(cls, teamhint):
        return {
            'event_id': teamhint.hint.puzzle.episode.event_id,
            'hint_uid': teamhint.hint.compact_id,
            'team_hint_id': teamhint.id,
            'puzzle_id': teamhint.team_puzzle_progress.puzzle_id,
            'team_id': teamhint.team_puzzle_progress.team_id,
        }

    async def hint_notification_task(self, teamhint):
        layer = get_channel_layer()
        groupname = PuzzleEventWebsocket.puzzle_groupname(
            teamhint.hint.puzzle, teamhint.team_puzzle_progress.team_id
        )
        time = teamhint.unlocked_at
        delay = (time - timezone.now()).total_seconds()
        obsolete = teamhint.obsoleted_by_id is not None

        log_params = self._log_params(teamhint)

        if delay > 0:
            logger.info(
                "Scheduling hint reveal task",
                time=time,
                delay=delay,
                **log_params,
            )
            # the "unlocked at" time is not relevant for obsolete hints
            if not obsolete:
                await asyncio.sleep(delay)
            logger.info(
                "Executing scheduled hint reveal task",
                **log_params,
            )
        else:
            logger.info(
                "Executing immediate hint reveal task",
                **log_params,
            )

        await self.reveal_hints(groupname, layer, [teamhint])

    @classmethod
    async def reveal_hints(cls, groupname, layer, teamhints):
        """Reveal hints by sending them through the websocket. The hints must all be for the same team and puzzle,
        corresponding to the groupname."""
        for teamhint in teamhints:
            obsolete = teamhint.obsoleted_by_id is not None
            log_params = cls._log_params(teamhint)
            try:
                # If the unlock this hint depends on is hidden, it won't have been sent yet, and needs to be revealed now.
                if teamhint.hint.start_after_id and teamhint.hint.start_after.hidden:
                    logger.debug(
                        "Revealing hidden unlocks",
                        **log_params,
                    )
                    teamunlocks = teamhint.team_puzzle_progress.teamunlock_set.filter(
                        unlockanswer__unlock_id=teamhint.hint.start_after_id,
                        unlockanswer__unlock__text=''
                    ).select_related(
                        'unlocked_by',
                        'unlockanswer__unlock',
                    ).prefetch_related(
                        'team_puzzle_progress__teamunlock_set__unlockanswer__unlock',
                        Prefetch(
                            'team_puzzle_progress__teamhint_set',
                            to_attr='_visible_hints',
                            queryset=models.TeamHint.objects.visible().select_related('hint'),
                        ),
                    ).seal()

                    async for teamunlock in teamunlocks:
                        await layer.group_send(
                            groupname, {
                                'type': 'send_json_msg',
                                'content': web_protocol.new_unlock(teamunlock),
                            }
                        )
                await layer.group_send(
                    groupname, {
                        'type': 'send_json_msg',
                        'content': web_protocol.new_hint(
                            teamhint.hint, bool(teamhint.accepted_at), obsolete
                        ),
                    }
                )
            except Exception as e:
                logger.error(
                    'Error while processing hint',
                    exc_info=e,
                    **log_params,
                )
                raise

    async def schedule_hint_notification_real(self, message):
        logger.debug('Scheduling hint reveal task')
        await self.cancel_hint_notifications_real(message, log_error_if_absent=False)
        teamhints = await self._get_teamhints(message)
        tenant_id = message['tenant_id']
        for teamhint in teamhints:
            logger.debug(
                "Scheduling hint notification",
                **self._log_params(teamhint),
            )
            self.hint_notification_tasks[tenant_id][teamhint.id] = asyncio.create_task(
                self.hint_notification_task(teamhint)
            )

    @classmethod
    def schedule_hint_notifications(cls, teamhints):
        layer = channels.layers.get_channel_layer()
        logger.debug("scheduling: synchronous layer calling channel layer")
        async_to_sync(layer.send)(
            CHANNEL_NAME,
            {
                'type': 'schedule_hint_notification_real',
                'tenant_id': connection.tenant.id,
                'teamhint_ids': [teamhint.id for teamhint in teamhints],
            },
        )

    async def cancel_hint_notifications_real(self, message, log_error_if_absent=True):
        tenant_id = message['tenant_id']
        for teamhint_id in message['teamhint_ids']:
            logger.debug(
                "Cancelling hint",
                event_id=tenant_id,
                team_hint_id=teamhint_id
            )
            try:
                self.hint_notification_tasks[tenant_id][teamhint_id].cancel()
                del self.hint_notification_tasks[tenant_id][teamhint_id]
            except KeyError:
                log_func = logger.error if log_error_if_absent else logger.debug
                log_func(
                    "Attempted to cancel non-existent hint task",
                    event_id=tenant_id,
                    team_hint_id=teamhint_id,
                    stack_info=log_error_if_absent,
                )

    @classmethod
    def cancel_hint_notifications(cls, teamhints):
        layer = channels.layers.get_channel_layer()
        logger.debug("cancelling: synchronous layer calling channel layer")
        async_to_sync(layer.send)(
            CHANNEL_NAME,
            {
                'type': 'cancel_hint_notifications_real',
                'tenant_id': connection.tenant.id,
                'teamhint_ids': [teamhint.id for teamhint in teamhints],
            },
        )
