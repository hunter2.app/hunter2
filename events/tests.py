# Copyright (C) 2018-2022 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
from datetime import timedelta
from io import StringIO
from unittest.mock import Mock, MagicMock

from django.apps import apps
from django.contrib import admin
from django.core.management import CommandError, call_command
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from events.factories import AttendanceFactory, EventFactory, EventFileFactory
from events.models import Event, EventFile
from hunter2.test import mock_inputs, MockTTY
from hunts.factories import EpisodeFactory, PuzzleFactory
from hunts.models import Episode
from . import context_processors
from .management.commands import createevent
from .test import EventAwareTestCase, EventTestCase


class FactoryTests(EventTestCase):

    def test_event_file_factory_default_construction(self):
        EventFileFactory.create()

    def test_attendance_factory_default_construction(self):
        AttendanceFactory.create()

    def test_event_factory_errors_in_testcase(self):
        with self.assertRaises(AssertionError):
            EventFactory.create()


class AdminRegistrationTests(TestCase):
    def test_models_registered(self):
        models = apps.get_app_config('events').get_models()
        # Models which don't need to be directly registered due to being managed by inlines
        inline_models = (EventFile, )
        for model in models:
            if model not in inline_models:
                self.assertIsInstance(admin.site._registry[model], admin.ModelAdmin)


class CreateEventManagementCommandTests(EventAwareTestCase):
    TEST_EVENT_NAME = "Custom Event"
    TEST_SUBDOMAIN = 'custom'
    TEST_EVENT_END_DATE = "Monday at 18:00"
    INVALID_END_DATE = "18:00 on the second Sunday after Pentecost"

    def test_no_event_name_argument(self):
        output = StringIO()
        with self.assertRaisesMessage(CommandError, "You must use --event, --subdomain and --enddate with --noinput."):
            call_command(
                'createevent',
                interactive=False,
                subdomain=self.TEST_SUBDOMAIN,
                end_date=self.TEST_EVENT_END_DATE,
                stdout=output
            )

    def test_no_end_date_argument(self):
        output = StringIO()
        with self.assertRaisesMessage(CommandError, "You must use --event, --subdomain and --enddate with --noinput."):
            call_command(
                'createevent',
                interactive=False,
                subdomain=self.TEST_SUBDOMAIN,
                event_name="Test Event",
                stdout=output
            )

    def test_invalid_date(self):
        output = StringIO()
        with self.assertRaisesMessage(CommandError, "End date is not a valid date."):
            call_command(
                'createevent',
                interactive=False,
                event_name=self.TEST_EVENT_NAME,
                subdomain=self.TEST_SUBDOMAIN,
                end_date=self.INVALID_END_DATE,
                stdout=output
            )

    def test_non_interactive_usage(self):
        output = StringIO()
        call_command(
            'createevent',
            interactive=False,
            event_name=self.TEST_EVENT_NAME,
            subdomain=self.TEST_SUBDOMAIN,
            end_date=self.TEST_EVENT_END_DATE,
            stdout=output
        )
        command_output = output.getvalue().strip()
        self.assertEqual(command_output, f'Created current event "{self.TEST_EVENT_NAME}"')

        event = Event.objects.get(name=self.TEST_EVENT_NAME)
        self.assertIsNotNone(event)

    @mock_inputs({
        'event': TEST_EVENT_NAME,
        'subdomain': TEST_SUBDOMAIN,
    })
    def test_interactive_usage(self):
        output = StringIO()
        call_command(
            'createevent',
            interactive=True,
            stdout=output,
            stdin=MockTTY(),
        )
        command_output = output.getvalue().strip()
        self.assertEqual(command_output, f'Created current event "{self.TEST_EVENT_NAME}"')

        event = Event.objects.get(name=self.TEST_EVENT_NAME)
        self.assertIsNotNone(event)

    @mock_inputs({
        'end date': "",
        'event': "",
        'subdomain': "",
    })
    def test_default_interactive_usage(self):
        output = StringIO()
        call_command(
            'createevent',
            interactive=True,
            stdout=output,
            stdin=MockTTY(),
        )
        command_output = output.getvalue().strip()
        self.assertEqual(command_output, f'Created current event "{createevent.Command.DEFAULT_EVENT_NAME}"')

        event = Event.objects.get(name=createevent.Command.DEFAULT_EVENT_NAME)
        self.assertIsNotNone(event)


class EventContentTests(EventTestCase):
    def test_can_load_about(self):
        self.tenant.about_text = '__test__'
        self.tenant.save()
        url = reverse('about')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '__test__')


class EventThemeTests(EventTestCase):
    def test_load_with_event_theme(self):
        self.tenant.script = '// script comment'
        self.tenant.script_file = EventFileFactory()
        self.tenant.style = '/* style comment */'
        self.tenant.style_file = EventFileFactory()
        self.tenant.save()
        url = reverse('about')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response_html = response.content.decode('utf-8')
        self.assertInHTML('<script defer>// script comment</script>', response_html)
        self.assertInHTML(f'<script src="{self.tenant.script_file.file.url}" defer></script>', response_html)
        self.assertInHTML('<style>/* style comment */</style>', response_html)
        self.assertInHTML(f'<link href="{self.tenant.style_file.file.url}" rel="stylesheet" />', response_html)


class TestEventTime:
    def test_puzzles_available_time_from_episode(self, event):
        p = PuzzleFactory(start_date=None)
        assert Episode.objects.count() == 1
        assert event.puzzles_available_time() == p.episode.start_date

    def test_puzzles_available_time_from_puzzle(self, event):
        e = EpisodeFactory()
        p = PuzzleFactory(episode=e, start_date=e.start_date + timedelta(hours=1))
        assert event.puzzles_available_time() == p.start_date

    def test_puzzles_available_time_ancestor_puzzle(self, event):
        e1 = EpisodeFactory()
        p1 = PuzzleFactory(episode=e1, start_date=e1.start_date + timedelta(hours=1))
        EpisodeFactory(prequels=e1, start_date=e1.start_date + timedelta(seconds=1))  # This episode starts before the puzzle in the first one
        assert event.puzzles_available_time() == p1.start_date


class TestContextProcessors:
    # This test uses the transactional fixture because it flushes the event table; the default behaviour permits
    # events to persist across tests.
    def test_other_events_none(self, tenant_transactional_db):
        request = MagicMock()
        request.META = {'HTTP_HOST': 'test:80'}
        request.tenant = EventFactory()
        assert len(context_processors.other_events(request)['other_events']) == 0

    def test_other_events_some(self, tenant_transactional_db):
        events = EventFactory.create_batch(4, is_published=True)
        EventFactory.create_batch(2, is_published=False)
        offset = timezone.timedelta(days=100)
        date = timezone.now() + offset + timezone.timedelta(hours=1)
        for ev in events:
            ev.end_date = date
            date -= offset
            ev.save()
        request = MagicMock()
        request.tenant = events.pop(1)
        request.scheme = 'https'
        request.META = {'HTTP_HOST': 'test:80'}

        other_events = context_processors.other_events(request)['other_events']
        assert len(other_events) == 3
        for context, event in zip(other_events, events):
            assert context['name'] == event.name
            assert context['url'] == f'//{event.get_primary_domain().domain}:80'

    def test_event_theme_none(self, event):
        request = Mock()
        request.tenant = event
        context = context_processors.event_theme(request)
        for key in ('event_script_file', 'event_script', 'event_style_file', 'event_style'):
            assert key not in context

    def test_event_theme_some(self, event):
        request = Mock()
        # Don't mock the attributes files_map methods check for cached maps, to allow them to be populated properly
        del request.site_files
        del request.event_files
        request.tenant = event
        request.tenant.script = '// script comment'
        request.tenant.script_file = EventFileFactory()
        request.tenant.style = '/* style comment */'
        request.tenant.style_file = EventFileFactory()
        context = context_processors.event_theme(request)
        for key in ('event_script_file', 'event_script', 'event_style_file', 'event_style'):
            assert context[key]
