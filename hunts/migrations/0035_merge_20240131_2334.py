# Generated by Django 4.2.9 on 2024-01-31 23:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hunts', '0002_create_teamhint'),
        ('hunts', '0034_puzzle_puzzle_episode_start_date_order_unique'),
    ]

    operations = [
    ]
