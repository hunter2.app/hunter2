import sys

from jsonmodels import models, fields, validators
from jsonmodels.errors import ValidationError
import inflection


class Message(models.Base):
    @staticmethod
    def from_struct(struct, validate=True):
        try:
            class_name = inflection.camelize(inflection.underscore(struct['type']))
            message = getattr(sys.modules[__name__], class_name)()
            message.populate(**struct['content'])
        except (AttributeError, KeyError):
            raise ValidationError()
        if validate:
            message.validate()
        return message

    def to_struct(self):
        return {
            'type': inflection.underscore(type(self).__name__),
            'content': super().to_struct(),
        }


class Error(Message):
    code = fields.StringField(required=True, validators=validators.Enum(
        "IMPOLITE_MESSAGE",
        "MISSING_TYPE",
        "INVALID_TYPE",
        "MISSING_TOKEN",
        "MALFORMED_TOKEN",
        "INVALID_TOKEN",
        "INVALID_MESSAGE",
    ))
    message = fields.StringField(required=True)


class EpisodeFinishersPlz(Message):
    api_token = fields.StringField(required=True)


class EpisodeFinished(Message):
    episode_id = fields.IntField(required=True)
    team_id = fields.IntField(required=True)


class PuzzleFinishersPlz(Message):
    api_token = fields.StringField(required=True)


class PuzzleFinished(Message):
    puzzle_id = fields.IntField(required=True)
    team_id = fields.IntField(required=True)
