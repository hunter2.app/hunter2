# Copyright (C) 2018 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.
import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter, ChannelNameRouter
from django.core.asgi import get_asgi_application

import hunts.routing
from events.middleware import TenantWebsocketMiddleware
from hunts import hint_scheduler

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hunter2.settings")

application = ProtocolTypeRouter({
    "http": get_asgi_application(),
    'websocket':
        TenantWebsocketMiddleware(
            AuthMiddlewareStack(
                URLRouter(
                    hunts.routing.websocket_urlpatterns
                )
            )
        ),
    'channel': ChannelNameRouter(
        {
            hint_scheduler.CHANNEL_NAME: hint_scheduler.HintScheduler.as_asgi()
        }
    ),
})
