# Generated by Django 5.0.6 on 2024-06-13 07:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0016_alter_event_max_team_size'),
        ('events', '0016_remove_event_current_event_published'),
    ]

    operations = [
    ]
