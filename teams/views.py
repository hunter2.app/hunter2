# Copyright (C) 2018 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.


import json

from dal import autocomplete
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.http import Http404, JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView, UpdateView

from events.utils import annotate_user_queryset_with_seat
from hunter2.mixins import APITokenRequiredMixin
from . import forms, models
from .forms import CreateTeamForm, RequestForm


class TeamAutoComplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    raise_exception = True

    def get_queryset(self):
        qs = models.Team.objects.filter(at_event=self.request.tenant).order_by('name')

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


class CreateTeamView(LoginRequiredMixin, UpdateView):
    form_class = forms.CreateTeamForm
    template_name = 'teams/create.html'

    def get_object(self):
        return self.request.team

    def get_success_url(self):
        team_id = self.request.team.pk
        return reverse('team', kwargs={'team_id': team_id})


def size_data(num_size_categories, current_category, category_after_accept, max_size, team):
    return {
        'num_size_categories': num_size_categories,
        'current_size': team.members.count(),
        'current_category': {
            'id': current_category.id if current_category else None,
            'label': current_category.display_label() if current_category else None,
        },
        'category_after_accept': {
            'id': category_after_accept.id if category_after_accept else None,
            'label': category_after_accept.display_label() if category_after_accept else None,
        },
        'is_full': (not team.is_admin) and (
            team.members.count() >= max_size if max_size is not None else False)
    }


class ManageTeamView(LoginRequiredMixin, TemplateView):
    template_name = "teams/manage.html"

    def get_context_data(self, **kwargs):
        request = self.request
        if request.team.is_explicit():
            members = annotate_user_queryset_with_seat(request.team.members, request.tenant)
            requests = annotate_user_queryset_with_seat(request.team.requests, request.tenant)
            context = {
                'members': members,
                'requests': requests,
            }
        else:
            requests = models.Team.objects.filter(requests=request.user)
            create_form = CreateTeamForm(instance=request.team)
            request_form = RequestForm()
            context = {
                'requests': requests,
                'create_form': create_form,
                'request_form': request_form,
            }
        max_size = models.SizeCategory.max_team_size()
        categories = models.SizeCategory.objects.all().with_lower_limit()
        current_category = request.team.get_size_category()
        category_after_accept = models.SizeCategory.for_size(request.team.members.count() + 1)

        context['token'] = request.team.token
        context['max_team_size'] = max_size
        context['size_categories'] = categories
        context['current_category'] = current_category
        context['size_data'] = size_data(categories.count(), current_category, category_after_accept, max_size, request.team)
        if request.tenant:
            context['discord_url'] = request.tenant.discord_url
            context['discord_bot_id'] = request.tenant.discord_bot_id
        return context


class TeamView(LoginRequiredMixin, View):
    def get(self, request, team_id):
        team = get_object_or_404(
            models.Team, at_event=request.tenant, pk=team_id
        )
        if not team.name:
            raise Http404
        else:
            members = annotate_user_queryset_with_seat(team.members, request.tenant)

            return TemplateResponse(
                request,
                'teams/view.html',
                context={
                    'team': team.name,
                    'members': members,
                    'requested': request.user in team.requests.all(),
                }
            )


class Request(LoginRequiredMixin, View):
    raise_exception = True

    def post(self, request, team_id):
        team = get_object_or_404(models.Team, at_event=request.tenant, pk=team_id)
        user = request.user
        if user.is_on_explicit_team(request.tenant):
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'Already a member of a team for this event',
            }, status=400)
        if user in team.requests.all():
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'Already requested',
            }, status=400)
        if team.is_full():
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'This team is full',
            }, status=400)
        team.requests.add(user)
        return JsonResponse({
            'result': 'OK',
            'message': 'Requested',
            'team': team.name,
        })


class CancelRequest(LoginRequiredMixin, View):
    raise_exception = True

    def post(self, request, team_id):
        team = get_object_or_404(models.Team, at_event=request.tenant, pk=team_id)
        user = request.user
        if user not in team.requests.all():
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'Request does not exist',
                'delete': True,
            }, status=400)
        team.requests.remove(user)
        return JsonResponse({
            'result': 'OK',
            'message': 'Requested cancelled',
        })


class AcceptRequest(LoginRequiredMixin, View):
    raise_exception = True

    def post(self, request, team_id):
        data = json.loads(request.body)
        team = get_object_or_404(models.Team, at_event=request.tenant, pk=team_id)
        if request.user not in team.members.all():
            return JsonResponse({
                'result': 'Forbidden',
                'message': 'Must be a team member to accept an request',
            }, status=403)
        User = get_user_model()
        try:
            user = User.objects.get(uuid=data['user'])
        except ValidationError:
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'Invalid User UUID',
            }, status=400)
        except User.DoesNotExist:
            return JsonResponse({
                'result': 'Not Found',
                'message': 'User does not exist',
                'delete': True,
            }, status=404)
        if user not in team.requests.all():
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'User has not requested to join',
                'delete': True,
            }, status=400)
        if user.is_on_explicit_team(request.tenant):
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'Already a member of a team for this event',
                'delete': True,
            }, status=403)
        if team.is_full():
            team.requests.remove(user)
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'This team is full',
                'delete': True,
            }, status=400)
        if team.members.count() != data['currentSize']:
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'Team size changed since page load',
                'delete': False,
            }, status=400)
        try:
            old_team = user.teams.get()
        except models.Team.DoesNotExist:
            pass
        else:
            old_team.guess_set.update(by_team=team)
            old_team.delete()  # This is the user's implicit team, as checked above.
            user.team_requests.remove(*user.team_requests.filter(at_event=request.tenant))
        team.members.add(user)
        seat = user.attendance_at(request.tenant).seat
        current_category = request.team.get_size_category()
        category_after_accept = models.SizeCategory.for_size(request.team.members.count() + 1)
        max_size = models.SizeCategory.max_team_size()
        return JsonResponse({
            'result': 'OK',
            'message': 'Request accepted',
            'username': user.username,
            'seat': seat,
            'url': user.get_absolute_url(),
            'picture': user.picture,
            'team_members': [
                {
                    'username': member.username,
                    'seat': member.attendance_at(request.tenant).seat,
                    'url': member.get_absolute_url(),
                    'picture': member.picture
                } for member in team.members.all()
            ],
            'size_data': size_data(
                models.SizeCategory.objects.count(), current_category, category_after_accept, max_size, team
            ),
        })


class DenyRequest(LoginRequiredMixin, View):
    raise_exception = True

    def post(self, request, team_id):
        data = json.loads(request.body)
        team = get_object_or_404(models.Team, at_event=request.tenant, pk=team_id)
        User = get_user_model()
        try:
            user = User.objects.get(uuid=data['user'])
        except ValidationError:
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'Invalid User UUID',
            }, status=400)
        except User.DoesNotExist:
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'User does not exist',
                'delete': True,
            }, status=400)
        if request.user not in team.members.all():
            return JsonResponse({
                'result': 'Forbidden',
                'message': 'Must be a team member to deny a request',
            }, status=403)
        if user not in team.requests.all():
            return JsonResponse({
                'result': 'Bad Request',
                'message': 'User has not requested to join',
                'delete': True,
            }, status=400)
        team.requests.remove(user)
        return JsonResponse({
            'result': 'OK',
            'message': 'Request denied',
        })


class TeamListView(APITokenRequiredMixin, View):
    operation = 'list'
    resource = 'teams'

    def get(self, request):
        plain = False
        # Very basic Accept header handling
        # If the header is present and contains text/plain then return that, otherwise return JSON
        # This can be expanded if we want to support more API paths and/or more formats or if we ever want to respect q values
        accepts = request.META.get('HTTP_ACCEPT')
        if accepts is not None:
            for media in accepts.split(','):
                if media.strip().split(';')[0].strip() == 'text/plain':
                    plain = True
                    break
        teams = models.Team.objects.all().prefetch_related('members').seal()
        if plain:
            return HttpResponse("\n".join(t.get_unique_ascii_name() for t in teams))
        return JsonResponse({
            'items': [{
                'id': t.id,
                'type': 'team' if t.is_explicit() else 'player',
                'name': t.get_display_name(),
            } for t in teams],
        })


class TeamInfoView(APITokenRequiredMixin, View):
    operation = 'read'
    resource = 'team'

    def get(self, request, team_token):
        try:
            team = models.Team.objects.get(token=team_token)
        except models.Team.DoesNotExist:
            return JsonResponse({
                'result': 'Not Found',
                'message': 'Invalid team token',
            }, status=404)
        return JsonResponse({
            'result': 'OK',
            'team': {
                'name': team.name,
            },
        })
