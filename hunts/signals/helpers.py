# Copyright (C) 2024 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.db.models.signals import post_save, pre_save, pre_delete, post_delete
from django.dispatch import receiver


def hybrid_save_handler(sender):
    """The purpose of this decorator is to connect signals to the type of handlers we use that interact across threads
    due to using channels to do stuff asynchronously.

    Before the normal signature of the signal handler, func is passed "old", the instance in the
    database before save was called (or None). func will then be called after the current
    transaction has been successfully committed, ensuring that the instance argument is stored in the
    database and accessible via database connections in other threads, and that data is ready to be
    sent to clients."""
    def inner(func):
        @pre_save_handler(sender)
        def handler(sender, instance, *args, **kwargs):
            try:
                old = type(instance).objects.get(pk=instance.pk)
            except ObjectDoesNotExist:
                old = None

            def after_commit():
                func(old, sender, instance, *args, **kwargs)

            if transaction.get_autocommit():
                # in this case we want to wait until *post* save so the new object is in the db, which on_commit
                # will not do. Instead, do nothing but set an attribute on the instance to the callback, and
                # call it later in a post_save receiver.
                if not hasattr(instance, '_hybrid_save_cbs'):
                    instance._hybrid_save_cbs = []
                instance._hybrid_save_cbs.append(after_commit)

            else:  # nocover
                transaction.on_commit(after_commit)

        return handler

    return inner


@receiver(post_save)
def hybrid_save_signal_dispatcher(sender, instance, **kwargs):
    # This checks for the attribute set by the above signal handler and calls it if it exists.
    hybrid_cbs = getattr(instance, '_hybrid_save_cbs', None)
    for hybrid_cb in hybrid_cbs or []:
        # No need to pass args because this will always be a closure with the args from pre_save
        hybrid_cb()

    instance._hybrid_save_cbs = []


def pre_save_handler(sender):
    def pre_save_decorator(func):
        pre_save.connect(func, sender=sender)
        return func

    return pre_save_decorator


def post_save_handler(sender, pass_old=False):
    # The functionality overlaps with that of the hybrid save handler in that it can pass the old instance through to
    # post_save. The major difference is that it doesn't ever rely on transaction behaviour. This is simpler and means
    # it works in tests where the transaction is never committed.
    def get_old_instance(sender, instance, *args, **kwargs):
        old = None
        if instance.pk:
            try:
                old = sender.objects.get(pk=instance.pk)
            except sender.DoesNotExist:
                pass

        instance._old_instance = old

    def post_save_decorator(func):
        if pass_old:
            pre_save.connect(get_old_instance, sender=sender, weak=False)
        post_save.connect(func, sender=sender)
        return func

    return post_save_decorator


def pre_delete_handler(sender):
    def pre_delete_decorator(func):
        pre_delete.connect(func, sender=sender)
        return func

    return pre_delete_decorator


def post_delete_handler(sender):
    def post_delete_decorator(func):
        post_delete.connect(func, sender=sender)
        return func

    return post_delete_decorator
