# Integrations

hunter2 can be extended via the use of integration APIs.
There are two types of API: HTTP RPCs and WebSocket update streams.

## Authentication Tokens

Authentication tokens can be made by superusers from the admin UI.
Tokens must be authorised for each operation they should perform, with optional further restrictions on the event and the fields in the request.
If a token specifies fields then it must also specify an event (to avoid unauthorised access due to duplicate IDs in different events).

## HTTP RPCs

The following HTTP RPCs are available:

| Path                     | Method | Description                                        | Return Format |
|--------------------------|--------|----------------------------------------------------|---------------|
| `/teaminfo/<team_token>` | GET    | Resolve team information from a team secret token. | `{}`          |
| `/teamlist/`             | GET    | Fetch a list of teams registered for the event.    | `{}`          |

## Websocket update streams

The endpoint for receiving a stream of hunt updates via websocket is `/ws/hunt/update_stream`.
Once connected, a client can subscribe for updates using the following messages.

| Type                    | Field(s)      | Description                                                                       | Default |
|-------------------------|---------------|-----------------------------------------------------------------------------------|---------|
| `episode-finishers-plz` | `episode_ids` | Array of episode IDs to receive updates for. If empty, subscribe to all episodes. | `[]`    |
| `puzzle-finishers-plz`  | `puzzle_ids`  | Array of puzzle IDs to receive updates for. If empty, subscribe to all puzzles.   | `[]`    |
