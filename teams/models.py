# Copyright (C) 2018 The Hunter2 Contributors.
#
# This file is part of Hunter2.
#
# Hunter2 is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Hunter2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Hunter2.  If not, see <http://www.gnu.org/licenses/>.

import uuid
from functools import total_ordering

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Count
from django.db.models import F, Q
from django.urls import reverse
from encodings.punycode import punycode_encode
from enumfields import Enum, EnumField
from seal.models import SealableModel
from seal.query import SealableQuerySet

import events


class TeamRole(Enum):
    PLAYER = 'p'
    ADMIN  = 'a'
    AUTHOR = 'A'


class TeamQuerySet(SealableQuerySet):
    def with_size(self):
        with_member_counts = list(self.annotate(members_count=Count('members', distinct=True)))
        size_categories = SizeCategory.objects.all()

        for team in with_member_counts:
            size_category = None
            for sc in size_categories:
                if sc.max is None or team.members_count <= sc.max:
                    size_category = sc
                    break
            team.size_category = size_category

        return with_member_counts


class Team(SealableModel):
    # Nullable CharField with the unique property allows us to enforce uniqueness at DB schema level
    # DB will allow multiple teams with no name while still enforcing name uniqueness
    name = models.CharField(blank=True, null=True, unique=True, max_length=100)
    at_event = models.ForeignKey(events.models.Event, on_delete=models.DO_NOTHING, related_name='teams')
    role = EnumField(
        TeamRole, max_length=1, default=TeamRole.PLAYER,
        help_text='Role of the team. Admins can edit the event and see admin views, authors are credited on the about page'
    )
    members = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='teams', through='TeamMembership')
    requests = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='team_requests')
    token = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    disqualified = models.BooleanField(default=False, help_text='Should this team be excluded from finished times and stats?')

    objects = TeamQuerySet.as_manager()

    def __str__(self):
        return '%s @%s' % (self.get_verbose_name(), self.at_event)

    def get_display_name(self):
        return self.name if self.is_explicit() else self.members.all()[0].username

    @staticmethod
    def _punycode(string):
        return punycode_encode(string).decode('utf-8')

    def get_unique_ascii_name(self):
        if self.is_explicit():
            return f'+{self._punycode(self.name)}'
        else:
            return f'-{self._punycode(self.members.all()[0].username)}'

    def get_verbose_name(self):
        if self.is_explicit():
            return self.name
        if 'members' in getattr(self, '_prefetched_objects_cache', {}):
            if len(self.members.all()) == 1:
                # We use .all()[0] to allow for prefetch_related
                return '[%s\'s team]' % (self.members.all()[0])
            elif len(self.members.all()) == 0:
                return '[empty anonymous team]'
            else:
                # This should never happen but we don't want things to break if it does!
                return '[anonymous team with %d members!]' % len(self.members.all())
        else:
            return f'[team id {self.id}]'

    def get_size_category(self):
        return SizeCategory.for_size(self.members.count())

    def clean(self):
        if (
            self.role == TeamRole.AUTHOR and
            Team.objects.exclude(id=self.id).filter(at_event=self.at_event).filter(role=TeamRole.AUTHOR).count() > 0
        ):
            raise ValidationError('There can only be one author team per event')

    def save(self, *args, **kwargs):
        # We don't want to use '' as our empty value because we would trip over the uniqueness constraint
        if self.name == '':
            self.name = None
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('team', kwargs={'team_id': self.pk})

    @property
    def is_admin(self):
        return self.role == TeamRole.ADMIN or self.role == TeamRole.AUTHOR

    def is_explicit(self):
        return self.name is not None

    def is_full(self):
        max = SizeCategory.max_team_size()
        if max is None:
            return False
        return self.members.count() >= SizeCategory.max_team_size() and not self.is_admin


class TeamMembership(SealableModel):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)


class SizeCategoryQuerySet(SealableQuerySet):
    def with_lower_limit(self):
        lower_limits = [1] + [n+1 for n in list(self.values_list('max', flat=True))[:-1]]
        for ll, instance in zip(lower_limits, self):
            instance.min = ll

        return self


# We only defined __lt__; __eq__ is defined by Django, which is compatible with the ordering by maxima, because max is
# unique.
@total_ordering
class SizeCategory(SealableModel):
    max = models.PositiveSmallIntegerField(
        blank=True, null=True, help_text='Maximum team size (inclusive) to fit in this category, or blank for no limit'
    )
    label = models.CharField(
        max_length=255,
        blank=True,
        help_text='Rendered as HTML. Displayed to players to identify the category. If left blank, the range will be used instead.'
    )
    description = models.TextField(blank=True, help_text='Rendered as HTML. Displayed when explaining the categories to players.')

    objects = SizeCategoryQuerySet.as_manager()

    class Meta:
        ordering = [F('max').asc(nulls_last=True)]
        constraints = (
            models.UniqueConstraint(
                fields=('max',),
                name='teams_sizecategory_unique_max',
                nulls_distinct=False,
            ),
        )
        verbose_name_plural = 'size categories'

    @classmethod
    def max_team_size(cls):
        maxes = cls.objects.all().values_list('max', flat=True)
        try:
            return max(maxes)
        except (ValueError, TypeError):
            return None

    @classmethod
    def for_size(cls, size):
        return cls.objects.filter(Q(max=None) | Q(max__gte=size)).first()

    def display_range(self):
        if hasattr(self, 'min') and self.max is not None:
            return f'{self.min}&ndash;{self.max}'
        if hasattr(self, 'min'):
            return f'{self.min} or more'
        if self.max is not None:
            return f'up to {self.max}'

        return 'unlimited'

    def display_label(self):
        if self.label:
            return self.label
        return self.display_range()

    def __lt__(self, other):
        return other.max is None or self.max < other.max

    def __str__(self):
        up_to = 'unlimited' if self.max is None else f'up to {self.max}'

        if self.label:
            return f'{self.label} ({up_to})'
        else:
            return up_to
