// The forms on this page use Django widgets which expect jquery
// to be loaded into the global scope as $ and jQuery
import $ from 'expose-loader?exposes=$,jQuery!jquery'

import {createApp, reactive} from 'vue'
import * as Sentry from '@sentry/vue'
import {Popover} from 'bootstrap'

import 'hunter2/js/base.js'
import 'hunter2/js/csrf.js'
import * as request from './request.js'
import ProgressTable from './size-info.vue'

import '../scss/team_members.scss'

$(function(){
  const sizeData = JSON.parse(
    document.getElementById('size-data').textContent,
  )
  window.sizeInfo = reactive(sizeData)
  const vueEl = document.getElementById('size-info')

  const progress = createApp(ProgressTable, {
    sizeInfo: window.sizeInfo,
  })
  progress.mixin(Sentry.createTracingMixins({ trackComponents: true }))
  Sentry.attachErrorHandler(progress, { logErrors: true })
  progress.mount(vueEl)

  let popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
  popoverTriggerList.map(function (popoverTriggerEl) {
    return new Popover(popoverTriggerEl)
  })

  $('#req-create').on('submit', request.create)
  $('.req-accept').on('click', request.accept.bind(window.sizeInfo))
  $('.req-deny').on('click', request.decline)
  $('.req-cancel').on('click', request.cancel)
})
